import 'react-native-gesture-handler';

import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions
} 
from 'react-native';
import React, { Component,useEffect } from 'react'

import SplashScreen from 'react-native-splash-screen'


// import RootNavigator from './app/navigator/RootNavigator';


import HomePage from '../Screens/HomePage/HomePage'
import Search from '../Screens/Search/Search'
import Wallet from '../Screens/Wallet/Wallet'
import Profile from '../Screens/Profile/Profile'
import EditProfile from '../Screens/EditProfile/EditProfile'
import Portfolio from '../Screens/Portfolio/Portfolio'
import Notifications from '../Screens/Notifications/Notifications'
import DrawerContainer from '../Screens/DrawerContainer/DrawerContainer';
import AboutUs from '../Screens/AboutUs/AboutUs';
import ContactUs from '../Screens/ContactUs/ContactUs';
import TermsConds from '../Screens/TermsCondition/TermsCondition';
import PrivacyPolicy from '../Screens/PrivacyPolicy/PrivacyPolicy';
import HelpSupport from '../Screens/HelpSupport/HelpSupport';
import Invoice from '../Screens/Invoice/Invoice';
import CreateTicket from '../Screens/CreateTicket/CreateTicket'
import WatchList from '../Screens/WatchList/WatchList'
import HowToTrade from '../Screens/HowToTrade/HowToTrade'
import Category from '../Screens/Category/Category'
import SubCat from '../Screens/SubCat/SubCat'
import KycVerification from '../Screens/KycVerification/KycVerification'
import Congrats from '../Screens/Congrats/Congrats'
import EventDetails from '../Screens/EventDetails/EventDetails';
import Withdraw from '../Screens/Withdraw/Withdraw'
import WithdrawSuccess from '../Screens/WithdrawSuccess/WithdrawSuccess';
import WithdrawFailure from '../Screens/WithdrawFailure/WithdrawFailure';
import AccountDetails from '../Screens/AccountDetails/AccountDetails';
import AccountDetailsFilled from '../Screens/AccountDetailsFilled/AccountDetailsFilled';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import TradeSuccess from '../Screens/TradeSuccess/TradeSuccess';
import Referral from '../Screens/Referral/Referral';
import WebviewComp from '../utils/WebviewComp';
import { imageConstant } from '../utils/constant';

import CustomModal from '../custom/CustomModal';

const width = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
import { useScrollToTop } from '@react-navigation/native';



const DrawerStack = () => (
  <>
    <Drawer.Navigator
      // initialRouteName="TabStack"
      screenOptions={{
      drawerPosition:'left',
      drawerType:"front",
        headerShown: false,
        drawerStyle: {
          width:'80%',
        },
      }}
      drawerContent={props => <DrawerContainer {...props} />}>
      <Drawer.Screen name="Tab" component={TabStack} />
    </Drawer.Navigator>
  </>
)

const TabStack = () => <>
    <Tab.Navigator
     backBehavior='history'
        screenOptions = {({ route: { name } }) => ({
            tabBarIcon: ({ focused }) => {
                switch (name) {

                    case 'HomePage':
                        return <Image source={focused ? imageConstant.home1 : imageConstant.home} style={{ width: 35, height: 40,marginBottom:10 }} />
    
                    case 'Search':
                          return <Image source={focused ? imageConstant.search1 : imageConstant.search} style={{ width: 39, height: 40, marginBottom:10}} />
                    
                    case 'Portfolio':
                        return <Image source={focused ? imageConstant.portfolio1 : imageConstant.portfolio} style={{ width: 54, height: 40, marginBottom:10 }} />
    
                    case 'Wallet':
                        return <Image source={focused ? imageConstant.wallet1 : imageConstant.wallet} style={{ width: 35, height: 40, marginBottom:10 }} />
                    
                    case 'My Profile':
                            return <Image source={focused ? imageConstant.profile1 : imageConstant.profile} style={{ width: 60, height: 40, marginBottom:10 }} />
              }
            },
           
            tabBarShowLabel:false,
            tabBarActiveTintColor: "white",
            tabBarInactiveTintColor: "white",
            tabBarStyle:{
              backgroundColor:"white",
              height: Platform.OS == 'ios' ? hp('10%') : width/5.8,
              width:width,
              // alignSelf:'center',
              // height:60,
              // height:hp('10%'),
              // alignItems:'center',
              justifyContent:"space-evenly",
              // position:'absolute',

        }

    })}
    
>
  <Tab.Screen name={'HomePage'} options={{headerShown: false}}  component={HomePage} />
  <Tab.Screen name={'Search'} options={{headerShown: false}}  component={Search} />
  <Tab.Screen name={'Portfolio'} options={{headerShown: false}}  component={Portfolio} />
  <Tab.Screen name={'Wallet'} options={{headerShown: false}}  component={Wallet} />
  <Tab.Screen name={'My Profile'} options={{headerShown: false}}  component={Profile} />

</Tab.Navigator>
</>


const MainStack = () => {

  return (
    // <SafeAreaView style={{flex:1}}>
      <Stack.Navigator initialRouteName='Drawer' screenOptions={{ headerShown: false }} >
        <Stack.Screen name= "CustomModal" component = {CustomModal}/>
       <Stack.Screen name= "TermsConds" component = {TermsConds}/>
        <Stack.Screen name= "Notifications" component = {Notifications}/>
        <Stack.Screen name= "AboutUs" component = {AboutUs}/>
        <Stack.Screen name= "WebviewComp" component = {WebviewComp}/>
        <Stack.Screen name= "ContactUs" component = {ContactUs}/>
        <Stack.Screen name= "PrivacyPolicy" component = {PrivacyPolicy}/>
        <Stack.Screen name= "HelpSupport" component = {HelpSupport}/>
        <Stack.Screen name= "CreateTicket" component = {CreateTicket}/>
        <Stack.Screen name= "Invoice" component = {Invoice}/>
        <Stack.Screen name= "EditProfile" component = {EditProfile}/>
        <Stack.Screen name= "WatchList" component = {WatchList}/>
        <Stack.Screen name= "HowToTrade" component = {HowToTrade}/>
        <Stack.Screen name= "Category" component = {Category}/>
        <Stack.Screen name= "SubCat" component = {SubCat}/>
        <Stack.Screen name= "KycVerification" component = {KycVerification}/>
        <Stack.Screen name= "Congrats" component = {Congrats}/>
        <Stack.Screen name= "EventDetails" component = {EventDetails}/>
        <Stack.Screen name= "AccountDetails" component = {AccountDetails}/>
        <Stack.Screen name= "Withdraw" component={Withdraw}/>
        <Stack.Screen name="WithdrawSuccess" component={WithdrawSuccess}/>
        <Stack.Screen name="WithdrawFailure" component={WithdrawFailure}/>
        {/* <Stack.Screen name="WithdrawStatus" component={WithdrawStatus}/> */}
        <Stack.Screen name= "AccountDetailsFilled" component = {AccountDetailsFilled}/>
        <Stack.Screen name="Referral" component={Referral} />
        <Stack.Screen name="Drawer" component={DrawerStack} />
        <Stack.Screen name="TradeSuccess" component={TradeSuccess} />
      </Stack.Navigator>
    // </SafeAreaView>

  )
}

export default MainStack
