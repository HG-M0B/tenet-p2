
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import MainScreen from '../Screens/Maintain/MainScreen';

const Stack = createNativeStackNavigator();




const MaintainStack = () => {
  return (
      <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="MainScreen">
          <Stack.Screen name="MainScreen" component={MainScreen} />
      </Stack.Navigator>
  )
}

export default MaintainStack

