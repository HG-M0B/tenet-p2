
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import IntroScreens from '../Screens/IntroSlider/IntroScreens';
import UserConsent from '../Screens/userConsent/UserConsent';


const Stack = createNativeStackNavigator();




const IntroStack = () => {
  return (
      <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="IntroScreens">
          <Stack.Screen name="IntroScreens" component={IntroScreens} />


      </Stack.Navigator>
  )
}

export default IntroStack

