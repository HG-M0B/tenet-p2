import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { useFocusEffect } from '@react-navigation/native';
import IntroStack from './IntroStack';
import MainStack from './MainStack';
import AuthStack from './AuthStack';
import MaintainStack from './MaintainStack';
import { useSelector } from 'react-redux';
import { useState ,useEffect} from 'react';
import AsyncStorage from "@react-native-async-storage/async-storage";


import PrivacyPolicy from '../Screens/PrivacyPolicy/PrivacyPolicy';
import { baseURL } from '../utils/URL';
const Stack = createNativeStackNavigator();


const RootNavigator =   ({ navigation }) => {

    const [status, setIntroStatus] = useState(null);
    let {introStatus} = useSelector((state) => state.reducers);
    let {reducers} = useSelector((state) => state);

    const changeStatus = async() => {
        setIntroStatus(introStatus)
    }
    useFocusEffect(() => {
     changeStatus();
        
    })
    console.log("redux-->",reducers);


    return (
        <Stack.Navigator 
        screenOptions={{
            headerShown: false
        }}>
            
            {status == null && (
                <Stack.Screen name="IntroStack" component={IntroStack} />
            )}

            {status == "login" && (
                <Stack.Screen name="AuthStack" component={AuthStack} />
            )}

            {status == "main" && (
                <Stack.Screen name="MainStack" component={MainStack} />
            )}
            {status == "check" && (
                <Stack.Screen name="MaintainStack" component={MaintainStack} />
            )}
        </Stack.Navigator>
    )
}

export default RootNavigator

