
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';



import Login from '../Screens/Login/Login'
import Otp from '../Screens/Otp/Otp'
import AdditionalDetails from '../Screens/AdditionalDetails/AdditionalDetails'
import PrivacyPolicy from '../Screens/PrivacyPolicy/PrivacyPolicy';
import TermsCondition from '../Screens/TermsCondition/TermsCondition';
import UserConsent from '../Screens/userConsent/UserConsent';

const Stack = createNativeStackNavigator();




const AuthStack = () => {
  return (
      <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Login">
        <Stack.Screen name = "Login" component = {Login}/>
        <Stack.Screen name= "Otp" component = {Otp}/>
        <Stack.Screen name= "AdditionalDetails" component = {AdditionalDetails}/>
        <Stack.Screen name= "TermsCondition" component = {TermsCondition}/>
        <Stack.Screen name= "PrivacyPolicy" component = {PrivacyPolicy}/>
        <Stack.Screen name= "UserConsent" component = {UserConsent}/>


      </Stack.Navigator>
  )
}

export default AuthStack

