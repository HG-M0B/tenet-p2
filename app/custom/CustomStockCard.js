import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant } from '../utils/constant'
import { Width } from '../dimension/dimension';
import ThemeContext from '../config/ThemeContext';
import { useContext } from 'react';

const CustomStockCard = ({item,index}) => {
let theme = useContext(ThemeContext);
 let percentage = item?.change > 0 ? ((item?.change/item?.price)*100).toFixed(2) : Math.abs(((item?.change/item?.price)*100).toFixed(2));


  return (
    <View style={[styles.mainView,{ borderColor: item?.change > 0 ? colorConstant.greenButton : colorConstant.red,backgroundColor:theme?.theme =='dark'? colorConstant.blackLight : colorConstant.white}]}>
        <View style={styles.rowView}>
            <Text style={[styles.key,{color:theme?.theme =="dark" ? colorConstant.white : colorConstant.black}]}>{item?.trading_symbol}</Text>
            <Text style={[styles.key,{color:theme?.theme =="dark" ? colorConstant.white : colorConstant.black}]}>{`₹${item?.price}`}</Text>
        </View>
        <View style={[styles.rowView,{marginTop:'1.3%'}]}>
            <Text style={styles.value1}>{`Listed in ${item?.exchange}`}</Text>
            <Text style={[styles.value2,{color: item?.change > 0 ? colorConstant.greenButton : colorConstant.red}]}>{`${item?.change > 0 ? "+":""}${(item?.change).toFixed(2)}(${percentage}%)`}</Text>
        </View>
    </View>
  )
}

export default CustomStockCard

const styles = StyleSheet.create({
    mainView:{
        width:"90%",
        alignSelf:"center",
        borderRadius:Width/40,
        padding:Width/25,
        borderLeftWidth:4,
       
    },
    rowView:{
        flexDirection:"row",
        justifyContent:"space-between"
    },
    key:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.black
    },
    key:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.black
    },
    value1:{
        fontSize:12,
        fontFamily:fontConstant.regular,
        color:colorConstant.notiTextColor
    },
    value2:{
        fontSize:12,
        fontFamily:fontConstant.bold,
        color:colorConstant.greenButton
    },

})