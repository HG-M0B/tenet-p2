import { Image, Platform, StyleSheet, Text, TextInput, View } from 'react-native'
import React ,{useContext}from 'react'
import { colorConstant, imageConstant } from '../utils/constant'
import { Width } from '../dimension/dimension';
import ThemeContext from '../config/ThemeContext';

const CustomSearch = (props) => {
    let theme = useContext(ThemeContext)
  return (
    <View style={[styles.rowView,{backgroundColor:theme?.theme =='dark'?colorConstant.darkBlack:colorConstant.white}]}>
    <Image
        source={imageConstant.search}
        resizeMode='contain'
        style={styles.img} />

    <TextInput
        placeholder='Search'
        style={styles.input}
        value={props.value}
        onChangeText={(text) => props.onChangeText(text)}
        placeholderTextColor={ theme?.theme =='dark' ?colorConstant.mud: colorConstant.black}
    />
</View>
  )
}

export default CustomSearch

const styles = StyleSheet.create({
    rowView:{
        width:"90%",
        alignSelf:"center",
        flexDirection:"row",
        backgroundColor:colorConstant.white,
        marginVertical:Width/30,
        paddingVertical: Platform.OS =="ios" ?  Width/25 : 0,
        justifyContent:"center",
        alignItems:"center",
        paddingHorizontal:Width/30,
        borderRadius:Width/40
    },
    input:{
        marginLeft:10,
        width:"90%",
        // backgroundColor:"green"
    },
    img:{
        width:18,
        height:18,

    }
})