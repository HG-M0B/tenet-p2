import { View, Text ,StyleSheet, TextInput,Image, Platform} from 'react-native';
import React, { useState,useContext } from 'react';
import { Height, Width } from '../dimension/dimension';
import { imageConstant, colorConstant, fontConstant } from '../utils/constant';
import ThemeContext from '../config/ThemeContext';
import { Dropdown } from 'react-native-element-dropdown';
const CustomDropDown = (props) => {
    let theme= useContext(ThemeContext)
    
  return (
      <View style={[{ ...styles.MainView }, {
          marginTop: props.marginTop ? props.marginTop : Height / 45,
          marginBottom: props.marginBottom ? props.marginBottom : 0,
          width: props.widthView ? props.widthView : "50%",
          borderWidth:props.borderWidth,
          borderColor:props.borderColor,
          padding:props.padding,
          backgroundColor: theme?.theme=="dark" ? colorConstant.darkBlack : colorConstant.backgroundColor
      }]}>
          {props.img
              ?
              <Image
                  source={props.img}
                  resizeMode='contain'
                  style={styles.img}
              /> :
              <></>}
          <Dropdown
            style={[styles.dropStyle,{width:props.dropWidth?props.dropWidth:"80%"}]}
              data={props.data}
              labelField="label"
              valueField="value"
              maxHeight={200}
              onChange={props.onChange}
              disable={props.disable}
              selectedTextStyle={props.selectedTextStyle}
              value={props.value}
              containerStyle={props.containerStyle}
              iconStyle={{
                  tintColor:theme?.theme =="dark" ? colorConstant.white : colorConstant.black,
                  position:"absolute",
                  right:props.right,
              }}
              placeholder={props.placeholder ?props.placeholder: "Select"}
              placeholderStyle={{fontSize:14, color:theme?.theme =="dark" ? colorConstant.white : colorConstant.lightGrayText}}
          />
      </View>
  );
};

export default CustomDropDown;

const styles = StyleSheet.create({
    MainView:{
    alignSelf:"center",
    flexDirection:"row",
    alignItems:"center",
    justifyContent:"space-between",
    borderRadius:Width/50,
    paddingVertical:Platform.OS == 'ios'  ? Width/65: Width/35,
    paddingHorizontal: Platform.OS == 'ios' ?  10 : Width/40
    },
    dropStyle:{
        color:"black"

    },
    img:{
        width:25,
        height:25,
    },
    placeholderStyle:{

    }
})
