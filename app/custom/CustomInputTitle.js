import { View, Text ,StyleSheet, TextInput,Image} from 'react-native';
import React, { useState ,useContext} from 'react';
import { Height, Width } from '../dimension/dimension';
import { imageConstant, colorConstant, fontConstant } from '../utils/constant';


const CustomInputTitle = (props) => {

  return (

      <View style= {[styles.outterView,{
          marginTop:props.marginTop?props.marginTop:Height/50,
          marginBottom:props.marginBottom,
          width: props.width ? props.width :"90%",

      }]}>
          <Text style={[styles.titleText,{color:colorConstant.notiTextColor}]}>{props.title}</Text>
           <View style={[styles.innerView,{backgroundColor:theme?.theme =='dark'?colorConstant.darkBlack:colorConstant.backgroundColor,
                borderWidth : theme?.theme =='dark' ? 0 :1
            }]}>
              <TextInput
                  keyboardType={props.keyboardType}
                  placeholder={props.placeholder ? props.placeholder : 'Type here'}
                  placeholderTextColor={theme?.theme =='dark'?colorConstant.white:colorConstant.placeholderColor}
                  style={[styles.InputField,{
                      fontFamily:props.textSize,color:theme?.theme =='dark' ?colorConstant.white :colorConstant.black,
                    }]}
                    onChangeText={(text) => props.onChangeText(text)}
                    value={props.value}
                  editable={ props.editable}
                  multiline={props.multiline}
                  numberOfLines={props.numberOfLines}
                  onBlur={props.onBlur}
                  onChange={props.onChange}
              /></View>   
          
      </View>
  );
};

export default CustomInputTitle;

const styles = StyleSheet.create({
    outterView:{
        alignSelf:"center",

    },
    innerView:{
        width:"100%",
        alignSelf:"center",
        flexDirection:"row",
        alignItems:"center",
        marginTop:Width/40,
        borderRadius:Width/50,
        paddingHorizontal:10,
        paddingVertical:5,
        borderColor:"#EDEBF5"


    },
    titleText:{
        fontSize:14,
        fontFamily:fontConstant.regular,
        color:colorConstant.notiTextColor

    },
    InputField:{
        width:"95%",
        paddingVertical:Width/40,
        paddingHorizontal:Width/80,
        fontSize:14,
       
    },
    Img:{
        width:20,
        height:20
    }
})
