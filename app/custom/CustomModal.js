import { StyleSheet, Text, View ,Modal,TouchableOpacity} from 'react-native'
import React from 'react'
import { Height } from '../dimension/dimension';
import { colorConstant } from '../utils/constant';

const CustomModal = (props) => {
  return (
    <Modal
    animationType="slide"
    transparent={true}
    visible={true}
    // onRequestClose={()=>{
    //   setDetailsModal(!detailsModal);
    // }}
  >

    <View style={styles.centeredView2}>
        

      {/* <TouchableOpacity
        onPress={() => setDetailsModal(!detailsModal)}>

        <Image
          source={imageConstant.cancelImg}
          style={{
            width: 30,
            height: 30,
            right: 10,
            top: Height * 0.10,
            position: "absolute"
          }} />
      </TouchableOpacity> */}

      <View style={styles.modalView2}>

      <TouchableOpacity onPress={props.OnButton}>
            <Text>click</Text>
        </TouchableOpacity>

        {/* <View style={{
          flexDirection: "row",
          width: "100%",
          justifyContent: "space-between",
          height: 60

        }}>
          <TouchableOpacity
            style={{
              width: "50%",
              backgroundColor: colorConstant.blue,
              justifyContent: "center",
              alignItems: "center",
              borderBottomWidth: 1,
              borderBottomColor: "#D3D3D3",
              borderRightWidth: 1,
              borderRightColor: "#D3D3D3",
              borderTopLeftRadius: 20
            }}>
            <Text style={{
              fontSize: 18,
              fontFamily: fontConstant.bold,
              color: "#FFFFFF",



            }}>YES <Text style={{
              fontSize: 14,
              fontFamily: fontConstant.medium,
              color: "#D3D3D3",
            }}> ₹{eventDetails?.price_yes}</Text></Text>
          </TouchableOpacity>


          <TouchableOpacity
            style={{
              width: "50%",
              backgroundColor: colorConstant.pink,
              justifyContent: "center",
              alignItems: "center",
              borderBottomWidth: 1,
              borderBottomColor: "#D3D3D3",
              borderTopRightRadius: 20

            }}>
            <Text style={{
              fontSize: 18,
              fontFamily: fontConstant.bold,
              color: "#FFFFFF",



            }}>NO <Text style={{
              fontSize: 14,
              fontFamily: fontConstant.medium,
              color: "#D3D3D3",
            }}> ₹{eventDetails?.price_no} </Text></Text>
          </TouchableOpacity>



        </View>



        <ScrollView
          show showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            width: "90%",
            paddingVertical: 20
          }}>
          <Text style={{
            fontSize: 16,
            fontFamily: fontConstant.medium,
            color: "#D3D3D3",
            // width: "90%",
            // alignSelf: "center"
          }}>#{eventDetails?.subcategory?.name}</Text>

          <Text style={{
            alignSelf: "center",
            fontSize: 20,
            lineHeight:30,
            textAlign:"justify",
            fontFamily: fontConstant.bold,
            color: colorConstant.white,
            marginTop: 20
          }}>{eventDetails?.title}
          </Text>

          <View style={{
            flexDirection: "row",
            alignItems: "center",
            marginTop: 10,
            // width: "90%"
          }}>
            <Image
              source={imageConstant.timer}

              style={{
                width: 20,
                height: 20,
                tintColor: colorConstant.white
              }}
            />
            <Text style={{
              fontSize: 14,
              fontFamily: fontConstant.regular,
              color: colorConstant.white
            }}> {moment(eventDetails?.end_datetime).fromNow()}</Text>
          </View>

{/* 
          {
            suggestedQuantity?.[0]?.option == optionChoose?.toUpperCase()

              ?
              <View style={{
                flexDirection: "row",
                justifyContent: "space-between"
              }}>

                <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.gray,
                  // width: "90%",
                  textAlign: "right",
                  marginTop: 20
                }}>Matches<Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.regular,
                  color: colorConstant.white
                }}>{suggestedQuantity?.[0]?.qty}</Text></Text>

                <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.gray,
                  // width: "90%",
                  textAlign: "right",
                  marginTop: 20
                }}>Set Price <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.regular,
                  color: colorConstant.white
                }}> ₹{price} </Text></Text>

              </View>
              :
              <></>
          } */}


          {/* <View style={{
            flexDirection: "row",
            justifyContent: "space-between"
          }}>
{/* ***HERE */}
            {/* {
              suggestedQuantity?.option?.toUpperCase() == optionChoose.toUpperCase()
                ?
                <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.gray,
                  // width: "90%",
                  textAlign: "right",
                  marginTop: 20
                }}>Matches<Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.regular,
                  color: colorConstant.white
                }}> {suggestedQuantity?.qty} </Text></Text>
                :
                <View></View>
            } */}

            {/* <Text style={{
              fontSize: 16,
              fontFamily: fontConstant.medium,
              color: colorConstant.gray,
              textAlign: "right",
              marginTop: 20
            }}>Set Price <Text style={{
              fontSize: 16,
              fontFamily: fontConstant.regular,
              color: colorConstant.white
            }}> ₹{price} </Text></Text>

          </View>  */}


          {/* <MultiSlider
            values={price}
            min={1}
            max={10}
            sliderLength={Width * 0.85}
            onValuesChange={(val) => setPrice(val)}
            selectedStyle={{ backgroundColor: '#50CAFF' }}
            unselectedStyle={{ backgroundColor: '#E3EDF7' }}
            //={imageConstant.thumbimage}
            customMarker={() => {
              return (
                <Image
                  source={imageConstant.logo}
                  resizeMode='contain'
                  style={{
                    height: 20,
                    width: 20
                  }}
                />
              )
            }}
            trackStyle={{ height: 4, borderRadius: 10 }}
            markerStyle={{ height: 20, width: 20, backgroundColor: '#087CFF', }}
          />

          <Text style={{
            fontSize: 16,
            fontFamily: fontConstant.medium,
            color: colorConstant.gray,
            // width: "90%",
            textAlign: "right",
          }}>Quantity <Text style={{
            fontSize: 16,
            fontFamily: fontConstant.regular,
            color: colorConstant.white
          }}>{quantity}</Text></Text>


          <MultiSlider
            values={quantity}
            min={1}
            max={25}
            sliderLength={Width * 0.85}
            onValuesChange={(val) => setQuantity(val)}
            selectedStyle={{ backgroundColor: '#50CAFF' }}
            unselectedStyle={{ backgroundColor: '#E3EDF7' }}
            //={imageConstant.thumbimage}
            customMarker={() => {
              return (
                <Image
                  source={imageConstant.logo}
                  resizeMode='contain'
                  style={{
                    height: 20,
                    width: 20
                  }}
                />
              )
            }}
            trackStyle={{ height: 4, borderRadius: 10 }}
            markerStyle={{ height: 20, width: 20, backgroundColor: '#087CFF', }}
          />


          <View style={{
            flexDirection: "row",
            // width: "100%",
            justifyContent: "space-between",
            marginTop: 20
          }}>
            <View style={{
              width: "50%",
              justifyContent: "center",
              alignItems: "center",
              borderRightWidth: 1,
              borderRightColor: colorConstant.gray
            }}>
              <Text style={{
                fontSize: 18,
                fontFamily: fontConstant.bold,
                color: colorConstant.white
              }}>₹ {quantity*price}</Text>
              <Text style={{
                fontSize: 16,
                fontFamily: fontConstant.medium,
                color: colorConstant.gray
              }}>You invest</Text>
            </View>
            <View
              style={{
                width: "50%",
                justifyContent: "center",
                alignItems: "center"
              }}>
              <Text style={{
                fontSize: 18,
                fontFamily: fontConstant.bold,
                color: colorConstant.white
              }}>₹ {eventDetails?.trade_value * quantity}</Text>
              <Text style={{
                fontSize: 16,
                fontFamily: fontConstant.medium,
                color: colorConstant.gray
              }}>You Earn **</Text>
            </View>
          </View> */}




{/* 
          <SwipeButton 
          title={`Swipe for ${optionChoose == "yes" ? "Yes"  :"No"}`}
            shouldResetAfterSuccess={true}
            height={45}
            // width={'90%'}
            railBackgroundColor="#D7D6D6"
            titleColor='#FFFFFF'
            railFillBackgroundColor='#D7D6D6'
            railBorderColor='#D7D6D6'
            railFillBorderColor='#D7D6D6'
            thumbIconBackgroundColor='#0049F1'
            thumbIconImageSource={imageConstant.rightarrow}
            thumbIconBorderColor='#0049F1'
            containerStyles={{ marginVertical: 20 }}
            onSwipeSuccess={() => checkBalance(eventDetails?.id)}


          />

          {
            price * quantity > walletBalance ?
              <View style={{
                // width: "90%",
                flexDirection: "row",
                marginTop: 20,
                alignItems: "center",
                justifyContent: "space-between"
              }}>
                <Text style={{
                  fontSize: 18,
                  fontFamily: fontConstant.regular,
                  color: colorConstant.gray
                }}> Balance <Text style={{
                  fontSize: 18,
                  fontFamily: fontConstant.bold,
                  color: colorConstant.white
                }}> ₹ {walletBalance}</Text></Text>


                <TouchableOpacity
                   onPress={()=>{
                    setDetailsModal(!detailsModal);
                    setTimeout(()=>{
                      navigation.navigate("Wallet");
                    },500)
                  }}
                  activeOpacity={1}
                  style={{
                    paddingHorizontal: 25,
                    paddingVertical: 8,
                    borderRadius: 20,
                    backgroundColor: colorConstant.white

                  }}>
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blue
                  }}>Recharge Now</Text>
                </TouchableOpacity>
              </View>

              :
              <></>
          }
         

        </ScrollView> */} 



      </View>


    </View>


  </Modal>
  )
}

export default CustomModal

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor:"rgba(1,1,1,0.5)"
  
      },
      centeredView2: {
        flex: 1,
        backgroundColor:"rgba(1,1,1,0.5)"
  
      }
      ,
      modalView: {
        margin:10,
        width:"90%",
        backgroundColor: "white",
        borderRadius: 20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      },
      
      modalView2: {
        width:"100%",
        height:Height*0.80,
        position:"absolute",
        bottom:0,
        backgroundColor:colorConstant.blue,
        borderTopLeftRadius: 20,
        borderTopRightRadius:20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
      }
      ,
})