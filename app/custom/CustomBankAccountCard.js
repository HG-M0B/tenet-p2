import { StyleSheet, Text, View,Image, TouchableOpacity} from 'react-native'
import React, { useContext } from 'react'
import { colorConstant, fontConstant, imageConstant } from '../utils/constant'
import { Width } from '../dimension/dimension'
import ThemeContext from '../config/ThemeContext'

const CustomBankAccountCard = (props) => {
    const {item,index} =  props;
    let theme = useContext(ThemeContext)
  return (
    <View style={[styles.mainView,{backgroundColor:theme?.theme =='dark' ?colorConstant.darkBlack :colorConstant.white}]}>
      <View style={styles.rowView}>
          <Text style={[styles.text1,{color:theme?.theme =='dark' ?colorConstant.white :colorConstant.black}]}>{item.name}</Text>
          <Image 
          source={imageConstant.verify}
          resizeMode='contain'
          style={[styles.img,{marginLeft:"2.5%",marginTop:"1%"}]}
          />
              <TouchableOpacity 
              onPress={props.onEdit}
              style={styles.pencilTocuh}>
                  <Image
                      source={imageConstant.pencil}
                      resizeMode='contain'
                      style={[styles.img,{tintColor:theme?.theme =='dark' ?colorConstant.white :colorConstant.black}]}
                  />
              </TouchableOpacity>
        

      </View>
        <View style={styles.bankView}>
        <Text style={styles.text2}>Bank Account Verified</Text>
        </View>
  

      <View style={styles.dataView}>
          <Text style={styles.key}>Account Number</Text>
          <Text style={[styles.value,{color:theme?.theme =='dark' ?colorConstant.white :colorConstant.black}]}>{item.AccNo}</Text>
      </View>

      <View style={styles.dataView}>
          <Text style={styles.key}>IFSC Code</Text>
          <Text style={[styles.value,{color:theme?.theme =='dark' ?colorConstant.white :colorConstant.black}]}>{item.code}</Text>
      </View>
      <View style={styles.dataView}>
          <Text style={styles.key}>Bank</Text>
          <Text style={[styles.value,{color:theme?.theme =='dark' ?colorConstant.white :colorConstant.black}]}>{item.bank}</Text>
      </View>

          <View style={[styles.rowView,{justifyContent:"space-between",marginTop:Width/20}]}>
              <TouchableOpacity 
            //   onPress={()=>alert("Default")}
              activeOpacity={0.8}
              style={styles.buttonTocuh}>
                  <Text style={[styles.key,{color:colorConstant.greenButton}]}>Set as Default</Text>
              </TouchableOpacity>
              <TouchableOpacity 
            //   onPress={()=>alert("Remove")}
              activeOpacity={0.8}
              style={styles.buttonTocuh}>
                  <Text style={[styles.key,{color:colorConstant.red}]}>Remove</Text>
              </TouchableOpacity>
          </View>

    </View>
  )
}

export default CustomBankAccountCard

const styles = StyleSheet.create({
    mainView:{
        width:"90%",
        alignSelf:"center",
        justifyContent:"center",
        padding:Width/20,
        borderRadius:Width/40,
        backgroundColor:colorConstant.white
    },
    rowView:{
        width:"100%",
        flexDirection:"row",
        alignSelf:"center",

    },
    img:{
        width:15,
        height:14,
    },
    pencilTocuh:{
        position:"absolute",
        right:10,
        top:3
    },
    text1:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.black
    },
    bankView:{
        backgroundColor:"#90ee90",
        alignSelf:"flex-start",
        padding:Width/40,
        borderRadius:Width/50,
        marginTop:Width/50
    },
    text2:{
        fontSize:12,
        fontFamily:fontConstant.regular,
        color:colorConstant.greenButton,

    },
    key:{
        fontSize:14,
        fontFamily:fontConstant.regular,
        color:colorConstant.notiTextColor
    },
    value:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.black,
        marginTop:Width/50
    },
    buttonTocuh:{
        paddingVertical:Width/40
    },
    dataView:{
        marginTop:Width/30
    }
})