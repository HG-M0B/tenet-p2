import { Text, StyleSheet, View ,Image} from 'react-native'
import React, { Component ,useContext} from 'react'
import { Height, Width } from '../dimension/dimension';
import { fontConstant,colorConstant,imageConstant } from '../utils/constant'
import ThemeContext from '../config/ThemeContext';


const CustomNotification = ({item,index}) =>{
   let theme = useContext(ThemeContext)
     return(
         <View
         style={[styles.rowView,{backgroundColor:theme?.theme=='dark' ? colorConstant.darkBlack:colorConstant.white}]}>
             <Image 
             source={item.image}
             resizeMode='contain'
             style={styles.img}
             />
             <View style={styles.innerView}>
                 <Text style={[styles.text1,{color:theme?.theme=='dark' ? colorConstant.white:colorConstant.black}]}>{item.title}</Text>
                 <Text style={styles.text2}>{item.body}</Text>
             </View>
             <Text style={styles.text3}>{item.date}</Text>
         </View>
     )
      
    
}


export default CustomNotification;

const styles = StyleSheet.create({
    rowView:{
        flexDirection:"row",
        width:"90%",
        alignSelf:"center",
        backgroundColor:colorConstant.white,
        borderRadius:Width/50,
        padding:13
    },
    innerView:{
        width:"75%",
        marginLeft:Width/40
    },
    img:{
        width:60,
        height:60
    },
    text1:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.black
    },
    text2:{
        fontSize:12,
        fontFamily:fontConstant.regular,
        color:colorConstant.notiTextColor,
        lineHeight:Width/25,
        marginTop:Width/90
    },
    text3:{
        fontSize:14,
        fontFamily:fontConstant.regular,
        color:colorConstant.notiTextColor,
        position:"absolute",
        top:Width/30,
        right:Width/30
    }
   

})