import { StyleSheet, Text, View } from 'react-native'
import React ,{useContext}from 'react'
import { colorConstant, fontConstant } from '../utils/constant'
import { Width } from '../dimension/dimension';
import ThemeContext from '../config/ThemeContext';

const CustomPortfolioCard = ({item,index}) => {
    let theme=useContext(ThemeContext);
    console.log("item--<>",item)
    let diff = item?.current_value - item?.purchased_value;
    let percentage = ((diff/item?.purchased_value)*100).toFixed(2);
    console.log(diff,percentage)
  return (
    <View style={[styles.mainView,{ borderColor:diff > 0 ? colorConstant.greenButton : colorConstant.red,backgroundColor:theme?.theme=='dark'?colorConstant.blackLight:colorConstant.white}]}>
        <View style={styles.rowView}>
            <Text style={[styles.key,{color:theme?.theme=='dark'?colorConstant.white:colorConstant.black}]}>{item?.trading_symbol}</Text>
            <Text style={[styles.key,{color:theme?.theme=='dark'?colorConstant.white:colorConstant.black}]}>{`₹${item?.purchased_value}`}</Text>
        </View>
        <View style={[styles.rowView,{marginTop:Width/50}]}>
            <Text style={styles.value1}>{`Listed In ${item?.exchange}`}</Text>
            <Text style={[styles.value2,{color: diff > 0 ? colorConstant.greenButton : colorConstant.red}]}>{`${diff.toFixed(2)}(${Math.abs(percentage)}%)`}</Text>
        </View>

        <View style={[styles.rowView,{marginTop:Width/50}]}>
            <Text style={styles.value1}>Purchased Value</Text>
            <Text style={[styles.value1,{marginLeft:'-6%'}]}>Qty</Text>
            <Text style={styles.value1}>Market Value</Text>
        </View>

        <View style={[styles.rowView,{marginTop:Width/50}]}>
            <Text style={[styles.value1,{color:theme?.theme=='dark'?colorConstant.white:colorConstant.black}]}>{`₹${item?.purchased_value}`}</Text>
            <Text style={styles.value1}>{item?.quantity}</Text>
            <Text style={[styles.value1,{color: item.color === "green"? colorConstant.greenButton : colorConstant.red}]}>{`₹${item?.current_value}`}</Text>
        </View>
    </View>
  )
}

export default CustomPortfolioCard

const styles = StyleSheet.create({
    mainView:{
        width:"90%",
        alignSelf:"center",
        backgroundColor:colorConstant.white,
        borderRadius:Width/40,
        padding:Width/18,
        borderLeftWidth:4,
       
    },
    rowView:{
        flexDirection:"row",
        justifyContent:"space-between"
    },
    key:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.black
    },
    key:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.black
    },
    value1:{
        fontSize:12,
        fontFamily:fontConstant.regular,
        color:colorConstant.notiTextColor
    },
    value2:{
        fontSize:12,
        fontFamily:fontConstant.bold,
        color:colorConstant.greenButton
    },

})