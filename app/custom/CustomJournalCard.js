import { StyleSheet, Text, View } from 'react-native'
import React ,{useContext}from 'react'
import { colorConstant, fontConstant } from '../utils/constant'
import { Width } from '../dimension/dimension'
import ThemeContext from '../config/ThemeContext'
const CustomJournalCard = ({item,index}) => {
    let theme = useContext(ThemeContext);

  return (
      <>
      <Text style={[styles.text2,{fontSize:12,marginTop:Width/20,color:theme?.theme=='dark'?colorConstant.white :colorConstant.black}]}>{item.date}</Text>
          {
              item?.test.map((item, i) => {
                  return (
                      <View style={[styles.main,{backgroundColor:theme?.theme=='dark'?colorConstant.blackLight :colorConstant.white}]} key={i}>
                          <View style={styles.rowView}>
                              <Text style={[styles.text1,{color:theme?.theme=='dark'?colorConstant.white :colorConstant.black}]}>Amount</Text>
                              <Text style={[styles.text2,{color:theme?.theme=='dark'?colorConstant.white :colorConstant.black}]}>₹17,691.25</Text>
                          </View>
                          <Text style={[styles.text3,{color:theme?.theme=='dark'?colorConstant.notiTextColor :colorConstant.black}]}>You have invested rather than spending on {`\n`}<Text style={[styles.text2,{color:theme?.theme=='dark'?colorConstant.white :colorConstant.black}]}>Pizza</Text></Text>
                      </View>
                  )
              })
          }
      </>

  )
}

export default CustomJournalCard

const styles = StyleSheet.create({
    main: {
        width: "100%",
        alignSelf: "center",
        padding: Width / 30,
        backgroundColor: colorConstant.white,
        borderRadius: Width / 40,
        marginTop:Width/30,
        shadowColor: "#000000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.21,
        shadowRadius: 7.68,
        elevation: 10
    },
    rowView: {
        flexDirection: "row",
        width: "100%",
        alignSelf: "center",
        justifyContent: "space-between",
        marginBottom: Width / 30,

    },
    text1: {
        fontSize: 20,
        fontFamily: fontConstant.bold,
        color: colorConstant.black

    },
    text2: {
        fontSize: 16,
        fontFamily: fontConstant.bold,
        color: colorConstant.black

    },
    text3: {
        fontSize: 14,
        fontFamily: fontConstant.regular,
        color: colorConstant.black,

    }
})