import { View, Text ,StyleSheet, TouchableOpacity,Image, SafeAreaView,Platform,Dimensions} from 'react-native';
import React ,{useEffect, useState,useContext}from 'react';
import {Height, Width} from '../dimension/dimension';
import { fontConstant } from '../utils/constant';



const CustomHeader = (props) => {
  return (
  <SafeAreaView style={[
        {...styles.MainView},
        {
            marginTop:Platform.OS === 'ios' ? Height/80 : Height/30,
            marginBottom:Platform.OS === 'ios' ? Height/80 : Height/60
        }]}>
        <TouchableOpacity
          activeOpacity={0.8}
              onPress={props.onPressBack ? props.onPressBack : ()=>props.navigation.goBack()}
              style={styles.BackTouch}>
            <Image source={props.img} resizeMode='contain' 
            style={[styles.Img]}
            />
        </TouchableOpacity>
      {
          props.headerText ? 
          <Text style= {styles.HeaderText}>{props.headerText}</Text>
          :<></>
      }
    
        {
            props.rightImg ?
                <TouchableOpacity
                onPress={props.rightButton}
                activeOpacity={0.8}
                // style={[styles.edit]}
                >
                    <Image
                    style = {styles.RImg}
                        source={props.rightImg}
                        resizeMode='contain'
                    />
                </TouchableOpacity>
                :
                <></>
        }

        {
            props.FrightImg ?
                <TouchableOpacity
                onPress={props.rightButton}
                activeOpacity={0.8}
                style={[styles.edit]}
                >
                    <Image
                    style = {styles.FRImg}
                        source={props.FrightImg}
                        resizeMode='contain'
                    />
                </TouchableOpacity>
                :
                <></>
        }

        {
            props.rightImg2 ?
                <TouchableOpacity
                onPress={props.rightButton}
                activeOpacity={0.8}
                // style={[styles.edit]}
                >
                    <Image
                    style = {styles.RImg2}
                        source={props.rightImg2}
                        resizeMode='contain'
                    />
                </TouchableOpacity>
                :
                <></>
        }
  </SafeAreaView>
  );
};

export default CustomHeader;

const styles  = StyleSheet.create({
    MainView: {
        width:"90%",
        alignSelf:"center",
        flexDirection:"row",
        // alignItems:"center",
        // justifyContent:"space-between",
    },
    HeaderText:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color : '#141416',
        width:"80%",
        textAlign:"center",
        // backgroundColor:'pink',
        top:2,
    },
    HeaderTextWithoutImage:{
        fontSize:18,
        fontFamily:fontConstant.bold,
        // color:colorConstant.black,
        width:"75%",
        textAlign:"center",
        position:"absolute",
        marginLeft:Width/10,

    },
    BackTouch:{
      padding:5,
      width:30,
      height:20,
      // backgroundColor:'pink',
      marginTop:Platform.OS === 'ios' ? null : 6
    },
    Img:{
      width:17,
      height:10,
    },
    RImg:{
      width:30,
      height:30,
      right:20
    },
    RImg2:{
      width:22,
      height:22,
      right:20
    },
    FRImg:{
      width:22,
      height:22,
      right:40
    },
    // edit:{
    //   padding:5
    // }
})
