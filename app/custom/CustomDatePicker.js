import React, { useState,useContext } from "react";
import { View, Text, Image, TextInput, Platform,StyleSheet ,TouchableOpacity} from "react-native";
import { colorConstant, fontConstant, imageConstant } from "../utils/constant";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import ThemeContext from "../config/ThemeContext";

const CustomDatePicker = (props) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  let theme =  useContext(ThemeContext)

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    console.warn("A date has been picked: ", date);
    hideDatePicker();
  };
  return (
    <>
      <TouchableOpacity style={[styles.touch, {
        width: props.width ? props.width : "50%",
        backgroundColor:props.backgroundColor,
        borderRadius:props.borderRadius,
        height:props.height,
        padding:props.padding,
        marginTop:props.marginTop,
        borderWidth:props.borderWidth,
        borderColor:props.borderColor,
        paddingVertical:props.paddingVertical


      }]}
        activeOpacity={1}
        onPress={props.showDatePicker}>
        {props.image ?
          <Image
            source={props.image ? props.image : imageConstant.CalenderIcon}
            resizeMode="contain"
            style={[styles.Img1]}
          />
          :
          <></>}
        <Text style={[ styles.text1,{fontFamily:props.fontFamily,color:props.color,position:props.position}]}>{props.placename}</Text>
        <Image
          source={imageConstant.angle}
          resizeMode="contain"
          style={[styles.Img2, { transform: [{ rotate: "-90deg" }],tintColor:theme?.theme=='dark'?colorConstant.white :colorConstant.black }]}
        />
      </TouchableOpacity>
      <DateTimePickerModal
        isVisible={props.isVisible}
        date={new Date()}
        mode="date"
        format="DD/MM/YYYY"
        minimumDate={props.openDatePicker}
        maximumDate={props.maximumDate}
        onConfirm={props.onClickDate}
        onCancel={props.onCancel}
        value={props.value}


      />
    </>
  );
};

export default CustomDatePicker;

const styles =  StyleSheet.create({
  touch:{
    flexDirection:"row",
    alignItems:"center",
    // justifyContent:"space-between"

  },
  Img1:{
    width:20,
    height:20,
    marginLeft:Platform.OS =='ios'? 0 :'-3%'
  },
  text1:{
    fontSize:14,
    marginLeft:"4%"

  },
  Img2:{
    width:10,
    height:10,
    position:"absolute",
    right:'5%'
  }
})
