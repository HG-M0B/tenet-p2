import { View, Text, TouchableOpacity ,StyleSheet,Platform} from 'react-native';
import React from 'react';
import {Width,Height} from '../dimension/dimension';
import { colorConstant, fontConstant } from '../utils/constant';
import { heightPercentageToDP, widthPercentageToDP } from '../utils/responsiveFIle';



const CustomButton = (props) => {
  return (
      <TouchableOpacity
        onPress={props.OnButtonPress}
        activeOpacity={0.8}
        style={[
          {...styles.ButtonTouch},
          {
              marginTop:props.marginTop,
              marginBottom:props.marginBottom,
              width: props.width ? props.width : "90%",
              position:props.position,
              bottom:props.bottom,
              top:props.top,
              backgroundColor:props.backgroundColor? props.backgroundColor: colorConstant.greenButton
          }]}
        >
        <Text style={styles.text}>{props.buttonName}</Text>
      </TouchableOpacity>
  );
};

export default CustomButton;

const styles = StyleSheet.create({
  ButtonTouch:{
   
    height : Platform.OS  === 'ios'? heightPercentageToDP('7%') : Height/15,
    alignSelf:"center",
    justifyContent:"center",
    alignItems:"center",
    borderRadius:Width/35,
    

  },
  text:{
    fontSize:Platform.OS =='ios' ? widthPercentageToDP('4.5%'):  18,
    fontFamily:fontConstant.medium,
    color:colorConstant.white
    
  }    
})
