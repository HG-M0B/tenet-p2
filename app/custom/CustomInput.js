import { View, Text ,StyleSheet, TextInput,Image} from 'react-native';
import React, { useState,useContext } from 'react';
import { Height, Width } from '../dimension/dimension';
import { imageConstant, colorConstant, fontConstant } from '../utils/constant';

const CustomInput = (props) => {
    
  return (
      <View
          style={[{...styles.MainView},{
            backgroundColor: props.backgroundColor ? props.backgroundColor : "#E6E8EC",
           marginTop:props.marginTop,
           marginBottom:props.marginBottom,
           marginVertical:10,
           width:props.width ? props.width: "90%",
           borderColor : "#0049F1",
           borderBottomWidth:1,
           paddingVertical:0,
           opacity:props.opacity
          }]}>
          { props.img ?
              <Image
                  source={props.img}
                  resizeMode='contain'
                  style={styles.Img}
              />
              :
              <></>

          }
        
          <TextInput
              keyboardType={props.keyboardType}
              placeholder={props.placeholder ? props.placeholder:'Type here'}
              placeholderTextColor={"#A9A9AC"}
              style={[styles.InputField]}
              onFocus={props.onFocus}
              ref={props.inputRef}
              onBlur={props.onBlur}
              editable={props.editable}
              value={props.value}
              secureTextEntry={props.secureTextEntry}
              onSubmitEditing={props.onSubmitEditing}
              onChangeText={(text) => props.onChangeText(text)}
            //   value={props.value}
            //   returnKeyType={props.returnKeyType}
            //   onSubmitEditing={props.onSubmitEditing}
            //   editable = {props.editable} 
              />
      </View>
  );
};

export default CustomInput;

const styles = StyleSheet.create({
    MainView:{
        alignSelf: "center",
        flexDirection: "row",
        alignItems: "center",
        borderRadius: Width / 50,
        paddingHorizontal: 7,
        
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.39,
        shadowRadius: 8.30,
        elevation: 2,


    },
    titleText:{
        fontSize:14,
        fontFamily:fontConstant.regular,
        color:"black"

    },
    InputField:{
        width:"95%",
        paddingHorizontal:Width/30,
        fontSize:16,
        fontFamily:fontConstant.medium,
        color:"black"

    },
    Img:{
        width:20,
        height:20
    }
})
