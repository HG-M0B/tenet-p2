import { StyleSheet, Text, View } from 'react-native'
import React ,{useContext}from 'react'
import { colorConstant, fontConstant } from '../utils/constant'
import { Width } from '../dimension/dimension'
import ThemeContext from '../config/ThemeContext'

const CustomInvestmentCard = ({item,index}) => {
    let theme = useContext(ThemeContext)
  return (
      <>
          <Text style={[styles.text4, { fontSize: 12, marginTop: Width / 30 ,width:"90%",alignSelf:"center",color:theme?.theme =='dark' ? colorConstant.white:colorConstant.black}]}>{item.date}</Text>
          {
              item?.test?.map((item,index) => {
                  return (
                      <View  key ={index} style={[styles.mainView, {backgroundColor:theme?.theme =='dark' ? colorConstant.black:colorConstant.white , borderColor: item.color === "green" ? colorConstant.greenButton : colorConstant.red ,marginTop:Width/30}]}>
                          <View style={styles.rowView}>
                              <Text style={[styles.key,{color:theme?.theme =='dark' ? colorConstant.white : colorConstant.black}]}>{item.StockName}</Text>
                              <Text style={[styles.key,{color:theme?.theme =='dark' ? colorConstant.white : colorConstant.black}]}>{item.Price}</Text>
                          </View>
                          <View style={[styles.rowView, { marginTop: Width / 50 }]}>
                              <Text style={styles.value1}>{item.Cat}</Text>
                              <Text style={[styles.value2, { color: item.color === "green" ? colorConstant.greenButton : colorConstant.red }]}>{item.Value}</Text>
                          </View>

                          <View style={[styles.rowView, { marginTop: Width / 50 }]}>
                              <Text style={styles.value1}>Purchased Value</Text>
                              <Text style={[styles.value1, { marginLeft: '-6%' }]}>Qty</Text>
                              <Text style={styles.value1}>Market Value</Text>
                          </View>

                          <View style={[styles.rowView, { marginTop: Width / 50 }]}>
                              <Text style={[styles.value1,{color:theme?.theme =='dark' ? colorConstant.white : colorConstant.black}]}>{item.Pvalue}</Text>
                              <Text style={styles.value1}>{item.Qty}</Text>
                              <Text style={[styles.value1, { color: item.color === "green" ? colorConstant.greenButton : colorConstant.red }]}>{item.Mvalue}</Text>
                          </View>
                      </View>
                  )
              })
          }
      </>
   
  )
}

export default CustomInvestmentCard

const styles = StyleSheet.create({
    mainView:{
        width:"90%",
        alignSelf:"center",
        backgroundColor:colorConstant.white,
        borderRadius:Width/40,
        padding:Width/18,
        borderLeftWidth:4,
       
    },
    rowView:{
        flexDirection:"row",
        justifyContent:"space-between"
    },
    key:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.black
    },
    key:{
        fontSize:16,
        fontFamily:fontConstant.bold,
        color:colorConstant.black
    },
    value1:{
        fontSize:12,
        fontFamily:fontConstant.regular,
        color:colorConstant.notiTextColor
    },
    value2:{
        fontSize:12,
        fontFamily:fontConstant.bold,
        color:colorConstant.greenButton
    },
    text4: {
        fontSize: 16,
        fontFamily: fontConstant.bold,
        color: colorConstant.black

    }

})