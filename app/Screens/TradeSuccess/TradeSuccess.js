import { Image, StyleSheet, Text, View ,Share,Linking,BackHandler} from 'react-native'
import React, { useEffect, useState,useCallback } from 'react'
import { fontConstant, imageConstant } from '../../utils/constant'
import { Height } from '../../dimension/dimension'
import { TouchableOpacity } from 'react-native'
import { useSelector } from 'react-redux'
import Clipboard from '@react-native-clipboard/clipboard';
import toastShow from '../../utils/Toast'
import { useFocusEffect } from '@react-navigation/native'
const TradeSuccess = (props) => {
    let {navigation,route}  = props;

    const [button,setButton] =  useState(1);
    const [price,setPrice] = useState(null);
    const [copy,setCopy] = useState("Copy")
   

    const {profileData} =  useSelector(state=>state.reducers);
    useEffect(()=>{
        let subs = navigation.addListener('focus',()=>{
            if(route?.params?.data){
                setPrice(route?.params?.data);
            }
           
        });
     
    },[]);

    useFocusEffect(
        React.useCallback(() => {
          const backAction = () => {
            props.navigation.navigate("Portfolio");
        
            return true;
          };
      
          const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
          );
      
          return () => backHandler.remove();
        }, [])
      );


    const copyToClipboard = () => {
        Clipboard.setString(profileData?.user_referral_code);
        setCopy("Copied");
        setTimeout(()=>{
          setCopy("Copy")
        },1500)
        // toastShow("Referral copied!!","transparent");
        
      };


      const shareReferCode = async ()=>{
        let messageTemp = `🚀 ${profileData?.user_name} has invited you to join ⏳ Tenet.\n\n
        Earn money from your opinion on Real life events like Cricket,Politics & Stocks.Use my referral code to sign-up & get a referral bonus of upto Rs. 15/-*Referral Code*: ${profileData?.user_referral_code}.
        \n\n 📍Download Tenet : https://tenetapp.in/`
        const shareOptions = {
          message: messageTemp,
            url:imageConstant.logo2
        }
        try {
          const shareResponse = await Share.share(shareOptions);
          console.log("result",shareResponse)
        } catch (error) {
          alert(error.message);
        }
    
      }
    
      const shareReferCodeOverWhatsapp = async () => {
        let messageTemp = `🚀 ${profileData?.user_name} has invited you to join ⏳ Tenet.\n\n
        Earn money from your opinion on Real life events like Cricket,Politics and Stocks.Use my referral code to sign-up and get a referral bonus of upto Rs. 15/-*Referral Code*: ${profileData.user_referral_code}.
        \n\n 📍Download Tenet : https://tenetapp.in/`
        
        let url = `whatsapp://send?text=${messageTemp}`
        
        try {
         await Linking.openURL(url)
            .then(() => console.log("WhatsApp Opened successfully"))
            .catch(() => {
              Linking.openURL("https://play.google.com/store/apps/details?id=com.whatsapp")
            });
        }
        catch (error) {
          console.log("error", error)
        }
    
      }
    
  return (
    <View style={styles.mainView}>

      <View style={{
        width: "90%",
        alignSelf: "center",
        alignItems: "flex-end",
        marginTop: 20
      }}>
        <TouchableOpacity onPress={() => {
          props.navigation.navigate("Portfolio");
        }}>
          <Image
            source={imageConstant.plus}
            style={{
              width: 25,
              height: 25,
              tintColor: "#FFFFFF",
              transform: [{ rotate: '130deg' }]
            }} />
        </TouchableOpacity>
      </View>

      <View style={{
        marginTop: Height * 0.20,
        width: "70%",
        alignSelf: "center"
      }}>
        <Text style={{
          fontSize: 18,
          color: "#FFFFFF",
          fontFamily: fontConstant.medium,
          textAlign: "center"
        }}>Sucess</Text>
        <Text style={{
          fontSize: 16,
          color: "#FFFFFF",
          marginTop: 6,
          fontFamily: fontConstant.regular,
          textAlign: "center"
        }}>{`Your investment of Rs.${price} has been made successfully`}</Text>
      </View>
      <Image
        source={imageConstant.right}
        style={{
          width: 50,
          height: 50,
          tintColor: "#FFFFFF",
          alignSelf: "center",
          marginTop: 20
        }}
      />

      <View style={{
        width: "90%",
        alignSelf: "center",
        position: "absolute",
        bottom: 20
      }}>
        <Text style={{
          fontSize: 16,
          color: "#FFFFFF",
        }}>Earn ₹ 10 with each referral</Text>




        <View style={{
          marginTop: 15,
          borderRadius: 10,
          borderStyle: "dashed",
          borderColor: "#FFFFFF",
          borderWidth: 2.4,
          flexDirection: "row",
          height: 60,
          paddingHorizontal: "9%",
          alignItems: "center",
          justifyContent: "space-between",
        }}>
          <Text style={{
            fontSize: 16,
            color: "#FFFFFF",
            fontFamily: fontConstant.medium,
          }}>
            {profileData.user_referral_code}
          </Text>


          <TouchableOpacity
            onPress={copyToClipboard}
            activeOpacity={0.4}
            style={{
              backgroundColor: "#FFFFFF",
              opacity: 0.4,
              paddingHorizontal: 15,
              borderRadius: 5,
              paddingVertical: 5,
              position: "absolute",
              right: '9%'

            }}>
            <Text style={{
              fontSize: 16,
              color: "#FFFFFF",
            }}>{copy}</Text>
          </TouchableOpacity>

          <View>

          </View>

        </View>



        <View style={{
          flexDirection: "row",
          marginTop: 15,
          justifyContent: "space-between",
        }}>

          <TouchableOpacity

            onPress={() => {
              setButton(1);
              shareReferCodeOverWhatsapp();
            }}
            style={button == 1 ? styles.selectedButton : styles.button}>
            <Image
              source={imageConstant.whatsapp}
              style={{
                width: 25,
                height: 25,
                tintColor: button == 1 ? "#024BE8" : "#FFFFFF",
              }} />
            <Text style={button == 1 ? styles.selectedText : styles.text}>Whatsapp</Text>
          </TouchableOpacity>


          <TouchableOpacity
            onPress={() => {
              setButton(2);
              shareReferCode();
            }}
            style={button == 2 ? styles.selectedButton : styles.button}>
            <Image
              source={imageConstant.share}
              style={{
                width: 20,
                height: 20,
                tintColor: button != 1 ? "#024BE8" : "#FFFFFF",
              }} />
            <Text style={button != 1 ? styles.selectedText : styles.text}>More Option</Text>
          </TouchableOpacity>
        </View>

      </View>
    </View>
  )
}

export default TradeSuccess

const styles = StyleSheet.create({
    mainView:{
        flex:1,
        backgroundColor:"#024BE8"
    },
    selectedButton:{
        flexDirection:"row",
        alignItems:"center",
        width:"48%",
        justifyContent:"space-evenly",
        backgroundColor:"#FFFFFF",
        borderRadius:5,
        borderWidth:1,
        borderColor:"#FFFFFF",
        paddingVertical:7
    },
    button:{
        flexDirection:"row",
        alignItems:"center",
        width:"48%",
        justifyContent:"space-evenly",
        borderWidth:1,
        borderColor:"#FFFFFF",
        borderRadius:5,
        paddingVertical:7
    },
    selectedText :{
        fontSize:16,
        color:"#024BE8"
    },
    text :{
        fontSize:16,
        color:"#FFFFFF"
    }
})