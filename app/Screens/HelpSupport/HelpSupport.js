import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList
} from 'react-native';
import React, {Component, useRef, useState} from 'react';


import CustomHeader from '../../custom/CustomHeader';
import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';


const windowWidth = Dimensions.get('window').width;
const layout=windowWidth-20


const HelpSupport = props => {
  const {route, navigation} = props;

  const notiArray = [
    {
        title :'I have a Issue',
        ticket:'#21543',
        date:'22 Feb 21, 11:45 Am',
        description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet.',
        status:'Pending for Admin'
    },
    {
      title :'I have a Issue',
      ticket:'#21544',
      date:'22 Feb 21, 11:45 Am',
      description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet.',
      status:'Resolve'
    },
    {
      title :'I have a Issue',
      ticket:'#21545',
      date:'22 Feb 21, 11:45 Am',
      description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet.',
      status:'Pending for Admin'
    },
    {
      title :'I have a Issue',
      ticket:'#21546',
      date:'22 Feb 21, 11:45 Am',
      description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet.',
      status:'Resolve'
    },
    {
      title :'I have a Issue',
      ticket:'#21547',
      date:'22 Feb 21, 11:45 Am',
      description:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet.',
      status:'Pending for Admin'
    },
  ]

  return (
    // <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
      <View style={{backgroundColor: '#FFFFFF'}}>
      <CustomHeader
        headerText={'Help & Support'}
        img={imageConstant.back}
        navigation={props.navigation}
      />
      <View style ={{borderBottomWidth:1, borderBottomColor:'#E6E8EC'}}></View>

      <View style={{width:"95%",height:"100%",alignSelf:'center'}}>
        <TouchableOpacity
            style={{height:20,width:150,marginLeft:'auto',marginTop:10,marginBottom:15}}
            activeOpacity={0.8}
            // onPress={() => Alert.alert('hiiii')}
            onPress={() => navigation.navigate('CreateTicket')}
        >
        <Text style={{fontSize:14, fontWeight:'400',fontFamily:fontConstant.regular,paddingLeft:15,color:'#004BEB'}}>+ Create New Ticket</Text>
        </TouchableOpacity>

        <View style={{backgroundColor:'white',width:"95%",height:"100%",alignSelf:'center'}}>
        <FlatList 
          // horizontal
          showsVerticalScrollIndicator = {false}
          contentContainerStyle={{
            paddingBottom:110
          }}
          data ={notiArray} 
          renderItem = {(d) => {
              return <View style={{flexDirection:'row',backgroundColor:d.item.status == 'Pending for Admin'?'rgba(237, 90, 80, 0.05)': 'rgba(8, 140, 133, 0.05)',padding:10,alignItems:'center',borderRadius:8,marginBottom:14}}>
        
                <View style={{flexDirection:'column',width:"100%",paddingLeft:5}}>
                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                <View>
                <Text style={styles.titleStyle}>{d.item.title} </Text>
                <Text style={styles.dateStyle}>{d.item.date}</Text>
                </View>
                <View>
                <Text style={styles.ticketStyle}>Ticket Id - {d.item.ticket} </Text>
                </View>
                </View>
                <Text style={styles.textStyle}>{d.item.description}</Text>
                <View style={{width:'60%',height:25,flexDirection:'row',marginTop:15,}}>
                <Image source={d.item.status == 'Pending for Admin'? imageConstant.pending : imageConstant.resolved} resizeMode='contain' 
                  style={[styles.Img]}
                />
                <Text style={{
                  fontSize:14,
                  fontWeight:'400',
                  color : d.item.status == 'Pending for Admin'?'#ED5A50': '#088C85'
                  }}>
                  {d.item.status}</Text>
                </View>
                </View>
              </View>
          }}
          />
        </View>

      </View>
      </View>
    // </SafeAreaView>
  )
}

export default HelpSupport

const styles = StyleSheet.create({
  textStyle: {
    marginTop:10,
    color: "#000000",
    fontSize:14,
    fontWeight:'400',
    fontFamily:fontConstant.regular,
    lineHeight:22
  },
  titleStyle: {
    marginBottom:5,
    color: "#000000",
    fontSize:18,
    fontWeight:'500',
    fontFamily:fontConstant.regular,
  },
  ticketStyle: {
    marginTop:5,
    color: "#000000",
    fontSize:13,
    fontWeight:'400',
    fontFamily:fontConstant.regular,
  },
  dateStyle:{
    color: "#353945",
    fontSize:12,
    fontWeight:'400',
    fontFamily:fontConstant.regular
  },
  viewStyle : {
    flexDirection:'row'
  },
  Img:{
    width:18,
    height:18,
    marginRight:10
  }
})