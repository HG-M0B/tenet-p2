import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions
} from 'react-native';
import React, {Component, useEffect, useRef, useState} from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
  heightPercentageToDP,
} from 'react-native-responsive-screen';
import {fontConstant, imageConstant} from '../../utils/constant';
import toastShow from '../../utils/Toast';

const windowWidth = Dimensions.get('window').width;

const layout=windowWidth-40

const Login = ({ navigation }) => {
  const [value, setValue] = useState('');
  const [buttonEnable,setButtonEnable] =  useState(false)
  const [number,setNumber]= useState("");

  useEffect(()=>{
    if(number.length == 10)
    {
      setButtonEnable(true);
    }
    else
    {
      setButtonEnable(false);
    }
  },[number])

console.log("number",number)
  return (

    <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
      <View style={{ backgroundColor: '#FFFFFF', flex: 1, top: Platform.OS === 'ios' ? null : 10 }}>
        <View style={{ width: windowWidth, minHeight: 40, maxHeight: 40, justifyContent: 'center', alignItems: 'center', }}><Text style={styles.loginStyle}>Login</Text></View>
        <View
          style={{
            marginTop: 20,
            alignItems: 'center'
          }}>
          <Image
            style={styles.imageStyle}
            source={require('../../assets/images/logo.png')}
          />
        </View>



        <View style={{
          marginTop: 50,
          flexDirection: "row",
          width: "90%",
          alignSelf: "center"
        }}>

          <View style={{
            flexDirection: "row",
            borderRadius: 5,
            borderBottomColor: "#d3d3d3",
            borderBottomWidth: 1,
            alignItems: "center",
            paddingHorizontal: 10,
            backgroundColor: "#d3d3d3",


          }}>
            <Image
              resizeMode='contain'
              source={imageConstant.flag}
              style={{
                width: 20,
                height: 20,
                borderRadius: 10
              }}
            />
            <Text style={{
              fontSize: 16,
              fontFamily: fontConstant.medium,
              marginLeft: 5,
              color: "#000000"
            }}>+91</Text>
          </View>
          <TextInput
            placeholder='Enter your mobile number'
            placeholderTextColor={"#E6E8EC"}
            value={number}
            maxLength={10}
            onChangeText={(text) => setNumber(text)}
            keyboardType="numeric"
            style={{
              fontSize: wp('4%'),
              color: "#000000",
              fontFamily: fontConstant.medium,
              marginLeft: 4,
              width: "73%",
              alignSelf: "center",
              borderBottomWidth: 2,
              borderBottomColor: "#E6E8EC"
            }} />
        </View>






        <View>
          <TouchableOpacity
            activeOpacity={0.8}
            style={{
              backgroundColor: buttonEnable ? '#004BEB' : "#E5E4E2",
              width: '92%',
              height: 50,
              alignSelf: 'center',
              padding: 9,
              marginTop: 20,
              borderRadius: 25,
              borderColor: 'rgba(255, 255, 255, 0.4)',
              shadowOpacity: 1,
              shadowRadius: 3,
              shadowOffset: { height: 0.2, width: 0.2 }
            }}
            // disabled = {!number}

            onPress={
              () => buttonEnable ? (
                navigation.navigate("Otp", {
                  number: number
                })
              ) : null
              //   () => {
              //   if (number == "") {
              //     toastShow("Please enter your number", "red")
              //   }
              //   else if (number.length != 13) {
              //     toastShow("Please enter valid mobile number", "red")
              //   }
              //   else {
              //     navigation.navigate("Otp", {
              //       number: number
              //     })
              //   }
              // }
            }>
            <Text style={styles.buttonStyle}>Send OTP</Text>
          </TouchableOpacity>
        </View>

        <View>
          <View style={{ flexDirection: 'row', marginTop: 20, alignSelf: 'center' }}>
            <Text style={{ fontWeight: '400', fontSize: 13, color: '#6F7C98' }}>By proceeding, you agree to </Text>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate("TermsCondition")
              }}
            >
              <Text style={{ fontWeight: '400', fontSize: 13, color: 'rgba(0, 100, 247, 1)' }}>Terms & Conditions</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 5, alignSelf: 'center' }}>
            <Text style={{ fontWeight: '400', fontSize: 13, color: '#6F7C98' }}>and </Text>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate("PrivacyPolicy")
              }}
            >
              <Text style={{ fontWeight: '400', fontSize: 13, color: 'rgba(0, 100, 247, 1)' }}>Privacy Policy</Text>
            </TouchableOpacity>
          </View>

        </View>

      </View>
    </SafeAreaView>
  )
};

const styles = StyleSheet.create({
  loginStyle: {
    fontSize: 16,
    color: '#141416',
    fontFamily: fontConstant.bold,
  },
  textStyle: {
    fontSize: 16,
    color: '#000000',
    fontFamily: fontConstant.regular,
    textAlign: 'center',
    marginVertical:30,
    alignSelf:'center'
  },
  imageStyle: {
    height: 64,
    width: 64,
   
  },
  textBoxStyle: {
    height: 30,
    width: 300,
    left: '30%',
    marginTop: 60,
    fontSize: 20,
    color: '#777E91',
  },
  buttonStyle: {
    marginTop: 5,
    alignSelf: 'center',
    color: '#FFFFFF',
    fontFamily: fontConstant.bold,
    fontSize: 16,
  },
});

export default Login;
