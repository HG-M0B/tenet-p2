import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList,
  Modal,
  StatusBar,
  Keyboard,
  Linking,
  BackHandler
} from 'react-native';
import React, {Component, useRef, useState, useEffect} from 'react';

import { useFocusEffect } from '@react-navigation/native';
import CustomHeader from '../../custom/CustomHeader';
import { colorConstant, imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'
import { useSelector,useDispatch } from 'react-redux';
import { baseURL } from '../../utils/URL';
import toastShow from '../../utils/Toast';
import { useIsFocused } from "@react-navigation/native";
import {actions} from '../../redux/reducers';
import Loader from '../../utils/loader';
import {
  CFCallback,
  CFErrorResponse,
  CFPaymentGatewayService,
} from 'react-native-cashfree-pg-sdk';
import {
  CFDropCheckoutPayment,
  CFEnvironment,
  CFPaymentModes,
  CFSession,
  CFThemeBuilder,
  CFPaymentComponentBuilder
} from 'cashfree-pg-api-contract';


const windowWidth = Dimensions.get('window').width;
const layout=windowWidth-20

const Wallet = props => {
  const {route, navigation} = props;
  const amount=route.amount;
  const [fundData,setFundData] =  useState(null);
  const [modalPadding, setModalPaddings] = useState(0);
  const [kycFlag,setFlag] = useState(false);
  const [addMoney, setAddmMoney] = useState("50");
  const [withdrawMoney, setWithdrawMoney] = useState("100");
  const [responseText,setResponseText] = useState('');
  const [loader,setLoader] = useState(false);
  const [addMoneyModalVisible, setAddmMoneyModalVisible] = useState(false);
  const [withdrawMoneyModalVisible, setWithdrawMoneyModalVisible] = useState(false);
  const [transactionDetails,setTransactionDetails] =  useState(null);
  const {jwtToken,appState,walletBalance} = useSelector((state)=>state.reducers)
  const dispatch = useDispatch()
  const isFocused =  useIsFocused();

  useEffect(()=> {
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      (e) => {
        // setPadding(0)
        if(Platform.OS == 'android') {
          setModalPaddings(0)
        }
        else {
          setModalPaddings(e.endCoordinates.height)
        }
      }
    );
    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        setModalPaddings(0)
      }
    );
    setWithdrawMoneyModalVisible(false);
    setWithdrawMoney("100");
    callMainApi();
    getFunds();
    getTransactionsDetails();
    CFPaymentGatewayService.setCallback({
      onVerify(orderID) {
        toastShow("Payment Successful",colorConstant.green)
        getFunds();
        getTransactionsDetails();
        changeResponseText('orderId is :' + orderID);
      },
      onError(error, orderID) {
        let err = error?.message?.charAt(0).toUpperCase() + error?.message?.slice(1);
        toastShow(err,"red")
        changeResponseText('exception is : ' + JSON.stringify(error) + '\norderId is :' + orderID)
      },
    });
    return () => {
        keyboardDidHideListener.remove();
        keyboardDidShowListener.remove();
        CFPaymentGatewayService.removeCallback();
    };

  },[]);


  useFocusEffect(
    React.useCallback(() => {
      const backAction = () => {
       props.navigation.goBack();
        return true;
      };
  
      const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
      );
  
      return () => backHandler.remove();
    }, [])
  );

  useEffect(()=>{
    let subs =  navigation.addListener('focus',()=>{
      getFunds();
      getTransactionsDetails();
      setAddmMoney("50");
      checkKycVerification();
      
    })
    return ()=> subs;
  },[appState])
    
// ************Api's calling***************************


const callMainApi = async () => {
  try {
    let response = await fetch(baseURL + `maintenance/`);
    if (response && response.status == 200) {
      let jsonData = await response.json();
      if (jsonData?.maintenance) {
        dispatch(actions.setIntroStatus("check"));
      }
    }
  }
  catch (error) {
    console.log("error-->", error)
  }
}
  const renderTransactionDetails = ({item,index}) => {
    return (
      <View>
        <Text style={styles.dateStyle}>{item?.date}</Text>
        {
          item?.transactions?.map((item,index)=>{
            return(
              <View 
              key = {index}
              style={{
                width: layout,
                justifyContent:'flex-end',
                backgroundColor: 
                item.transaction_type?.includes("CREDIT") && !item.transaction_type?.includes("REFERRAL") 
                ? '#08A02A'  
                : item.transaction_type?.includes("REFERRAL") 
                ? "#F0E68C" 
                : item.transaction_type?.includes("DEBIT") || item.transaction_type?.includes("WITHDRAWN")
                ? '#fad6a5'
                : null,
                alignSelf: 'center',
                borderRadius: 8,
                borderColor: '#CDD1D9',
                borderWidth: 0.5,
                marginTop: 10
              }}>
                <View style={{ width: '15%', opacity:1}}>
                </View>
                <View style={{ flexDirection: 'row', width: "90%", alignSelf: 'center', justifyContent: 'space-between', marginTop: 15,marginBottom:15 }}>
                  <Text numberOfLines={3} style={[{...styles.textStyle},{
                    color: item.transaction_type?.includes("CREDIT") ? colorConstant.white : colorConstant.blackText
                  }]}>{
                  item.transaction_type?.includes("REFCREDIT")
                  ? `Refund: ${item.event_title?item.event_title:item.transaction_type} `
                  :item.transaction_type?.includes("REFERRAL USER CREDIT") 
                  ? `User referral bonus`
                  :  item.transaction_type?.includes("JOINING CREDIT") 
                  ? `Joining Bonus`
                  : item.transaction_type?.includes("WINCREDIT")
                  ? `Winning: ${item.event_title?item.event_title:item.transaction_type}`
                  :  item.transaction_type?.includes("CREDIT")  
                  ? `Credit: ` 
                  : item.transaction_type?.includes("DEBIT")
                  ? `Debit: ${item.event_title?item.event_title:item.transaction_type} `
                  :item.transaction_type?.includes("WITHDRAWN")
                  ? 'Withdrawn to UPI'
                  : null
                  }</Text>
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color:
                    item.transaction_type?.includes("CREDIT") && !item.transaction_type?.includes("REFERRAL") 
                        ? colorConstant.white
                        : item.transaction_type?.includes("REFERRAL")
                          ? "#772F1A"
                          : item.transaction_type?.includes("DEBIT") || item.transaction_type?.includes("WITHDRAWN")
                            ? 'red'
                            : null
                  }}>{
                    item.transaction_type?.includes("CREDIT")  
                  ? `+ ₹${item?.amount}` 
                  :  item.transaction_type?.includes("REFERRAL") 
                  ? `+ ₹${item?.amount}`
                  : item.transaction_type?.includes("DEBIT") || item.transaction_type?.includes("WITHDRAWN")
                  ? `- ₹${item?.amount}`
                  : null
          }</Text>
                  
                </View>
              </View>
            )
          })
        }
      </View>
    )
  }
  const getFunds = async () => {
    try {
      setLoader(true);
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL + 'funds/',
        {
          headers: {
            'Authorization': tokenSent,
            'Accept': 'application/json',
            'Content-Type': 'application/json',

          }
        })
      
      if (response && response.status === 200) {
        let jsonData = await response.json();
         dispatch(actions.setWalletBalance(jsonData));
        setFundData(jsonData);
        

      }
      if (response.status == 401) {
        const jsonData = await response.json()
        dispatch(actions.setIntroStatus("login"));
        toastShow(jsonData.detail, "red");

      }
    }
    catch (error) {
      toastShow(error.message, "red")
      setLoader(false);
    }
  }

  const addFunds = async () => {
    setAddmMoneyModalVisible(!addMoneyModalVisible);
    setAddmMoney("50");
    try {
      let reqData = {
        amount : addMoney
      }
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL + 'funds/add',{
        method : "POST",
          headers: {
            'Authorization': tokenSent,
            'Accept': 'application/json',
            'Content-Type': 'application/json',

          },
        body : JSON.stringify(reqData)
        
      })
      
      console.log(response);
      if (response && response.status === 200) {
        let jsonData = await response.json();
        verifyLink(jsonData?.data?.order_id,jsonData?.data?.payment_session_id);

      }else{
        
      }
    }
    catch (error) {
      
      toastShow(error.message, "red")
    }
  }


  const verifyLink = async (orderID,sessionID) => {
    try {
      let reqData = {
        order_id: orderID
      }
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL + 'funds/verify',{
        method : "POST",
          headers: {
            'Authorization': tokenSent,
            'Accept': 'application/json',
            'Content-Type': 'application/json',

          },
        body : JSON.stringify(reqData)
        
      });
      if(response && response.status === 200)
      {
        let jsonData = await response.json();
        if(jsonData.status == 200)
        {
          openPaymentOptions(orderID,sessionID);
        } 
      }
    }
    catch (error) {
      
      toastShow(error.message, "red")
    }
  }

  const getTransactionsDetails = async () => {
    try {

      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL+'funds/transactions/',{
        method : "GET",
          headers: {
            'Authorization': tokenSent,
            'Accept': 'application/json',
            'Content-Type': 'application/json',

          },
       
        
      })
      if (response && response.status === 200) {
        let jsonData = await response.json();
        console.log(jsonData.results);
        setTransactionDetails(jsonData?.results)
        setLoader(false);
      }
    }
    catch (error) {
      toastShow(error.message,"red")
      setLoader(false);
    }
  }

  const openPaymentOptions = async (orderID,payment_session_id)=>{
    const theme = new CFThemeBuilder()
    .setNavigationBarBackgroundColor('#024BE8')
    .setNavigationBarTextColor('#FFFFFF')
    .setButtonBackgroundColor('#024BE8')
    .setButtonTextColor('#FFFFFF')
    .setPrimaryTextColor('#212121')
    .setSecondaryTextColor('#757575')
    .build();
    const paymentModes = new CFPaymentComponentBuilder()
    .add(CFPaymentModes.CARD)
    .add(CFPaymentModes.UPI)
    .add(CFPaymentModes.NB)
    .add(CFPaymentModes.WALLET)
    .add(CFPaymentModes.PAY_LATER)
    .build();
    try {
      const session = new CFSession(
        payment_session_id, 
        orderID,
        CFEnvironment.PRODUCTION
      );
      const dropPayment = new CFDropCheckoutPayment(session, paymentModes, theme);
      CFPaymentGatewayService.doPayment(dropPayment)
    } catch (error) {
      toastShow(error,"red")
    }
  }

  const changeResponseText = (message) => {
    setResponseText(message)
  };

  const checkKycVerification= async()=>{
    try {
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL + "withdrawn/kyc",
        {
          headers: {
            'Authorization': tokenSent,
            'Accept': 'application/json',
            'Content-Type': 'application/json',

          }
        })
      
      
      if (response && response.status === 200) {
        let jsonData = await response.json();

        if(jsonData && jsonData.status == 200)
        {
          if(jsonData.message!=="KYC Not Done Yet!"){
            setFlag(true);
          }
        }
        else
        {
          toastShow(jsonData?.message,"red")
        }
        
      }
      else {
        let jsonData = await response.json();
        toastShow(jsonData?.message, "red");
        
      }
    }
    catch (error) {
      toastShow(error?.message ? error?.message : "Something went wrong","red")
      setLoader(false);
    }
  }


  return (
    <>
      <SafeAreaView style={{ flex: 0, backgroundColor: '#0049F1' }}>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF' }}>

        {loader && <Loader  data={loader} />}
        <View style={{ backgroundColor: '#0049F1', width: '100%', height: 55, alignSelf: 'center', flexDirection: 'row', }}>
          <TouchableOpacity
            style={{
              top: 15,
              position: 'absolute',
              height: 36,
              width: 36,
              left: 15,
              zIndex: 1
            }}
            onPress={() => {
              navigation.toggleDrawer()
            }}>
            <Image source={imageConstant.whiteOptions}
              style={[styles.optionImageStyle]}
            />
          </TouchableOpacity>
          <Text style={{ fontWeight: '700', fontSize: 16, padding: 12, textAlign: 'center', width: '100%', color: '#FFFFFF', marginTop: Platform.OS === 'ios' ? null : 10, }}>Wallet</Text>
          {/* <View style={{flexDirection: 'row',width:92,position:'absolute',right:16,marginTop:Platform.OS === 'ios'?null :10,}}> */}
          {/* <TouchableOpacity
            onPress={() => {
              navigation.navigate("Notifications")
            }}>
            <Image source={imageConstant.whiteBell}
              style={[styles.bellImageStyle]}
            />
          </TouchableOpacity> */}

          {/* </View> */}
        </View>




        <View style={{ backgroundColor: '#0049F1', width: '100%', padding: 10 }}>
          <View style={{ marginTop: 10 }}>
            <Text style={{ color: '#FFFFFF', fontWeight: '700', fontSize: 40, textAlign: 'center', fontFamily: fontConstant.regular }}>
            ₹{Math.abs(walletBalance ?? 0)?.toFixed(2) > 40000 ? Math.sign(walletBalance ?? 0)*((Math.abs(walletBalance ?? 0)/1000).toFixed(2)) + 'k' : (Math.sign(walletBalance ?? 0)*Math.abs(walletBalance ?? 0))?.toFixed(2)}
              {/* {`₹${walletBalance ?? 0}`} */}
              </Text>
            <Text style={{ color: '#FFFFFF', fontWeight: '400', fontSize: 16, textAlign: 'center', fontFamily: fontConstant.regular, marginTop: 5 }}>Balance</Text>
          </View>

          <View style={{ width: '100%', marginTop: 20, flexDirection: 'row', justifyContent: "space-between" }}>

            <TouchableOpacity style={styles.buttonStyle}
              onPress={() => {
                setAddmMoneyModalVisible(!addMoneyModalVisible)
              }}
            >
              <Image source={imageConstant.plus}
                style={[styles.plusImageStyle]}
              />
              <Text style={{
                color: '#088C85', fontWeight: '700', fontSize: 16, fontFamily: fontConstant.regular,
                marginLeft: 5
              }}>Add Money</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.buttonStyle}
              onPress={() => {
                if (kycFlag) {
                  setWithdrawMoney("100")
                  setWithdrawMoneyModalVisible(!withdrawMoneyModalVisible)
                }
                else {
                  navigation.navigate("KycVerification");
                  toastShow("Please add your account to proceed","red")
                }

              }}
            >
              <Image source={imageConstant.minus}
                style={[styles.plusImageStyle]}
              />
              <Text style={{
                color: '#EAA666', fontWeight: '700', fontSize: 16, fontFamily: fontConstant.regular,
                marginLeft: 5
              }}>Withdraw</Text>
            </TouchableOpacity>

          </View>

        </View>

        <View style=
          {{
            width: '92%',
            alignSelf: 'center',
            marginTop: 15,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>

          <View style={styles.tabStyle}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.tabHeading}>Winnings</Text>
            </View>
            <Text style={styles.tabValue}>
            {`₹${Math.abs(fundData?.winnings ?? 0)?.toFixed(2) > 999 ? Math.sign(fundData?.winnings ?? 0)*((Math.abs(fundData?.winnings ?? 0)/1000).toFixed(2)) + 'k' : (Math.sign(fundData?.winnings ?? 0)*Math.abs(fundData?.winnings ?? 0))?.toFixed(2)}`}
              </Text>
          </View>

          <View style={styles.tabStyle}>
            <Text style={styles.tabHeading}>Promotional</Text>
            <Text style={styles.tabValue}>
            ₹{Math.abs(fundData?.promotional ?? 0)?.toFixed(2) > 999 ? Math.sign(fundData?.promotional ?? 0)*((Math.abs(fundData?.promotional ?? 0)/1000).toFixed(2)) + 'k' : (Math.sign(fundData?.promotional ?? 0)*Math.abs(fundData?.promotional ?? 0))?.toFixed(2)}
              </Text>
          </View>

          <View style={styles.tabStyle}>
            <Text style={styles.tabHeading}>Deposits</Text>
            <Text style={styles.tabValue}>
            ₹{Math.abs(fundData?.deposits ?? 0)?.toFixed(2) > 999 ? Math.sign(fundData?.deposits ?? 0)*((Math.abs(fundData?.deposits ?? 0)/1000).toFixed(2)) + 'k' : (Math.sign(fundData?.deposits ?? 0)*Math.abs(fundData?.deposits ?? 0))?.toFixed(2)}
              </Text>
          </View>

        </View>

        <View style={{ width: '100%', borderBottomWidth: 3, borderBottomColor: '#E6E8EC', marginTop: 20 }}>
        </View>

        <ScrollView showsVerticalScrollIndicator={false}
          contentContainerStyle={
            {
              paddingBottom: 10
            }
          }
        >
          <View style={{ flexDirection: 'row', width: '92%', alignSelf: 'center', marginTop: 15, justifyContent: 'space-between' }}>
            <Text style={{ color: '#1A1A1A', fontWeight: '600', fontSize: 14, fontFamily: fontConstant.regular }}>Transaction Details</Text>
     
          </View>

          <FlatList
  

            showsVerticalScrollIndicator={false}
            data={transactionDetails}
            renderItem={renderTransactionDetails}
          />
        </ScrollView>



        <Modal
          animationType="slide"
          transparent={true}
          visible={addMoneyModalVisible}
          supportedOrientations={['portrait', 'landscape']}
          onRequestClose={() => {
            setAddmMoneyModalVisible(!addMoneyModalVisible)
          }}>
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => { setAddmMoneyModalVisible(!addMoneyModalVisible) }}
            style={styles.centeredView}>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => { }}
              style={{
                bottom: modalPadding,
                position: 'absolute',
                // margin: 20,
                backgroundColor: "white",
                borderTopStartRadius: 20,
                borderTopEndRadius: 20,
                padding: 20,
                // alignItems: "center",
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 2
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
                width: '100%',
                height: 381,
              }}>
              <TouchableOpacity
                onPress={() => {
                  setAddmMoney("50");
                  setAddmMoneyModalVisible(!addMoneyModalVisible)
                }
                }
                style={{
                  right: 10,
                  top: 10,
                  position: 'absolute'
                }}
              >
                <Image
                  style={styles.cancelButtonStyle}
                  source={imageConstant.cancelImg}
                />
              </TouchableOpacity>

              <Text style={styles.modalHeadingStyle}>Add Money</Text>

              <View style={{ flexDirection: 'row', marginTop: 30 }}>
                <Text style={{ color: '#777E91', fontWeight: '400', fontSize: 16, fontFamily: fontConstant.regular }}>Balance : </Text>
                <Text style={{ color: '#141416', fontWeight: '500', fontSize: 16, fontFamily: fontConstant.bold }}>{`₹${walletBalance?.toFixed(2)}`}</Text>
              </View>


              <View style={{ flexDirection: 'column', marginTop: 30 }}>
                <TextInput
                  style={styles.input1}
                  keyboardType="numeric"
                  onChangeText={(text) => setAddmMoney(text)}
                  value={addMoney}
                  placeholder='Enter Amount'
                />

                <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: '#0049F1', marginTop: 15 }}>
                </View>
              </View>

              <View style=
                {{
                  width: '92%',
                  // height:70,
                  alignSelf: 'center',
                  marginTop: 15,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>

                <TouchableOpacity
                  onPress={() => setAddmMoney(Number(addMoney) + 100 + "")}
                  style={styles.tabStyle2}>
                  <Text style={styles.tabValue1}>100</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => setAddmMoney(Number(addMoney) + 200 + "")}
                  style={styles.tabStyle2}>
                  <Text style={styles.tabValue1}>200</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => setAddmMoney(Number(addMoney) + 300 + "")}
                  style={styles.tabStyle2}>
                  <Text style={styles.tabValue1}>300</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => setAddmMoney(Number(addMoney) + 500 + "")}
                  style={styles.tabStyle2}>
                  <Text style={styles.tabValue1}>500</Text>
                </TouchableOpacity>

              </View>

              <TouchableOpacity
                style={{
                  alignSelf: "center",
                  marginTop: 30,

                }}
                onPress={() => {
                  if (addMoney == "") {
                    toastShow("Please enter money", "red")
                  }
                  else if (addMoney < 50) {
                    toastShow("Amount shuold be greater than 50", "red")
                  }
                  else {
                    addFunds();
                  }
                }}
              >
                <Image
                  style={styles.mainButton}
                  source={imageConstant.addAmount}
                />
              </TouchableOpacity>
            </TouchableOpacity>
          </TouchableOpacity>
        </Modal>

        <Modal
          animationType="slide"
          transparent={true}
          visible={withdrawMoneyModalVisible}
          supportedOrientations={['portrait', 'landscape']}
          onRequestClose={() => {
            setWithdrawMoneyModalVisible(!withdrawMoneyModalVisible)
          }}>
          <TouchableOpacity
            activeOpacity={1}

            onPress={() => { setWithdrawMoneyModalVisible(!withdrawMoneyModalVisible) }}
            style={styles.centeredView}>
            <TouchableOpacity
              activeOpacity={1}

              onPress={() => { }}
              style={{
                bottom: modalPadding,
                position: 'absolute',
                backgroundColor: "white",
                borderTopStartRadius: 20,
                borderTopEndRadius: 20,
                padding: 20,
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 2
                },
                shadowOpacity: 0.25,
                shadowRadius: 4,
                elevation: 5,
                width: '100%',
                height: 400,
              }}>
              <TouchableOpacity
                onPress={() => { setWithdrawMoneyModalVisible(!withdrawMoneyModalVisible) }}

                style={{
                  right: 10,
                  top: 10,
                  position: 'absolute'
                }}
              >
                <Image
                  style={styles.cancelButtonStyle}
                  source={imageConstant.cancelImg}
                />
              </TouchableOpacity>

              <Text style={styles.modalHeadingStyle}>Withdraw Money</Text>

              <View style={{ flexDirection: 'row', marginTop: 30 }}>
                <Text style={{ color: '#777E91', fontWeight: '400', fontSize: 16, fontFamily: fontConstant.regular }}>Balance : </Text>
                <Text style={{ color: '#141416', fontWeight: '500', fontSize: 16, fontFamily: fontConstant.bold }}>{`₹${fundData?.winnings}`}</Text>
              </View>

              <View style={{ flexDirection: 'column', marginTop: 30 }} />
                <TextInput
                  style={styles.input1}
                  defaultValue="50"
                  keyboardType="numeric"
                  onChangeText={(text) => setWithdrawMoney(text)}
                  value={withdrawMoney}
                  placeholder='Enter Amount'
                />
                <View style={{ width: '100%', borderBottomWidth: 1, borderBottomColor: '#0049F1', marginTop: 15 }}>
                </View>

                <View style=
                  {{
                    width: '92%',
                    alignSelf: 'center',
                    marginTop: 15,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <TouchableOpacity
                    onPress={() => setWithdrawMoney(Number(withdrawMoney) + 100 + "")}
                    style={styles.tabStyle2}>
                    <Text style={styles.tabValue1}>100</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => setWithdrawMoney(Number(withdrawMoney) + 200 + "")}
                    style={styles.tabStyle2}>
                    <Text style={styles.tabValue1}>200</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() =>setWithdrawMoney(Number(withdrawMoney) + 300 + "")}
                    style={styles.tabStyle2}>
                    <Text style={styles.tabValue1}>300</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => setWithdrawMoney(Number(withdrawMoney) + 500 + "")}
                    style={styles.tabStyle2}>
                    <Text style={styles.tabValue1}>500</Text>
                  </TouchableOpacity>

                </View>
              <TouchableOpacity
                onPress={() => {
                  console.log(withdrawMoney);
                  if (withdrawMoney == "") {
                    setWithdrawMoneyModalVisible(!withdrawMoneyModalVisible)
                    toastShow("Please enter withdraw money amount", "red")
                  }
                  else if (withdrawMoney < 100) {
                    setWithdrawMoneyModalVisible(!withdrawMoneyModalVisible)
                    toastShow("Withdraw money should be greater than 100", "red")
                  }
                  else if(withdrawMoney>Number(fundData?.winnings)){
                    setWithdrawMoneyModalVisible(!withdrawMoneyModalVisible)
                    toastShow("Withdraw money is greater than winning money", "red")
                  }
                  else if (kycFlag) {
                    navigation.navigate("Withdraw",{amount:withdrawMoney});
                  }
                  else {
                    toastShow("KYC mandatory for withdrawal", "red");
                    navigation.navigate("KycVerification");
                  }

                }}
                style={{
                  alignSelf: "center",
                  position: "absolute",
                  bottom: 10


                }}
              >
                <Image
                  style={{
                    width: 330,
                    height: 65,
                    resizeMode: 'contain'
                  }}
                  source={imageConstant.withdrawButton}
                />
              </TouchableOpacity>



            </TouchableOpacity>
          </TouchableOpacity>
        </Modal>

      </SafeAreaView>
    </>
  )
}

export default Wallet

const styles = StyleSheet.create({
  bellImageStyle: {
      marginTop:15,
      position:'absolute',
      height: 36,
      width: 36,
      right:15,
      resizeMode:'contain'
  },
  optionImageStyle: {

      height: 36,
      width: 36,
  },
  plusImageStyle: {
    height: 24,
    width: 24,
    resizeMode:'contain',
  },
  tickImageStyle: {
    height: 20,
    width: 20,
    resizeMode:'contain'
  },
  buttonStyle:{
    width:"47%",
    height:56,
    backgroundColor:'#FFFFFF',
    borderRadius:8,
    flexDirection:'row',
    justifyContent:'space-evenly',
    alignItems:'center',
    padding:10
  },
  tabStyle:{
    width:layout/3.4,
    height:60,
    backgroundColor:'#F4F5F6',
    borderRadius:8,
    flexDirection:'column',
    padding:10
  },
  tabHeading:{
    color:'#141416',
    fontWeight:'400',
    fontSize:12,
    fontFamily:fontConstant.regular
  },
  tabValue:{
    color:'#141416',
    fontWeight:'500',
    fontSize:18,
    fontFamily:fontConstant.regular,
    marginTop:5
  },
  textStyle: {
    fontSize:16,
    opacity:1,
    width:"80%",
    fontFamily:fontConstant.regular
  },
  dateStyle:{
    color: "#B1B5C3",
    fontSize:14,
    fontWeight:'600',
    fontFamily:fontConstant.regular,
    marginTop:20,
    marginLeft:15,
    marginBottom:10
  },
centeredView: {
  flex: 1,
  justifyContent: "center",
  alignItems: "center",
  backgroundColor:'rgba(0, 0, 0, 0.4)'
},
cancelButtonStyle:{
  width:30,
  height:30,
},
modalHeadingStyle:{
  marginTop:30,
  color :'#141416',
  fontWeight:'500',
  fontSize:16,
  fontFamily:fontConstant.regular,
  textAlign:'center'
},
input1 : {
  fontSize: 16,
  color:'#141416',
},
tabStyle2:{
  width:layout/5,
  backgroundColor:'#E6E8EC',
  borderRadius:4,
  flexDirection:'column',
  padding:10
},
tabValue1:{
  color:'#141416',
  fontWeight:'400',
  fontSize:16,
  fontFamily:fontConstant.regular,

  textAlign:'center'
},
mainButton:{
  width:345,
  height:65,
  resizeMode:'contain'
},
mainButton2:{
  width:345,
  height:65,
  marginLeft:10,
  marginTop:40
}
})