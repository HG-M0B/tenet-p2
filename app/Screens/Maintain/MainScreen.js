import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import { Height, Width } from '../../dimension/dimension'

const MainScreen = () => {
  return (
    <View style={styles.main}>
        <Image
            resizeMode='contain'
            source={imageConstant.main}
            style={{
                width:"90%",
                height:Height*0.50,
                alignSelf:"center",
                marginTop:Height*0.12,
            }}
            />
            <Text style={{
                fontSize:20,
                fontFamily:fontConstant.regular,
                color:colorConstant.blackText,
                textAlign:"center",
                lineHeight:27,
                marginTop:10
            }}>We are under maintenance{'\n'}and will be live soon</Text>
    </View>
  )
}

export default MainScreen

const styles = StyleSheet.create({
    main:{
        flex:1,
        backgroundColor:colorConstant.white
    }
})