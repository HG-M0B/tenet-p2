import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    SafeAreaView,
    Button,
    TouchableOpacity,
    Alert,
    Platform,
    Dimensions,
    ScrollView,
    FlatList,
    Pressable,
    BackHandler,
    Modal
} from 'react-native';
import React, {Component, useEffect, useRef, useState} from 'react';

import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";

import CustomHeader from '../../custom/CustomHeader';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Timeline from 'react-native-timeline-flatlist'
import { baseURL } from '../../utils/URL';
import moment from 'moment';
import { useSelector } from 'react-redux';
import { WebView } from 'react-native-webview';
import WebviewComp from '../../utils/WebviewComp';
import Loader from '../../utils/loader';
import { useFocusEffect } from '@react-navigation/native';
import { Height,Width } from "../../dimension/dimension";
import SwipeButton from 'rn-swipe-button';
import toastShow from '../../utils/Toast';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { colorConstant, imageConstant ,fontConstant} from '../../utils/constant';
const windowWidth = Dimensions.get('window').width;


const layout=windowWidth-20

const EventDetails = props => {
  const {jwtToken,walletBalance} =  useSelector((state) => state.reducers);
  const [event,setEvent]=useState([])
  const [typeEvent,setTypeEvent]=useState('');
  const [loader,setLoader] = useState(false);
  const [trendingVolume,setTrendingVolume]=useState(0)
  const [id,setID]=useState('')
  const [quantity,setQuantity]=useState([5]);
  const [price,setPrice]=useState([7]);
  const [eventDetails,setEventDetails] = useState(null);
  const [detailsModal, setDetailsModal] = useState(false);
  const [optionChoose,setOptionChoose] = useState("");
  const [orderData,setOrderData] =  useState(null);
  const [sliderShow,setSliderShow] =  useState(false);
const [mapsuggestedData,setMapSuggestData] = useState([]);
const [suggestedQuantity,setSuggestedQuantity] = useState(0);

  const {route, navigation} = props;
 
useEffect(()=>{
  let subs = navigation.addListener('focus',()=>{
  
    if(route?.params.id)
    {
      getEventDataByID(route.params.id)
    }
  })
 return ()=>subs;
  
},[]);
useEffect(()=>{
  gettingSingleSuggestion();
  },[price,mapsuggestedData]);
  
  const gettingSingleSuggestion=()=>{
  let tempData = mapsuggestedData;
  let qty = tempData?.filter((item,index) => item?.amount === price[0]);
  setSuggestedQuantity(qty?.[0]);
  }


  useEffect(() => {
      if (orderData !== null) {
        suggestData()
      }
    }, [orderData])



useFocusEffect(
  React.useCallback(() => {
    const backAction = () => {
      props.navigation.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, [])
);

useEffect(()=>{
  let tempArray = event?.event_history?.volume_history;
        let sum  = 0 
        for(let i = 0 ;i<tempArray?.length ;i++){
          sum  = tempArray?.[i]?.value+sum
        }
        setTrendingVolume(sum);
  
},[event]);
  let data = [
    
    {title: 'Event Live',icon:require('../../assets/images/greenBell.png'),ques:event.title,lineColor:'#08A02A',trade_value:trendingVolume,endtime:moment(event?.end_datetime).fromNow(),tip:event.tip},
    // {title: '',icon:require('../../assets/images/greenBell.png'),ques:event.title,lineColor:'#08A02A',trade_value:trendingVolume,endtime:moment.utc(event?.end_datetime).format('h'),tip:event.tip},
    // {title: 'Possible Event Closure', des: moment(event?.end_datetime).format("Do MMMM, hh:mm a" ),icon:require('../../assets/images/greenCal.png'),lineColor:'#08A02A'},
    {title: 'Source',icon:require('../../assets/images/verified.png'),lineColor:'#D9D9D9',outcome_verify_source:event.outcome_verify_source},
    
    { title: 'Event Closes',description:moment(event?.end_datetime).format("Do MMMM, hh:mm a" ),icon:require('../../assets/images/conf.png'),lineColor:'#D9D9D9'},
  ]

  const chartData = {
    labels: ["2009", "2010", "2011", "2012", "2013"],
    datasets: [
      {
        data: [100, 200, 150, 300, 200],
        color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`, // optional
        // strokeWidth: 1 // optional
      }
    ],
  };

  const getEventDataByID = async (id) => {
    getTradeOrderCall(id)
    try {
      setLoader(true)
      let tokenSent = "Token " + jwtToken
      setID(id);
      let response = await fetch(baseURL + `events/get-events/${id}`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': tokenSent
        }
      })
      if (response.status == 200) {
        let res = await response.json()
        setEvent(res)
        setLoader(false);

      }
    }
    catch (error) {
      toastShow(error, "red")
      setLoader(false);
    }

  }

  const getEventData = async (id,otp,money) => {
    setOptionChoose(otp);
    setPrice([money]);
    setSliderShow(false);
    try {
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL + `events/get-events/${id}`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': tokenSent
        }
      })
      if (response.status == 200) {
        let res = await response.json();
        setEventDetails(res);
        setDetailsModal(!detailsModal);

        
      }
    }
    catch (error) {
      toastShow(error, "red")
      setLoader(false)
    }

  }

  const tradeCall =  async (id)=>{
    try{
      let reqData = {
        amount:price?.[0],
        quantity:quantity?.[0],
        selected_option:optionChoose,
        binary_event: id
      }
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL +`orders/`, {
        method:"POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': tokenSent
        },
        body:JSON.stringify(reqData)
      })
      if( response && response.status == 200)
      {
        let jsonData = await response.json();
        
        if(jsonData?.status == 200)
        {
          setDetailsModal(!detailsModal);
          toastShow(jsonData?.data,"red");
          navigation.navigate("HomePage");
        }
        
        }
      

    
    if(response && response.status == 201) 
      {
        setDetailsModal(!detailsModal);
        navigation.navigate("TradeSuccess",{
          data : price * quantity
        });
      } 

    }
    catch(error){
      toastShow(error, "red")
    }

  }

  const getTradeOrderCall = async(id)=>{
    try{
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL +`orders/?event_id=${id}`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': tokenSent
        }
      });
      
      if(response.status === 200)
      {
        let jsonData = await response.json();
        setOrderData(jsonData);

      }

    }
    catch(error){
      toastShow(error, "red")
    }
  }


  const suggestData = () => {

    let tempObj = { "yes": [], "no": [] }
    let tempData = orderData;
    let obj = {};
    // **********Group by obj**********
    for (let i = 0; i < tempData?.length; i++) {

      if (tempData?.[i]?.selected_option == "yes") {
        tempObj["yes"] = [...tempObj["yes"], tempData?.[i]]
      }
      if (tempData?.[i]?.selected_option == "no") {
        tempObj["no"] = [...tempObj["no"], tempData?.[i]]
      }

    }

    let yesArr = tempObj?.["yes"];
    let noArr = tempObj?.["no"]

    yesArr?.forEach((item, index) => {
      yesArr[index] = { amount: 10 - item?.amount, qty: item?.quantity - item?.matched_quantity, option: "No" }
    })
    noArr?.forEach((item, index) => {
      noArr[index] = { amount: 10 - item?.amount, qty: item?.quantity - item?.matched_quantity, option: "Yes" }
    })
    let yesFinalArray = [];
    let noFinalArray = [];

    let sumYesArray = yesArr?.reduce(function (acc, curr) {
      let findIndex = acc.findIndex(item => item.amount === curr.amount);

      if (findIndex === -1) {
        acc.push(curr)
      } else {

        acc[findIndex].qty += curr.qty
      }
      return acc;
    }, [])

    let sumNoArray = noArr?.reduce(function (acc, curr) {
      let findIndex = acc.findIndex(item => item.amount === curr.amount);

      if (findIndex === -1) {
        acc.push(curr)
      } else {

        acc[findIndex].qty += curr.qty
      }
      return acc;
    }, [])


    let finalArray = [...sumYesArray, ...sumNoArray];
    setMapSuggestData(finalArray);
  }

const renderDetail=(item)=>{
    return(
    <View>

        <Text style={styles.timelineTitle}>{item.title}</Text>
        <Text style={styles.timelineDesc}>{item.description}</Text>
        
        {
            item.title=='Event Live' ?
            
            <View>
              <Text style={styles.timlineQues}>{item.ques}</Text>
            <View style={styles.timlineView1}>
            <View>
            <View style={styles.timelineView2}>
              <Image
                style={{width:18,height:18}}
                source={imageConstant.timer}
              />
              <Text style={styles.timelineText1}> {item.endtime} 
              {/* hours */}
              </Text>
            </View>
            <Text style={styles.timelineText2}>Ends in</Text>
            </View>
           
            <View style={{
            }}>
            <View style={styles.timelineView3}>
              <Image
                style={{width:14,height:14}}
                source={imageConstant.volume}
              />
              <Text style={styles.timelineText1}>₹ {item.trade_value}</Text>
            </View>
            <Text style={styles.timelineText2}>Volume</Text>
            </View>
            </View>  
          {item.tip ?
            <View style={styles.timlineView4}>
            <Image
                style={{width:22,height:28}}
                source={imageConstant.greenBulb}/>
            <Text style={styles.timelineText3}>{item.tip}</Text>
            </View>:null}

            </View>

            : 

            <View>
                {item.title=='Possible Event Closure'
                ?
                <View>
                <TouchableOpacity style={{}}>
                    <Text style={{color:'#000000',fontWeight:'600'}}>{item.des}</Text>
                </TouchableOpacity>
                </View>
                :
                <View>
                  {item.title == 'Source'
                    ?
                    <>


                      <TouchableOpacity style={styles.timelineTouch}
                        onPress={() => {
                          props.navigation.navigate("WebviewComp", {
                            url: item?.outcome_verify_source
                          })
                        }}
                        activeOpacity={1}
                      >
                        <Image source={imageConstant.Research} style={{ height: 20, width: 20, marginLeft: 10 }} />
                        {/* <Text style={styles.timelinetochText}>Research</Text> */}

                        <Text style={styles.timelinetochText}> {item?.outcome_verify_source}</Text>
                      </TouchableOpacity></>


                    :
                    null
                  }

                  {
                    item.title == 'Event Closes'
                      ?
                      <></>
                      // <Text style={{
                      //   fontSize: 16,
                      //   fontFamily: fontConstant.bold,
                      //   color: "#414246"
                      // }}>{item?.description}</Text>
                      :
                      null
                  }
            
            </View>
            
            }
            
            </View>
        }

    </View>)
}

const checkBalance =(id)=>{
  if(price*quantity > walletBalance)
  {
    toastShow("You don't have enough funds to proceed","red");
    setDetailsModal(!detailsModal)
  }
  else
  {
    tradeCall(id)
  }
}

  return (
    <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
    <View style={{flex: 1,backgroundColor:'#FFFFFF' }}>
   
    <CustomHeader
        headerText={'Event Details'}
        img={imageConstant.back}
        navigation={props.navigation}
      />
      {loader && <Loader  data={loader} />}
    <ScrollView
    showsVerticalScrollIndicator={false}
    contentContainerStyle={{
        paddingBottom:60
    }}
    >

    <View style={{right:30,marginBottom:10}}>
    <Timeline
        data={data}
        renderDetail={renderDetail}
        lineWidth={7}
        innerCircle={'icon'}
        circleSize={26}
        detailContainerStyle={{marginBottom:10}}
        renderFullLine={false}
    />
    </View>

    <View style={{width:layout-20,flexDirection:'column',alignSelf:'center',marginTop:20}}>
        <Text style={{color:'#000000',fontWeight:'700',fontSize:14,fontFamily:fontConstant.regular}}>Event Details</Text>
        <Text style={{color:'#414246',fontWeight:'500',fontSize:10,fontFamily:fontConstant.regular,marginTop:10}}>
          {event.overview}
            {/* Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus */}
        </Text>
       

    </View>


          <View style={styles.settlementView}>
            <View style={{ width: layout - 10, alignSelf: 'center', paddingVertical: 10 }}>
              <Text style={styles.settlementViewTitleStyle}>Settlement Terms:</Text>

              <Text style={styles.settlementViewTextStyle}>
                {event.settlement_terms}

              </Text>
            </View>
          </View>




    {/* <View style={{width:windowWidth,height:400,marginTop:20,marginBottom:-120}}>
        <LineChart
        data={chartData}
        width={windowWidth}
        height={220}
        chartConfig={{
          backgroundColor: "#F5F5F5",
          backgroundGradientFrom: "#F5F5F5",
          backgroundGradientTo: "#E1EAF4",
          decimalPlaces: 0, // optional, defaults to 2dp
          color: (opacity = 1) => `rgba(0, 73, 241,  ${opacity})`,
          labelColor: (opacity = 1) => `rgba(65, 66, 70, ${opacity})`,
          style: {
            borderRadius: 16
          },
          // barPercentage: 0.5,
          propsForDots: {
            r: "6",
            strokeWidth: "3",
            stroke: "blue",
            strokeOpacity:1
          },
          lineWidth:2
        }}
      />
    </View> */}
    </ScrollView>

    <View style={styles.bottomButtonView}>
        <TouchableOpacity 
        activeOpacity={1}

       onPress={()=>getEventData(id,"yes",event?.price_yes)}
        // onPress={()=>navigation.navigate("EventDetails1",{id:id,typeEvent:typeEvent,opt:"yes",price:event?.price_yes})}
        style={styles.yesButtonStyle}
        >
        <Text style = {styles.bottomButtonTextStyle}>{`Yes ₹ ${event?.price_yes ?? 0}`}</Text>
        </TouchableOpacity>

        <TouchableOpacity
         activeOpacity={1}

   
         onPress={()=>getEventData(id,"no",event?.price_no)}
        // onPress={()=>navigation.navigate("EventDetails1",{id: id,typeEvent:typeEvent,opt:"no",price:event?.price_no})}
        style={styles.noButtonStyle}
        >
        <Text style = {styles.bottomButtonTextStyle}>{`No ₹ ${event?.price_no ?? 0}`}</Text>
        </TouchableOpacity>
    </View>
    
    </View>



      <Modal
        animationType="slide"
        transparent={true}
        visible={detailsModal}
        onRequestClose={() => {
          setDetailsModal(!detailsModal);
        }}
      >

        <View style={styles.centeredView2}>
          <TouchableOpacity
            onPress={() => setDetailsModal(!detailsModal)}>

            <Image
              source={imageConstant.cancelImg}
              style={{
                width: 30,
                height: 30,
                right: 10,
                top: Height * 0.10,
                position: "absolute"
              }} />
          </TouchableOpacity>

          <View style={styles.modalView2}>

            <View style={{
              flexDirection: "row",
              // width: "100%",
              justifyContent: "space-between",
              height: 60

            }}>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => setOptionChoose("yes")}
                style={{
                  width: "50%",
                  backgroundColor: optionChoose == "yes" ? colorConstant.bbg : colorConstant.lightGray,
                  justifyContent: "center",
                  alignItems: "center",
                  borderBottomWidth: 1,
                  borderBottomColor: "#D3D3D3",
                  borderRightWidth: 1,
                  borderRightColor: "#D3D3D3",
                  borderTopLeftRadius: 20
                }}>
                <Text style={{
                  fontSize: 18,
                  fontFamily: fontConstant.bold,
                  color: optionChoose == "yes" ? colorConstant.white : colorConstant.blackText,



                }}>YES <Text style={{
                  fontSize: 14,
                  fontFamily: fontConstant.medium,
                  color: optionChoose == "yes" ? colorConstant.white : colorConstant.blackText
                }}> ₹{eventDetails?.price_yes}</Text></Text>
              </TouchableOpacity>


              <TouchableOpacity
                onPress={() => setOptionChoose("no")}
                activeOpacity={1}
                style={{
                  width: "50%",
                  backgroundColor: optionChoose == "no" ? colorConstant.bbg : colorConstant.lightGray,
                  justifyContent: "center",
                  alignItems: "center",
                  borderBottomWidth: 1,
                  borderBottomColor: "#D3D3D3",
                  borderTopRightRadius: 20

                }}>
                <Text style={{
                  fontSize: 18,
                  fontFamily: fontConstant.bold,
                  color: optionChoose == "no" ? colorConstant.white : colorConstant.blackText
                }}>NO <Text style={{
                  fontSize: 14,
                  fontFamily: fontConstant.medium,
                  color: optionChoose == "no" ? colorConstant.white : colorConstant.blackText
                }}> ₹{eventDetails?.price_no} </Text></Text>
              </TouchableOpacity>



            </View>



            <ScrollView
              show showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                paddingVertical: 20
              }}>

              <View style={{
                width: "90%",
                alignSelf: "center",
                marginTop: 20
              }}>
                <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.bold,
                  lineHeight: 25,
                  color: colorConstant.blackText,

                }}>{eventDetails?.title}
                </Text>
              </View>

              <View style={{
                flexDirection: "row",
                alignItems: "center",
                alignSelf: "center",
                marginTop: 20,
                width: "90%"
              }}>
                <Image
                  source={imageConstant.timer}

                  style={{
                    width: 20,
                    height: 20,
                    tintColor: colorConstant.blackText
                  }}
                />
                <Text style={{
                  fontSize: 14,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.blackText
                }}> {moment(eventDetails?.end_datetime).fromNow()}</Text>
              </View>



              <View style={{
                flexDirection: "row",
                width: Width * 0.9,
                justifyContent: "space-between",
                alignSelf: "center",


              }}>

                <View style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 20

                }}>
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText,
                  }}>Set Price <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText
                  }}> ₹{price}</Text></Text>

                  {
                    sliderShow ?
                      <TouchableOpacity
                        onPress={() => setSliderShow(!sliderShow)}
                        style={{
                          padding: 5,
                          marginLeft: 5
                        }}>

                        <Image
                          resizeMode='contain'
                          source={imageConstant.poly}
                          style={{
                            width: 10,
                            height: 10
                          }} />
                      </TouchableOpacity>
                      :
                      <TouchableOpacity
                        onPress={() => setSliderShow(!sliderShow)}
                        style={{
                          padding: 5,
                          marginLeft: 5
                        }}>

                        <Image
                          resizeMode='contain'
                          source={imageConstant.poly}
                          style={{
                            width: 10,
                            height: 10,
                            transform: [{ rotate: "180deg" }]
                          }} />
                      </TouchableOpacity>
                  }


                </View>


                {
                  suggestedQuantity?.option?.toUpperCase() == optionChoose.toUpperCase()
                    ?
                    <Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.medium,
                      color: colorConstant.blackText,
                      textAlign: "right",
                      marginTop: 20
                    }}>Available Matches<Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.medium,
                      color: colorConstant.blackText
                    }}> {suggestedQuantity?.qty} </Text></Text>
                    :
                    <View>
                    </View>
                }



              </View>





              {/* //Price Slider */}

              {
                sliderShow && (
                  <View style={{
                    width: Width * 0.9,
                    alignSelf: "center",
                    alignItems: "center"
                  }}>
                    <MultiSlider
                      values={price}
                      min={1}
                      max={9}
                      sliderLength={Width * 0.86}
                      onValuesChange={(val) => setPrice(val)}
                      selectedStyle={{ backgroundColor: colorConstant.white }}
                      unselectedStyle={{ backgroundColor: colorConstant.white }}
                      //={imageConstant.thumbimage}
                      customMarker={() => {
                        return (
                          <Image
                            source={imageConstant.thumb}
                            resizeMode='contain'
                            style={{
                              height: 30,
                              width: 30
                            }}
                          />
                        )
                      }}
                      trackStyle={{ height: 4, borderRadius: 10 }}
                      markerStyle={{ height: 20, width: 20, backgroundColor: '#087CFF', }}
                    />
                  </View>
                )
              }


              <View style={{
                width: "90%",
                alignSelf: "center",
                alignItems: "flex-start",
                marginTop: sliderShow ? 0 : 20
              }}>
                <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.blackText

                }}>Quantity <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.blackText
                }}>{quantity}</Text></Text>
              </View>


              <View style={{
                width: Width * 0.9,
                alignSelf: "center",
                alignItems: "center"
              }}>
                <MultiSlider
                  values={quantity}
                  min={1}
                  max={25}
                  sliderLength={Width * 0.85}
                  onValuesChange={(val) => setQuantity(val)}
                  selectedStyle={{ backgroundColor: colorConstant.white }}
                  unselectedStyle={{ backgroundColor: colorConstant.white }}
                  //={imageConstant.thumbimage}
                  customMarker={() => {
                    return (
                      <Image
                        source={imageConstant.thumb}
                        resizeMode='contain'
                        style={{
                          height: 30,
                          width: 30
                        }}
                      />
                    )
                  }}
                  trackStyle={{ height: 4, borderRadius: 10 }}
                  markerStyle={{ height: 20, width: 20, backgroundColor: '#087CFF', }}
                />

              </View>




              <View style={{
                flexDirection: "row",
                // width: "100%",
                justifyContent: "space-between",
                marginTop: 20
              }}>
                <View style={{
                  width: "50%",
                  justifyContent: "center",
                  alignItems: "center",
                  borderRightWidth: 1,
                  borderRightColor: "#004BEB"
                }}>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: fontConstant.bold,
                    color: colorConstant.blackText
                  }}>₹ {quantity * price}</Text>
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText
                  }}>You invest</Text>
                </View>
                <View
                  style={{
                    width: "50%",
                    justifyContent: "center",
                    alignItems: "center"
                  }}>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: fontConstant.bold,
                    color: colorConstant.blackText
                  }}>₹ {eventDetails?.trade_value * quantity}</Text>
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText
                  }}>You Earn **</Text>
                </View>
              </View>







              <SwipeButton
                title={`SWIPE FOR ${optionChoose == "yes" ? "YES" : "NO"}`}
                shouldResetAfterSuccess={true}
                height={45}
                width={Width * 0.9}
                railBackgroundColor={colorConstant.blue}
                titleColor='#FFFFFF'
                railFillBackgroundColor={colorConstant.blue}
                railBorderColor={colorConstant.blue}
                railFillBorderColor={colorConstant.blue}
                thumbIconBackgroundColor={colorConstant.green}
                thumbIconImageSource={imageConstant.rightarrow}
                thumbIconBorderColor={colorConstant.green}
                containerStyles={{ marginTop: 30, alignSelf: "center" }}
                onSwipeSuccess={() => checkBalance(eventDetails?.id)}


              />

              {
                price * quantity > walletBalance ?
                  <View style={{
                    width: "85%",
                    flexDirection: "row",
                    marginTop: 30,
                    alignItems: "center",
                    alignSelf: "center",
                    justifyContent: "space-between"
                  }}>
                    <Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.bold,
                      color: colorConstant.blackText
                    }}> Wallet Balance <Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.bold,
                      color: colorConstant.blackText
                    }}> ₹ {walletBalance?.toFixed(2)}</Text></Text>


                    <TouchableOpacity
                      onPress={() => {
                        setDetailsModal(!detailsModal);
                        setTimeout(() => {
                          navigation.navigate("Wallet");
                        }, 500)
                      }}
                      activeOpacity={1}
                      style={{
                        paddingHorizontal: 25,
                        paddingVertical: 8,
                        borderRadius: 20,
                        backgroundColor: "#99BFF3"

                      }}>
                      <Text style={{
                        fontSize: 16,
                        fontFamily: fontConstant.medium,
                        color: "#004BEB"
                      }}>Recharge Now</Text>
                    </TouchableOpacity>
                  </View>

                  :
                  <></>
              }


            </ScrollView>



          </View>


        </View>


      </Modal>

        
    </SafeAreaView>
  )
}

export default EventDetails

const styles = StyleSheet.create({
  chartConfig : {
    backgroundGradientFrom: "#1E2923",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#08130D",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false // optional
  },
    headerImage:{
        height:20,
        width:20
    },
    View1:{
        position:'absolute',
        top:10,
        width:70,
        right:10,
        flexDirection:'row',
        justifyContent:'space-around'
    },
    timelineTitle:{
      marginTop:-5,
      color:'#414246',
      fontSize:10,
      fontWeight:'500'
    },
    timelineDesc:{
      marginTop:5,
      color:'#414246',
      fontSize:14,
      fontWeight:'700'
    },
    timlineQues:{
      fontSize:16,
      color:'#414246',
      fontWeight:'900',
      marginTop:-20
    },
    timlineView1:{
      paddingTop:10,
      backgroundColor:'#F8F8F8',
      width:'100%',
      height:50,
      borderRadius:30,
      alignSelf:'center',
      flexDirection:'row',
      justifyContent:'space-around',
      marginTop:25
    },
    timelineText1:{
      color:'#414246',
      fontWeight:'600',
      fontSize : 14, 
      marginLeft :8
    },
    timelineText2:{
      color:'#414246',
      fontWeight:'500',
      fontSize : 9, 
      marginTop:2
    },
    timelineView2:{
      justifyContent : 'center',
      flexDirection : 'row',
    },
    timelineView3:{
      justifyContent : 'center',
      alignItems:"center",
       flexDirection : 'row'
    },
    timlineView4:{
      flexDirection:'row',
      marginVertical:30
    },
    timelineText3:{
      color:'#08A02A',
      fontSize:12,
      fontWeight:'400',
      width:'80%',
      textAlign:'justify',
      marginLeft:8
    },
    timelineTouch:{
      backgroundColor:'#EBECF0',
      flexDirection:'row',
      shadowColor: '#000000',
      shadowOpacity: 1,
      elevation:17,
      shadowRadius: 3,
      shadowOffset: { height: 5, width: 5 },
      borderRadius:40,
      maxWidth:Width*0.65,
      minWidth:Width*0.40,
      alignItems:'center',
      paddingVertical:7,
      paddingRight:7,
      
    },
    timelinetochText:{
      width:"80%",
      fontSize:12,
      color:'#414246',
      marginLeft:5,
    },

    yesButtonStyle:{
        backgroundColor:'#0049F1',
        width:'50%',
        alignItems:'center',
        justifyContent:'center'
    },
    noButtonStyle:{
        backgroundColor:'#FF2E6D',
        width:'50%',
        alignItems:'center',
        justifyContent:'center'
    },
    bottomButtonTextStyle:{
        color:'#FFFFFF',
        fontWeight:'700',
        fontSize:16,
        fontFamily:fontConstant.regular
    },
    bottomButtonView:{
        width:windowWidth,
        height:hp('7%'),
        alignSelf:'center',
        bottom:0,
        position:'absolute',
        flexDirection:'row',
    },
    settlementView:{
        marginVertical:10,
        backgroundColor:'#F8F8F8',
        width:windowWidth,
        // height:hp('20%'),
        alignSelf:'center'
    },
    settlementViewTitleStyle:{
        color:'#414246',
        fontWeight:'700',
        fontSize:10,
        fontFamily:fontConstant.regular,
    },
    settlementViewTextStyle:{
        color:'#414246',
        fontWeight:'400',
        fontSize:10,
        fontFamily:fontConstant.italic,
        lineHeight:15,
        letterSpacing:0.6,
        marginTop:10
        // marginBottom:10
    },
    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor:"rgba(1,1,1,0.5)"
  
    },
    centeredView2: {
      flex: 1,
      backgroundColor:"rgba(1,1,1,0.5)"
  
    }
    ,
    modalView: {
      margin:10,
      width:"90%",
      backgroundColor: "white",
      borderRadius: 20,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    
    modalView2: {
      width:"100%",
      height:Height*0.80,
      position:"absolute",
      bottom:0,
      backgroundColor:colorConstant.skyblue,
      borderTopLeftRadius: 20,
      borderTopRightRadius:20,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    }
})