import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    SafeAreaView,
    Button,
    TouchableOpacity,
    Alert,
    Platform,
    Dimensions,
    ScrollView
  } from 'react-native';
import React, {Component, useRef, useState} from 'react';
import CustomHeader from '../../custom/CustomHeader';
import { colorConstant, imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'

import { baseURL } from '../../utils/URL';
import {useDispatch} from 'react-redux';
import  {actions} from '../../redux/reducers';
import toastShow from '../../utils/Toast';
import { Height } from '../../dimension/dimension';

const windowWidth = Dimensions.get('window').width;
const layout=windowWidth-20
const AdditionalDetails = ({route, navigation}) => {
  const [additionalData, setAdditionalData]=useState([])
  const [name,setName]=useState('')
  const [email,setEmail]=useState('')
  const [phone, setPhone]=useState('');
  const [codeRefer,setCodeRefer] = useState("")
  const dispatch =useDispatch();
  const {number, token} = route?.params;
 
  


  const navigateToConsent=()=>{
    navigation.navigate("UserConsent",{email:email?.toLowerCase(),username:name,num:number,uidToken:token,referCode:codeRefer})
  }


  const playEvent=()=>{
    let tempEmail  = true;
    if(name == "")
    {
      toastShow("Enter your name","red");
    }
    else if(email !== "")
    {
      if (!/^.+?@.+?\..+$/.test(email)) {
        toastShow("Please enter valid email","red");
        tempEmail =  false
        
      }
      else
      {
        checkEmailExist();
        tempEmail =  false
      }
       
    }
    else if(name !== ""  && tempEmail)
    {

      navigateToConsent();
    }
   
  }

  const checkEmailExist = async ()=>{
    try {
      let reqData = {
        email:email?.trim()
      }
      let response = await fetch(baseURL+'auth/checkemail/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'accept': 'application/json'
        },
        body: JSON.stringify(reqData)
      }
      )
     if(response && response.status == 200){
       let jsonData = await response.json();
      toastShow("User already exists with this email", "red")
     }
     else
     {
      navigateToConsent();
     }
    }
    catch(error){
      console.log("error",error)
    }
  }


    return(
        <View style={{flex: 1,backgroundColor:'#FFFFFF' }}>
      
        <KeyboardAwareScrollView 
          showsVerticalScrollIndicator={false}
          extraHeight={Height/2}
          extraScrollHeight={Height/2}
          contentContainerStyle={{
            height:Height-20,
          }}>

          <Image
            style={styles.imageStyle}
            source={imageConstant.logo}
          />
          <View>
            <Text style={styles.textStyle}>Please provide additional Info</Text>
          </View>
          <View style={{
            width: "90%",
            minHeight: 40,
            maxHeight: 40,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 30,
            borderBottomWidth: 1,
            borderColor: '#E6E8EC',
          }}>
            <TextInput
              style={styles.input1}
              placeholder="Your Name"
              placeholderTextColor={'#777E91'}
              onChangeText={(e) => setName(e)}
            />
           
          </View>
 
          <View style={{
            width: "90%",
            minHeight: 40,
            maxHeight: 40,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop:20,
            borderBottomWidth: 1,
            borderColor: '#E6E8EC',
          }}>
            <TextInput
              style={styles.input1}
              placeholder="Email Address (Optional)"
              placeholderTextColor={'#777E91'}
              onChangeText={(e) => setEmail(e)}
            />
          </View>
          <View style={{
            width: "90%",
            minHeight: 40,
            maxHeight: 40,
            justifyContent: 'center',
            alignSelf: 'center',
            margin: 30,
            marginBottom:60,
            borderBottomWidth: 1,
            borderColor: '#E6E8EC',
          }}>
            <TextInput
              value={codeRefer}
              onChangeText={(e) => setCodeRefer(e)}
              style={styles.input1}
              placeholder="Referral Code (Optional)"
              placeholderTextColor={'#777E91'}
            />
          </View>

          <TouchableOpacity
            activeOpacity={1}
            onPress={playEvent}
            style={{
              alignSelf: 'center',
              width: 343,
              height: 64,
              position:"absolute",
              bottom:10

            }}
          >
            <Image
              source={imageConstant.letStart}
              style={styles.buttonStyle}
            />
          </TouchableOpacity>

        </KeyboardAwareScrollView>
       

        </View>
    )
}
export default AdditionalDetails

const styles = StyleSheet.create({
    imageStyle: {
        height: 64,
        width: 64,
        alignSelf:"center",
        marginTop:40
       
      },
    textStyle: {
        fontSize: 16,
        color: '#000000',
        fontFamily: fontConstant.regular,
        textAlign: 'center',
        marginTop: 40,
        alignSelf:'center'
      },
      numStyle: {
        fontSize: 16,
        color: '#353945',
        fontFamily: fontConstant.bold,
        textAlign: 'center',
        marginVertical:11,
        // marginTop: 50,
        alignSelf:'center'
        // left: '1%',
      },
      input1 : {
        // textAlign: 'center',
        // marginTop: 40,
        fontSize: 16,
        color:'black',
        fontWeight:'400',
        paddingVertical:0,
        fontFamily:fontConstant.regular,
       
        // backgroundColor : 'red'
      },
      buttonStyle:{
        width:343,
        height:64,
        marginTop: 5,
        alignSelf: 'center',
        position: Platform.OS === 'ios'? null :'absolute',
        bottom : Platform.OS === 'ios' ? null : 0,
        resizeMode: "contain",
      }


})