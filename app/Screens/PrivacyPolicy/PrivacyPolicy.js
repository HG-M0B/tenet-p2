import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList
} from 'react-native';
import React, {Component, useEffect, useRef, useState} from 'react';

import CustomHeader from '../../custom/CustomHeader';
import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import { baseURL } from '../../utils/URL';

const windowWidth = Dimensions.get('window').width;

// const width=Platform.width
const layout=windowWidth-20

const PrivacyPolicy = props => {
  const [policyData, setPolicyData]=useState([])
  useEffect(()=>{
    getPolicyData()
  },[])
  const {route, navigation} = props;

  const getPolicyData=async()=>{
    
    const response=await fetch(baseURL+'static/privacy-policy/')
    if(response.status==200)
    {
      const res= await response.json()
      console.log(res)
      setPolicyData(res)
    }
    else{
      console.log('status not ok',response.status)
    }
  }

  return (
    <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
      <CustomHeader
        headerText={'Privacy Policy'}
        img={imageConstant.back}
        navigation={props.navigation}
      />
      <ScrollView
      showsVerticalScrollIndicator={false}
      style={{backgroundColor:'#FFFFFF',flexDirection:'column'}}>
      <View style= {{width:'100%',borderBottomWidth:6, borderBottomColor:'#E6E8EC',}}>
        <View style={{width:'100%',marginBottom:10}}>
        {/* <Image source={imageConstant.betting2}
          style={{width:'100%',height:189}}
          /> */}
        </View>
        <View style={{width:'95%',alignSelf:'center'}}>
          <Text style={{color:'#141416',fontSize:28,fontWeight:'700',fontFamily:fontConstant.regular,marginBottom:10,}}>Tenet Privacy Policy</Text>
        </View>
      </View>

      <View style= {{width:'100%',backgroundColor:'white',height:'100%'}}>
      <View style={{width:'92%', alignSelf:'center',marginTop:25}}>
        <Text style={styles.textStyle}>{policyData.privacy_policy}</Text>
      </View>

      {/* <View style={{width:'92%', alignSelf:'center',marginTop:25,borderLeftWidth:4,borderLeftColor:'#C2F44E',paddingLeft:15}}>
        <Text style={styles.text1Style}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum</Text>
      </View>

      <View style={{width:'92%', alignSelf:'center',marginTop:25}}>
        <Text style={styles.textStyle}>2. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum</Text>
      </View>

      <View style={{width:'92%', alignSelf:'center',marginTop:25}}>
        <Text style={styles.textStyle}>3. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum</Text>
      </View> */}


      </View>

      </ScrollView>
      </SafeAreaView>
  )
}

export default PrivacyPolicy

const styles = StyleSheet.create({
  textStyle:{
    color:'#414246',
    fontWeight:'400',
    fontSize:16,
    fontFamily:fontConstant.regular,
    lineHeight:22,
    textAlign:'justify'
  },
  text1Style:{
    color:'#414246',
    fontWeight:'500',
    fontSize:20,
    fontFamily:fontConstant.italic,
    lineHeight:30
  },
})