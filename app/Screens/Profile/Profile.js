import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList,
  Modal,
  StatusBar,
  BackHandler
} from 'react-native';
import React, {Component, useEffect, useRef, useState} from 'react';

import CustomHeader from '../../custom/CustomHeader';
import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import { baseURL } from '../../utils/URL';
import { useFocusEffect, useIsFocused } from "@react-navigation/native";
import {useSelector} from 'react-redux';
import { useDispatch } from 'react-redux';
import {actions} from '../../redux/reducers'
import toastShow from '../../utils/Toast';

const windowWidth = Dimensions.get('window').width;

// const width=Platform.width
const layout=windowWidth-20

const Profile = props => {
  const [profileData, setProfileData]=useState(null)
  const [modalVisible, setModalVisible] = useState(false);
  const isFocused = useIsFocused();
  const {jwtToken} =  useSelector((state) => state.reducers);
  const dispatch =  useDispatch();
  useEffect(()=>{
    getProfileData();
    callMainApi();
  },[isFocused]);


  useFocusEffect(
    React.useCallback(() => {
      const backAction = () => {
       props.navigation.goBack();
        return true;
      };
  
      const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
      );
  
      return () => backHandler.remove();
    }, [])
  );

  const {route, navigation} = props;

  const getProfileData=async()=>{
    try
    {
      let tokenSent = "Token "+jwtToken;
      const response=await fetch(baseURL+'profile/',{ headers:{
        'Authorization': tokenSent,
        'Accept': 'application/json',
        
      }})
      if(response.status==200){
        const res=await response.json()
        setProfileData(res)
      }
      if(response.status == 401)
        {  const jsonData = await response.json()
          dispatch(actions.setIntroStatus("login"));
          toastShow(jsonData.detail,"red");
        
        }
     
    }
    catch(error)
    {
      toastShow(error.message,"red")
    }
  }

  const callMainApi = async () => {
    try {
      let response = await fetch(baseURL + `maintenance/`);
      if (response && response.status == 200) {
        let jsonData = await response.json();
        if (jsonData?.maintenance) {
          dispatch(actions.setIntroStatus("check"));
        }
      }
    }
    catch (error) {
      console.log("error-->", error)
    }
  }

  const logoutApiCall= async ()=>{
    try{
      let tokenSent = "Token "+jwtToken;
      console.log("tokenSent",tokenSent)
      const response = await fetch(baseURL+'auth/logout/',{
        method:"POST",
        headers:{
          'Authorization': tokenSent,
          'Accept': 'application/json',
          
        }
      })

      if(response.status == 200)
      {
        setModalVisible(!modalVisible);
        dispatch(actions.setIntroStatus("login"))
      
      }
      if(response.status == 401)
        {  const jsonData = await response.json()
          dispatch(actions.setIntroStatus("login"));
          toastShow(jsonData.detail,"red");
        
        }
    }
    catch(error)
    {
      toastShow(error.message,"red")
    }
  }


  return (
    <>
    <SafeAreaView style={{flex: 0,backgroundColor:'#0049F1' }}>
    </SafeAreaView>

    <SafeAreaView style={{flex: 1,backgroundColor:'#0049F1' }}>
     
    <View style={{backgroundColor: '#0049F1',width:'99%',height:55,alignSelf:'center', flexDirection: 'row',marginTop:10}}>

    <TouchableOpacity
          style={{
            marginVertical :4,
            position:'absolute',
            height: 36,
            width: 36,
            zIndex:1,
            left:15
          }}
          onPress={() => {
            navigation.toggleDrawer()
            }}>
             <Image source={imageConstant.whiteOptions}
             style={[styles.optionImageStyle]}
             />
          </TouchableOpacity>
       <Text style={{fontWeight:'700',fontSize:16,padding:10,textAlign:'center',width:'100%',color:'#FFFFFF'}}>Profile</Text>
       
       
        
     
    </View>




        <View style={{ backgroundColor: '#F5F5F5', width: '100%', height: '100%', alignItems: 'center', paddingTop: 20 }}>

          <View style={{
            backgroundColor: '#FFFFFF',
            width: "90%",
            alignSelf: "center",
            marginVertical: 10,
            borderRadius: 10,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.32,
            shadowRadius: 5.46,
            elevation: 9,


          }}>


            <View style={{ flexDirection: 'row', width: '100%', padding: 20, justifyContent: 'space-between' }}>

              <Image
                source={profileData?.profile_photo !== null ? { uri: profileData?.profile_photo?.file } : imageConstant.Profile_image}
                style={{ width: 72, height: 72, borderRadius: 72 / 2, alignSelf: 'center', alignItems: 'center', }}
              />


              <View style={{ width: "70%", paddingLeft: 10, }}>
                <Text style={{ fontSize: 20, color: '#141416', fontFamily: fontConstant.medium, lineHeight: 25, marginBottom: 3, marginTop: 4 }}>
                  {profileData?.user_name}
                </Text>
                <Text style={{ fontWeight: '400', fontSize: 16, color: '#777E91', fontFamily: fontConstant.regular, lineHeight: 20, marginBottom: 3 }}>{profileData?.phone_number}</Text>
                <Text style={{ fontWeight: '400', fontSize: 16, color: '#777E91', fontFamily: fontConstant.regular }}>{profileData?.email}</Text>
              </View>

              <TouchableOpacity
                onPress={() => navigation.navigate("EditProfile", { data: profileData })}
                style={{ alignSelf: 'center', alignItems: 'center', width: 23, height: 23 }}>
                <Image source={imageConstant.Edit}
                  style={{ width: 22, height: 22, resizeMode: 'contain' }}
                />
              </TouchableOpacity>
            </View>


          </View>

          <View style={{ 
            backgroundColor: '#FFFFFF', 
          width: '90%', 
          padding: 20, 
          marginTop:10,
          paddingTop: 10, 
          paddingBottom: 10, 
          borderRadius: 10,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 4,
            },
            shadowOpacity: 0.32,
            shadowRadius: 5.46,
            elevation: 9
          }}
           >
            <View style={{}}>


              {/* <TouchableOpacity
        onPress={() => {
          navigation.navigate('WatchList')
          }}
        style={{alignItems:'center',flexDirection:'row',paddingTop:12,paddingBottom:12,width:'100%',borderBottomWidth:1, borderBottomColor:'#E6E8EC'}}
          >
        <Image source={imageConstant.watchlist}
          style={{width:16,height:21}}
        />
        <Text style={{color:'#141416',fontWeight:'500',fontSize:16,marginLeft:15,fontFamily:fontConstant.regular}}>Watchlist</Text>
        <Image source={imageConstant.twoArrow}
          style={{width:9,height:9,position:'absolute',right:0}}
        />
        </TouchableOpacity> */}

              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Referral')
                }}
                style={{
                  alignItems: 'center', flexDirection: 'row', paddingTop: 12, paddingBottom: 12, width: '100%',
                  borderBottomWidth: 1,
                  borderBottomColor: '#E6E8EC'
                }}
              >
                <Image source={imageConstant.referEarn}
                  style={{ width: 21, height: 21 }}
                />
                <Text style={{ color: '#141416', fontWeight: '500', fontSize: 16, marginLeft: 15, fontFamily: fontConstant.regular }}>Refer & Earn</Text>
                <Image source={imageConstant.twoArrow}
                  style={{ width: 9, height: 9, position: 'absolute', right: 0 }}
                />
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('HowToTrade')
                }}
                style={{ alignItems: 'center', flexDirection: 'row', paddingTop: 12, paddingBottom: 12, width: '100%' }}
              >
                <Image source={imageConstant.trade}
                  style={{ width: 16, height: 21 }}
                />
                <Text style={{ color: '#141416', fontWeight: '500', fontSize: 16, marginLeft: 15, fontFamily: fontConstant.regular }}>How to Trade</Text>
                <Image source={imageConstant.twoArrow}
                  style={{ width: 9, height: 9, position: 'absolute', right: 0 }}
                />
              </TouchableOpacity>

              {/* <TouchableOpacity
        onPress={() => {
          navigation.navigate('Invoice')
          }}
        style={{alignItems:'center',flexDirection:'row',paddingTop:12,paddingBottom:12,width:'100%',}}
          >
        <Image source={imageConstant.invoice}
          style={{width:24,height:24,marginLeft:-4}}
        />
        <Text style={{color:'#141416',fontWeight:'500',fontSize:16,marginLeft:15,fontFamily:fontConstant.regular}}>Invoice</Text>
        <Image source={imageConstant.twoArrow}
          style={{width:9,height:9,position:'absolute',right:0}}
        />
        </TouchableOpacity> */}



              {/* <TouchableOpacity
        onPress={() => {
          navigation.navigate('AccountDetails')
          }}
        style={{alignItems:'center',flexDirection:'row',paddingTop:12,paddingBottom:12,width:'100%',borderTopWidth:1, borderTopColor:'#E6E8EC'}}
          >
        <Image 

          source={imageConstant.bank}
          style={{width:21,height:21,tintColor:"#0049F1"}}
        />
        <Text style={{color:'#141416',fontWeight:'500',fontSize:16,marginLeft:15,fontFamily:fontConstant.regular}}>Account details</Text>
        <Image source={imageConstant.twoArrow}
          style={{width:9,height:9,position:'absolute',right:0}}
        />
        </TouchableOpacity> */}



            </View>
          </View>



          <View style={{ backgroundColor: '#FFFFFF', width: '90%', padding: 20, bottom: 80, position: 'absolute', borderRadius: 10, shadowColor: 'black', shadowOpacity: 0.25, shadowRadius: 5, shadowOffset: { height: 0.5, width: 0.2 } }}>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(!modalVisible)
              }}
              style={{ alignItems: 'center', flexDirection: 'row', width: 120 }}
            >
              <Image source={imageConstant.Logout}
                style={[styles.logoutImageStyle]}
              />
              <Text style={{ color: '#ED5A50', fontSize: 16, marginLeft: 15 }}>Log Out</Text>
            </TouchableOpacity>
          </View>
        </View>




    <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        supportedOrientations={['portrait', 'landscape']}
        onRequestClose={() => {
            setModalVisible(!modalVisible)
        }}>
        <TouchableOpacity
            activeOpacity={1}
        onPress={() => setModalVisible(!modalVisible)}
        style={styles.centeredView}>
            <TouchableOpacity
            activeOpacity={1}
            style={styles.modalView}>
                <Image
                  source={imageConstant.logoutLogo}
                  resizeMode='contain'
                  style={{width:51,height:51,marginTop:30}} 
                />
                <Text style={{marginTop:30,color :'black',fontWeight:'500',fontSize:16,fontFamily:fontConstant.regular}}>Are you sure to logout?</Text>

                <View style={{flexDirection:'row',width:'100%',marginTop:40,justifyContent:'space-between'}}>

                <TouchableOpacity
                onPress={logoutApiCall}
                >
                <Image
                style={{
                  width:167,
                  height:50,
                }}
                source={imageConstant.confirmButton}
                />
                </TouchableOpacity>

                <TouchableOpacity
                onPress={() => setModalVisible(!modalVisible)}
                >
                <Image
                style={{
                  width:167,
                  height:44,
                }}
                source={imageConstant.cancelButton}
                />
                </TouchableOpacity>
                </View>
            </TouchableOpacity>
        </TouchableOpacity>
    </Modal>

    </SafeAreaView>
    </>
    

  )
}

export default Profile

const styles = StyleSheet.create({
  appImageStyle: {
    marginVertical :10,
    height: 19.12,
    width: 79.43,
  },
  bellImageStyle: {
      marginVertical :4,
      right:15,
      position:'absolute',
      height: 36,
      width: 36,
      resizeMode:'contain'
  },
  optionImageStyle: {
      // marginVertical :4,
      // position:'absolute',
      height: 36,
      width: 36,
      // left:50
  },
  logoutImageStyle: {
    // marginVertical :4,
    // position:'absolute',
    height: 24,
    width: 24,
    // left:50
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // marginTop: 22,
    // bottom:0,
    // position:'absolute',
    // width:'100%',
    // height:'100%',
    backgroundColor:'rgba(0, 0, 0, 0.4)' 
  },
  modalView:{
    bottom:0,
    position:'absolute',
    // margin: 20,
    backgroundColor: "white",
    borderTopStartRadius: 20,
    borderTopEndRadius: 20,
    padding: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width:'100%',
    height:312,
    // backgroundColor:'red'
  }
})