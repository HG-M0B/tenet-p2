



import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList,
  Pressable,
  Modal,
  BackHandler
} from 'react-native';
import React, {Component, useRef, useState} from 'react';

import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import CustomHeader from '../../custom/CustomHeader';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view';
import { useFocusEffect } from '@react-navigation/native';
import toastShow from '../../utils/Toast';
import { baseURL } from '../../utils/URL';
import { useSelector } from 'react-redux';
import validation from '../../utils/validation'
const windowWidth = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const layout=windowWidth-20

let initialState = {
  pan:"",
  name:"",
  email:"",
  address:""
}

const KycVerification = props => {
const [iState,updateState] =  useState(initialState);
const {route, navigation} = props;
const {jwtToken,appState,walletBalance} = useSelector((state)=>state.reducers)

const {
  accountNumber,
  upiVpa,
  ifsc,
  aadhaar,
  gst,
  name,
  email,
  address
} =  iState;
const [pan, setPan] = useState("");
const [kycfailVisible, setKycfailVisible] = useState(false);
const [kycpassVisible, setKycpassVisible] = useState(false);
useFocusEffect(
  React.useCallback(() => {
    const backAction = () => {
     props.navigation.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, [])
);

const validationCheck=()=>{
  // pan1=pan.toUpperCase();
  // console.log(pan1);\
  setPan(pan.toUpperCase());
  console.log(pan);
  let panRegx = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/
  // props.navigation.navigate("AccountDetails")
 {
   name == "" ?
   toastShow("Name is required","red") 
   : pan == "" 
   ? toastShow("Pan is required","red")
   : email == ""
   ? toastShow("Email is required","red")
   : validation.isEmail(email)
   ? toastShow("Email invalid format","red")
   : address == ""
   ? toastShow("Address is required","red")
   : panRegx.test(pan.trim())
   ? kycVerification()
   :  toastShow("Pan format is not right","red")
 } 
}

const kycVerification = async () => {
  console.log('---->')
  try {
    let tokenSent = "Token " + jwtToken;
    console.log(iState.name);
    console.log(iState.email);
    console.log(iState.address);
    console.log(pan);
    let reqData = {
      name: iState.name.trim(),
      email:iState.email.trim(),
      address1:iState.address.trim(),
      pan: pan.trim()

    }
    let response = await fetch(baseURL + 'withdrawn/kyc', {
      method: "POST",
      headers: {
        'Authorization': tokenSent,
        'Accept': 'application/json',
        'Content-Type': 'application/json',

      },
      body: JSON.stringify(reqData)
    })
    console.log("ressss---->", response)
    if (response && response.status == 200) {
      let jsonData = await response.json();
      console.log("jsonDatajsonData==>",jsonData)
      if (jsonData && jsonData.status == 200) {
        //toastShow(jsonData.message, "#228B22")
        setKycpassVisible(!kycpassVisible)
      }
      else {
        //toastShow(jsonData.message, "red");
        setKycfailVisible(!kycfailVisible)
      }


    }
    else {
      let jsonData = await response.json()
      console.log(jsonData)
      toastShow(jsonData.message, "red")
    }

  }
  catch (error) {
    console.log('Error---->',error)
  }
}
// console.log("name",name,"pan",pan)
return (
    <View style={{ backgroundColor: '#FFFFFF', flex: 1, }}>
      <CustomHeader
        headerText={'KYC Verification'}
        img={imageConstant.back}
        navigation={props.navigation}
      />

    <KeyboardAwareScrollView
      showsVerticalScrollIndicator={false}
      extraHeight={100}
      contentContainerStyle={{
        paddingBottom:150,

      }}>
      <View style={{ borderBottomWidth: 1, borderBottomColor: '#E6E8EC' }}></View>

      {/* <View style={{ marginTop: 20, alignItems: 'center' }}>
        <Image
          style={styles.imageStyle}
          source={imageConstant.logo}
        />
      </View> */}

      <View>
        <Text style={styles.textStyle}>Please enter information as written in your Pancard</Text>
      </View>
      <View style={{
        width: "90%",
        marginTop:40,
        justifyContent: 'center',
        alignSelf: 'center',
        borderBottomWidth: 1,
        // borderColor: '#E6E8EC',
      }}>
        <TextInput
          style={styles.input1}
          onChangeText={(text) => (updateState({ ...iState, name: text }))}
          placeholder="Shyam Thakur"
          placeholderTextColor={'#aeb0b5'}
        />
      </View>
      <Text style={styles.textStyle2}>Full name as written in your Pan Card</Text>

      <View style={{
        width: "90%",

        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 10,
        borderBottomWidth: 1,
        // borderColor: '#E6E8EC',
      }}>
        <TextInput
          style={styles.input1}
          onChangeText={(text) => (setPan(text))}
          placeholder="USYPU3586B"
          placeholderTextColor={'#aeb0b5'}
          
        />
      </View>
      <Text style={styles.textStyle2}>Pan Card No as written in your Pan Card</Text>
      <View style={{
        width: "90%",
        marginTop:10,
        justifyContent: 'center',
        alignSelf: 'center',
        borderBottomWidth: 1,
        
      }}>
          <TextInput
          style={styles.input1}
          onChangeText={(text) => updateState({ ...iState, email: text })}
          placeholder="shyamthakur7635@gmail.com"
          placeholderTextColor={'#aeb0b5'}
          />
      </View>
      <View style={{
        width: "90%",
        marginTop:10,
        //justifyContent: 'center',
        alignSelf: 'center',
        //borderBottomWidth: 1,
        //borderColor:"#E6E8EC",
        //borderLeftWidth: 1,
        //borderRightWidth: 1,
        //borderTopWidth:1,
        height:100,
        borderBottomWidth:1,
        // marginTop:4,
        // marginBottom:10,
        //borderColor: '#E6E8EC',
        //elevation:2
      }}>
          <TextInput
          style={styles.input2}
          multiline={true}
          onChangeText={(text) => updateState({ ...iState, address: text })}
          placeholder="Your Address"
          placeholderTextColor={'#aeb0b5'}
          />
      </View>

      <TouchableOpacity
        style={{marginTop:15}}
        onPress={validationCheck}>
        <Image
          style={{ height:50, width: 250, alignSelf: 'center', resizeMode: 'contain' }}
          source={imageConstant.kycButton}
        />
        {/* <Text style={{height:50, width: 250, alignSelf: 'center', resizeMode: 'contain',color:'blue'}}>Verify KYC</Text> */}
      </TouchableOpacity>

      {/* <TouchableOpacity
      style={{marginTop:15}}
      onPress={()=>{
        setKycpassVisible(!kycpassVisible)
      }}
      >
        <Image
          style={{ height:50, width: 250, alignSelf: 'center', resizeMode: 'contain' }}
          source={imageConstant.kycButton}
        />
      </TouchableOpacity> */}
      <Modal
     animationType="slide"
     transparent={true}
     visible={kycfailVisible}
     onRequestClose={() => {
      //  Alert.alert('Modal has been closed.');
       setKycfailVisible(!kycfailVisible)
     }}
    >
     <View style={styles.centeredView}>
      <View style={styles.modalView}>
      <TouchableOpacity
        onPress={() => { setKycfailVisible(!kycfailVisible) }}

        style={{
          right: 10,
          top: 10,
          position: 'absolute'
        }}
      >
        <Image
          style={styles.cancelButtonStyle}
          source={imageConstant.cancelImg}
        />
      </TouchableOpacity>
      <Text style={styles.modalText}>KYC Fail</Text>
      <View style={{flexDirection:'row',marginTop:10,marginLeft:5}}>
        <Image
        source={imageConstant.wrongmark}
        style={{height:40,width:15,marginTop:20}}
        />
        <View style={{marginLeft:30}}>

        
        <Text style={styles.textStyle3}>Your Information filled here does not match to your Pan Card.</Text>
        <Text style={styles.textStyle3}>Please fill the Information correctly as written in your Pan card.</Text>
        </View>
      </View>
      </View>
     </View>
    </Modal>
    <Modal
     animationType="slide"
     transparent={true}
     visible={kycpassVisible}
     onRequestClose={() => {
      //  Alert.alert('Modal has been closed.');
       setKycpassVisible(!kycpassVisible)
     }}
    >
     <View style={styles.centeredView}>
      <View style={styles.modalView}>
      <TouchableOpacity
        onPress={() => { setKycpassVisible(!kycpassVisible);props.navigation.navigate("Wallet") }}

        style={{
          right: 10,
          top: 10,
          position: 'absolute'
        }}
      >
        <Image
          style={styles.cancelButtonStyle}
          source={imageConstant.cancelImg}
        />
      </TouchableOpacity>
      <Text style={styles.modalText}>KYC Verified</Text>
      <View style={{flexDirection:'row',marginTop:10}}>
        <Image
        source={imageConstant.tickmark}
        style={{height:30,marginTop:20}}
        />
        <View style={{marginLeft:20}}>

        
        <Text style={styles.textStyle3}>Thank you for confirming your KYC with us.</Text>
        <Text style={styles.textStyle3}>Now you can withdraw money from the wallet</Text>
        </View>
      </View>
      </View>
     </View>
    </Modal>


    </KeyboardAwareScrollView>
    

    
    </View>
)
}

export default KycVerification

const styles = StyleSheet.create({
  imageStyle: {
      height: 64,
      width: 64,
     
    },
    modalText: {
      marginTop: 5,
      fontSize:18,
      textAlign: 'center',
      color: '#000000',
      fontFamily: fontConstant.bold
    },
    buttonClose: {
      borderRadius: 20,
    padding: 10,
    elevation: 2,
      backgroundColor: '#2196F3',
    },
  textStyle: {
      fontSize: 15,
      color: '#000000',
      fontFamily: fontConstant.bold,
      textAlign: 'center',
      marginTop: 40,
      alignSelf:'center'
    },
    textStyle3:{
      color:'#000000',
      fontSize:14,
      marginTop:5,
      fontFamily:fontConstant.medium
    },
    textStyle2:{
      fontSize:12,
      color:'#030408',
      fontFamily: fontConstant.regular,marginLeft:18
    },
    cancelButtonStyle:{
      width:30,
      height:30,
    },
    numStyle: {
      fontSize: 16,
      color: '#353945',
      fontFamily: fontConstant.bold,
      textAlign: 'center',
      marginVertical:11,
      // marginTop: 50,
      alignSelf:'center'
      // left: '1%',
    },
    input1 : {
      // textAlign: 'center',
      // marginTop: 40,
      fontSize: 16,
      color:"#121413",
      fontWeight:'400'
      // backgroundColor : 'red'
    },
    input2:{
      fontSize: 16,
      color:"#121413",
      fontWeight:'400',
      // borderBottomColor: '#000', // Add this to specify bottom border color
      // borderBottomWidth: 2 
      // borderColor:"#808080",
      // borderLeftWidth: 1,
      // borderRightWidth: 1,
      // borderTopWidth:1,
      // borderBottomWidth:1,
      // paddingLeft: 5,
      // paddingRight: 5,
      // elevation:2,
      
      
    },
    centeredView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 10,
      height:'60%',
      width:'100%'
    },
    modalView: {
      margin: 20,
      backgroundColor: 'white',
      borderRadius: 20,
      height:250,
      width:'90%',
      padding: 35,
      alignItems: 'center',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
    },
    buttonStyle2:{
      // width:163,
      width:"47%",
      height:56,
      backgroundColor:'#FFFFFF',
      // backgroundColor:'pink',
      borderRadius:8,
      borderColor:"#808080",
      borderBottomWidth:1,
      flexDirection:'row',
      // alignItems:'center',
      // justifyContent:'space-evenly',
      alignItems:'center',
      alignSelf:'center',
      padding:10,
      marginTop:15
    },
    buttonStyle: {
      marginTop: 5,
      alignSelf: 'center',
      color: '#FFFFFF',
      fontFamily: fontConstant.bold,
      fontSize: 16,
    },

})