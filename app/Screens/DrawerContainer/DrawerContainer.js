import { Text, View,Image, TouchableOpacity, ScrollView, SafeAreaView, Modal, Switch ,Appearance, StyleSheet,Dimensions, Linking} from 'react-native';
import React, { Component, useState,useContext, useEffect } from 'react';
import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import { Layout } from 'react-native-reanimated';
import { baseURL } from '../../utils/URL';
import {useSelector,useDispatch} from 'react-redux';
import {actions} from '../../redux/reducers'
import { useIsFocused } from "@react-navigation/native";
import DeviceInfo from 'react-native-device-info';
import {name as app_name, version as app_version}  from '../../../package.json';
const windowWidth = Dimensions.get('window').width;

const layout=windowWidth-20

const DrawerContainer = (props) => {
    const {route, navigation} = props;
    const [profileData,setProfile] = useState(null)
   
    const {jwtToken} =  useSelector((state) => state.reducers);
    const isFocused = useIsFocused();
    const dispatch =  useDispatch();
    useEffect(()=>{
        getProfileImage();
        getFunds();
    },[isFocused]);

    const getFunds = async () => {
        try {
          let tokenSent = "Token " + jwtToken;
          let response = await fetch(baseURL + 'funds/',
            {
              headers: {
                'Authorization': tokenSent,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
    
              }
            })
          
          if (response && response.status === 200) {
            let jsonData = await response.json();
           dispatch(actions.setWalletBalance(jsonData));
    
          }
          else {
            let jsonData = await response.json();
            toastShow(jsonData?.errors, "red")
          }
        }
        catch (error) {
          cosole.log("Error on wallet page", error)
        }
      }
    const getProfileImage = async () => {
        try {
            let tokenSent = "Token " + jwtToken;
            let response = await fetch(baseURL+"profile/", {
                method: "GET",
                headers: {
                    "Authorization": tokenSent,
                    'Content-Type': 'application/json'
                }
            })
    
            if (response && response.status == 200) {
                 let jsonData = await response.json();
                 setProfile(jsonData);
                 dispatch(actions.setReferCode(jsonData))
            }
        }
        catch (error) {
            setFlag(false)
            console.log(error)
    
        }
    }
    
    return (
        <SafeAreaView style={{ backgroundColor: '#FFFFFF', width:'100%', height:'100%' }}>
            <View style={{backgroundColor:'#FFFFFF', width:'100%',flexDirection:'row', alignItems:'center',padding:20,borderBottomWidth:4, borderBottomColor:'#E6E8EC', marginBottom:15}}>
            <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => navigation.navigate("My Profile")}
                    >
                        <Image
                            source={profileData?.profile_photo !==null ? {uri: profileData?.profile_photo?.file} :imageConstant.Profile_image}
                            style={styles.Img}
                            resizeMode='contain'/>
                    </TouchableOpacity>
                    <Text style={{fontSize:20, color:"#141416", fontWeight:'700',fontFamily:fontConstant.regular,paddingLeft:15,width:"70%"}}>{profileData?.user_name}</Text>
            </View>
            <View style={{flexDirection:'column',borderBottomWidth:1, borderBottomColor:'#E6E8EC',}}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => props.navigation.navigate('AboutUs')}
                    style={{flexDirection:'row',paddingLeft:15,backgroundColor:'white', alignItems:'center',padding:20,}}
                    >
                    <View style={{width:30, }}>
                    <Image
                        source={imageConstant.aboutUs}
                        resizeMode='contain'
                        style={{ width:15,height:18, }} />
                    </View>
                    <Text style={styles.Text1}>About Us</Text> 
                </TouchableOpacity>
            </View>

            <View style={{flexDirection:'column',borderBottomWidth:1, borderBottomColor:'#E6E8EC',}}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => props.navigation.navigate('TermsConds')}
                    style={{flexDirection:'row',paddingLeft:15,backgroundColor:'white', alignItems:'center',padding:20,}}
                    >
                    <View style={{width:30, }}>
                    <Image
                        source={imageConstant.tAndc}
                        resizeMode='contain'
                        style={{ width:15,height:18, }} />
                    </View>
                    <Text style={styles.Text1}>Terms & Conditions</Text> 
                </TouchableOpacity>
            </View>

            <View style={{flexDirection:'column',borderBottomWidth:1, borderBottomColor:'#E6E8EC',}}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => props.navigation.navigate('PrivacyPolicy')}
                    style={{flexDirection:'row',paddingLeft:15,backgroundColor:'white', alignItems:'center',padding:20,}}
                    >
                    <View style={{width:30, }}>
                    <Image
                        source={imageConstant.privacypolicy}
                        resizeMode='contain'
                        style={{ width:15,height:18, }} />
                    </View>
                    <Text style={styles.Text1}>Privacy Policy</Text> 
                </TouchableOpacity>
            </View>

            <View style={{flexDirection:'column',borderBottomWidth:1, borderBottomColor:'#E6E8EC',}}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => props.navigation.navigate('ContactUs')}
                    style={{flexDirection:'row',paddingLeft:15,backgroundColor:'white', alignItems:'center',padding:20,}}
                    >
                    <View style={{width:30, }}>
                    <Image
                        source={imageConstant.contactUs}
                        resizeMode='contain'
                        style={{ width:15,height:18, }} />
                    </View>
                    <Text style={styles.Text1}>Contact Us</Text> 
                </TouchableOpacity>
            </View>

 

            <View style={{flexDirection:'column'}}>

                <TouchableOpacity
                onPress={()=>{
                    Linking.openURL('https://t.me/tenetapp');
                }}
                    activeOpacity={0.8}
                    // onPress={() => props.navigation.navigate('MyProfile')}
                    style={{flexDirection:'row',paddingLeft:15,backgroundColor:'white', alignItems:'center',padding:20,}}
                    >
                    <View style={{width:30, }}>
                    <Image
                        source={imageConstant.telegram}
                        resizeMode='contain'
                        style={{ width:20,height:20}} />
                    </View>
                    <Text style={styles.Text1}>Join Telegram</Text>
                </TouchableOpacity>
            </View>

            <View style={{position:"absolute",bottom:20,width:"90%",alignSelf:"center"}}>
                <Text style={{fontSize:12,fontWeight:'400',color:'#141416',fontFamily:fontConstant.italic}}>App Version: {app_version}</Text>
            </View>
        </SafeAreaView>
    );
}
export default DrawerContainer

const styles = StyleSheet.create({
    Img: {
        height: 72,
        width: 72,
        borderRadius:72/2,
    },
    Text1:{
        fontSize:16, 
        fontWeight:'500',
        color:"#141416",
        fontFamily:fontConstant.regular
    }
})