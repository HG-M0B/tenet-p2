import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList,
  Pressable,
  RefreshControl,
  Modal,
  BackHandler,
  PermissionsAndroid
} from 'react-native';
import BottomSheet from '@gorhom/bottom-sheet';
import React, {Component, useRef, useState, useEffect,useCallback, useMemo } from 'react';

import { colorConstant, imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { baseURL } from "../../utils/URL";
import moment from 'moment';
import {useSelector} from 'react-redux';
import { useDispatch } from 'react-redux';
import {actions} from '../../redux/reducers'
import toastShow from '../../utils/Toast';
import { Dropdown } from 'react-native-element-dropdown';
import { useFocusEffect } from '@react-navigation/native';
import SwipeButton from 'rn-swipe-button';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import { Width } from '../../dimension/dimension';
const windowWidth = Dimensions.get('window').width;
import { useScrollToTop } from '@react-navigation/native';
import RNFetchBlob from "rn-fetch-blob";
const Height = Dimensions.get('window').height;
import {version as app_version}  from '../../../package.json';

const layout=windowWidth-20

const HomePage = (props) => {
  const {navigation} = props;
  const {jwtToken,walletBalance} =  useSelector((state) => state.reducers);
  const [catData, setCatData]=useState([])
  const [eventData,setEventData]=useState([])
  const [selected,setSelected]=useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [detailsModal, setDetailsModal] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [quantity,setQuantity]=useState([5]);
  const [price,setPrice]=useState([7]);
  const [eventDetails,setEventDetails] = useState(null);
  const [optionChoose,setOptionChoose] = useState("");
  const [orderData,setOrderData] =  useState(null);
  const [mapsuggestedData,setMapSuggestData] = useState([]);
  const [sliderShow,setSliderShow] =  useState(false)
  const [updateFlag,setUpdateFlag] = useState(false);
  const [versionInfo,setVersionInfo] = useState(null);
  const [suggestedQuantity,setSuggestedQuantity] = useState(0);
  const dispatch = useDispatch();
  const reff = React.useRef(0);


  useScrollToTop(reff);
  useEffect(()=>{
    let subs = navigation.addListener('focus',()=>{
      getCatData();
      setSelected(0);
      getEventData();
      callMainApi();
      getVersion();
  })
 

    return ()=> {
      subs;
    }
  },[])

  useFocusEffect(
    React.useCallback(() => {
      const backAction = () => {
        setModalVisible(!modalVisible);
        return true;
      };
  
      const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
      );
  
      return () => backHandler.remove();
    }, [])
  );

  useEffect(() => {
    if (orderData !== null) {
      suggestData()
    }
  }, [orderData])



 

  const onRefresh = () => {
    setRefreshing(true);
    getCatData();
    getEventData();
  }


  const getCatData = async() => {
   try
   {
    let tokenSent = "Token "+jwtToken;
    const response = await fetch(baseURL+'category/get-categories/',
    {
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': tokenSent,
    }
  })
    if(response.status==200){
      const res=await response.json()
      setCatData(res);
      setRefreshing(false);
    }
    if(response.status==401){
      dispatch(actions.setIntroStatus("login"));
      toastShow("Either user blocked or deleted","red")
    }
   
   }
   catch(error)
   {
    toastShow(error.message,"red")
   }
  }

  const getVersion = async() => {
    try
    {
     const response = await fetch(baseURL+'maintenance/')
     if(response &&  response.status==200){
       const res=await response.json();
       setVersionInfo(res);
       if(app_version != res?.app_version)
       {
        //  console.log('====================================');
        //  console.log("pkg app",app_version,'res app',res?.app_version);
        //  console.log('====================================');
         setUpdateFlag(true) 
       }
     }
    }
    catch(error)
    {
    console.log(error)
    }
   }

  const getEventData = async () => {
    try {
      let tokenSent = "Token " + jwtToken;
      const response = await fetch(baseURL + 'events/get-events/',
        {
          headers: {
            'Authorization': tokenSent,
            'Accept': 'application/json',
            'Content-Type': 'application/json',

          }
        })
      if (response && response.status == 200) {
        const res = await response.json();
        setRefreshing(false)
        setEventData(res)
      }
    }
    catch (error) {
      setRefreshing(false);
      toastShow(error.message, "red")
    }
  }


 

  let data2=[{
    id:1,
    text:`Take 'Yes' or 'No' Position on any event.`
    },
    {
      id:2,
      text:'Select quantity and place order.'
    },
    {
      id:3,
      text:'After event outcome, withdraw your earnings.'
    }
  ]

  let DATA=[
  {
    id:0,
    name:'All'
  },
  
  ]

  // *************Api call
  const getEventDataByID = async (id, otp, money) => {
    getTradeOrderCall(id);
    setOptionChoose(otp);
    setPrice([money]);
    setSliderShow(false);
    try {
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL + `events/get-events/${id}`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': tokenSent
        }
      })
      if (response.status == 200) {
        let res = await response.json();
        setEventDetails(res);
        setDetailsModal(!detailsModal);


      }
    }
    catch (error) {
      toastShow(error.message, "red");
      setLoader(false)
    }

  }

  const tradeCall = async (id) => {
    try {
      let reqData = {
        amount: price?.[0],
        quantity: quantity?.[0],
        selected_option: optionChoose,
        binary_event: id
      }
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL + `orders/`, {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': tokenSent
        },
        body: JSON.stringify(reqData)
      })
      if (response && response.status == 200) {
        let jsonData = await response.json();

        if (jsonData?.status == 200) {
          setDetailsModal(!detailsModal);
          toastShow(jsonData?.data, "red");
        }

      }



      if (response && response.status == 201) {
        setDetailsModal(!detailsModal);
        navigation.navigate("TradeSuccess", {
          data: price * quantity
        });
      }

    }
    catch (error) {
      toastShow(error, "red")
    }

  }

  const checkBalance = (id) => {
    if (price * quantity > walletBalance) {
      toastShow("You don't have enough funds to proceed", "red");
      setDetailsModal(!detailsModal)
    }
    else {
      tradeCall(id)
    }
  }


  //****************Matches function****/
  const getTradeOrderCall = async(id)=>{
    try{
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL +`orders/?event_id=${id}`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': tokenSent
        }
      });
      
      if(response.status === 200)
      {
        let jsonData = await response.json();
        setOrderData(jsonData);

      }

    }
    catch(error){
      toastShow(error.message, "red")
    }
  }


  const suggestData = () => {

    let tempObj = { "yes": [], "no": [] }
    let tempData = orderData;
    let obj = {};
    // **********Group by obj**********
    for (let i = 0; i < tempData?.length; i++) {

      if (tempData?.[i]?.selected_option == "yes") {
        tempObj["yes"] = [...tempObj["yes"], tempData?.[i]]
      }
      if (tempData?.[i]?.selected_option == "no") {
        tempObj["no"] = [...tempObj["no"], tempData?.[i]]
      }

    }

    let yesArr = tempObj?.["yes"];
    let noArr = tempObj?.["no"]

    yesArr?.forEach((item, index) => {
      yesArr[index] = { amount: 10 - item?.amount, qty: item?.quantity - item?.matched_quantity, option: "No" }
    })
    noArr?.forEach((item, index) => {
      noArr[index] = { amount: 10 - item?.amount, qty: item?.quantity - item?.matched_quantity, option: "Yes" }
    })
    let yesFinalArray = [];
    let noFinalArray = [];

    let sumYesArray = yesArr?.reduce(function (acc, curr) {
      let findIndex = acc.findIndex(item => item.amount === curr.amount);

      if (findIndex === -1) {
        acc.push(curr)
      } else {

        acc[findIndex].qty += curr.qty
      }
      return acc;
    }, [])

    let sumNoArray = noArr?.reduce(function (acc, curr) {
      let findIndex = acc.findIndex(item => item.amount === curr.amount);

      if (findIndex === -1) {
        acc.push(curr)
      } else {

        acc[findIndex].qty += curr.qty
      }
      return acc;
    }, [])


    let finalArray = [...sumYesArray, ...sumNoArray];
    setMapSuggestData(finalArray);
  }

  useEffect(() => {
    gettingSingleSuggestion();
  }, [price, mapsuggestedData]);
    
    const gettingSingleSuggestion=()=>{
    let tempData = mapsuggestedData;
    let qty = tempData?.filter((item,index) => item?.amount === price[0]);
    setSuggestedQuantity(qty?.[0]);
    }


    // ********Download funciton******
    const StartDownload=()=>{
      let date = new Date();
      let FILE_URL = versionInfo?.app?.file;
      let file_name = FILE_URL.toString().split("/")[FILE_URL.toString().split("/").length-1];
      let file_ext = file_name.split(".")[file_name.split(".").length-1];
      const { config, fs } = RNFetchBlob;
      let RootDir = fs.dirs.DCIMDir;
      try{
        let options = {
          fileCache: true,
          // appendExt: file_ext,
          addAndroidDownloads: {
            path:RootDir+"/Tenet" + Math.floor( date.getDate() + date.getSeconds()/2) + file_name,
            description:'Downloading your file',
            notification: true,
            // useDownloadManager works with Android only
            useDownloadManager: true,   
          },
        };
        config(options)
          .fetch('GET', FILE_URL)
          .then(res => {
            console.log('res -> ', JSON.stringify(res));
            alert('File Downloaded Successfully.');
          })
          .catch(error=>console.log(error));
      }
      catch(error)
      {
        console.log("error",error)
      }
  }
  

  const takePermissionForDownload = async()=>{
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: 'Downloading the app ',
          message:'Needs permissions for downloading',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            StartDownload();
            setUpdateFlag(false)
      } else {
        console.log('permission denied');
        // setUpdateFlag(false)
      }
    } catch (err) {
      console.warn(err);
    }
  }
  
  const callMainApi = async () => {
    try {
      let response = await fetch(baseURL + `maintenance/`);
      if (response && response.status == 200) {
        let jsonData = await response.json();
        if (jsonData?.maintenance) {
          dispatch(actions.setIntroStatus("check"));
        }
      }
    }
    catch (error) {
      console.log("error-->", error)
    }
  }
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#FFFFFF', }}>
      <View style={{width: '97%', padding: 5, alignSelf: 'center', marginTop: Platform.OS === 'ios' ? null : 10, flexDirection: 'row' , alignItems:"center"}}>
        <TouchableOpacity
          style={{ height: 36, width: 36}}
          onPress={() => {
            navigation.toggleDrawer()
          }}>
          <Image source={imageConstant.options}
            style={[styles.optionImageStyle]}
          />
        </TouchableOpacity>



        <Image
          style={styles.appImageStyle}
          source={imageConstant.appName}
        />




        {/* <TouchableOpacity
          style={{
            position: 'absolute',
            right:7,
            height: 36,
            width: 36,

          }}
          onPress={() => {
            navigation.navigate("Notifications")
          }}>
          <Image source={imageConstant.bell}
            style={[styles.bellImageStyle]}
          />
        </TouchableOpacity> */}





      </View>

      <ScrollView

      ref={reff}
        contentContainerStyle={{
          paddingBottom: 10,

        }}
        refreshControl={
          <RefreshControl
          colors={['#0049F1','#FF2567']}
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
        showsVerticalScrollIndicator={false}
        style={{ backgroundColor: '#F5F5F5' }}>

        <FlatList
          data={DATA.concat(catData.results)}
          renderItem={({ item, index }) => <Pressable onPress={() => {
            
            setSelected(item?.id);
            if(index == 0)
            {
              navigation.navigate("Search");
             
            }
            if(index !== 0)
            {
              navigation.navigate("Category", {
                id: item.id
              }) 
            }
          }}
          style={{ marginHorizontal: 5, left: 10, marginTop: 20, height: 30, minWidth: 96, paddingHorizontal: 5, borderRadius: 5, justifyContent: 'center', alignItems: 'center', backgroundColor: selected === item?.id ? '#004BEB' : '#FFFFFF' }}><Text style={{ color: selected === item?.id ? '#FFFFFF' : '#81838A' }} > {item?.name}</Text></Pressable>}
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            paddingRight: 20
          }}
        />

        <View style={{ marginTop: 15 }}>
          <Text style={{ fontSize: 18, marginLeft: 20, fontWeight: 'bold' ,color:"#323232"}}>How To Trade</Text>
        </View>

        <FlatList
          data={data2}
          renderItem={({ item }) =>
            <View style={{ margin: 5, left: 10, marginTop: 15, flexDirection: 'row', borderRadius: 10, justifyContent: 'space-between', backgroundColor: '#FFFFFF', alignItems: 'center', height:70, minWidth:175, maxWidth:230,paddingHorizontal:10}}>
              <View style={{ height: 30, width: 30, borderRadius: 15, backgroundColor: '#f2f2f2', justifyContent: 'center', alignItems: 'center',marginRight:10 }}>
                <Text style={{
                  color:"#323232"
                }}>{item.id}</Text>
              </View>
              <Text style={{ color: 'gray',width:"80%"}}>{item.text}</Text>
            </View>
          }
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{
            paddingRight: 20
          }}
        />


        {
           eventData?.results?.length > 0 
           ?
           eventData?.results?.map((item,index) => {
            let tempArray = item?.event_history?.volume_history;
            let sum = 0
            for (let i = 0; i < tempArray?.length; i++) {
              sum = tempArray?.[i]?.value + sum
            }


            return (
             
              <TouchableOpacity
                key={index}
                activeOpacity={1}
                onPress={() => {
                  navigation.navigate("EventDetails", { id: item.id });
                }}
                style={{
                  borderRadius: 10,
                  marginTop:25,
                  alignSelf: "center",
                  width: "90%",
                  backgroundColor: "#FFFFFF",
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 3.84,
                  elevation: 5,
                }}
              >


                <View
                  style={{ alignSelf: "center", marginTop: 20, flexDirection: "row", width: "90%" }}
                >
                  <View
                    style={{
                      height: 70,
                      width: 70,
                      backgroundColor: "#FFFFFF",
                      borderRadius: 35,
                      shadowColor: "#000000",
                      shadowOffset: {
                        width: 0,
                        height: 4,
                      },
                      shadowOpacity: 0.80,
                      shadowRadius: 10,
                      elevation: 25,
                      overflow: "hidden"
                    }}
                  >
                    <Image
                      // source={item?.photo?.file ? { uri: item?.photo?.file } : imageConstant.Profile_image}
                      source={item?.photo?.file ? { uri: item?.photo?.file } : imageConstant.defaultimage}
                      style={{
                        height: 70,
                        width: 70,
                        resizeMode: "contain",
                        borderRadius: 35,
                        borderWidth: 0.4,

                      }}
                    />
                  </View>


                  <Text
                    style={{
                      width: "75%",
                      fontWeight: "900",
                      fontSize: wp('4%'),
                      lineHeight: 20,
                      marginLeft: 15,
                      textAlign: "left",
                      color: "#1A1A1A",
                      fontFamily: fontConstant.regular,
                    }}
                  >
                    {item.title}
                  </Text>
                </View>

                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 15,
                    width: "65%",
                    height: 40,
                    marginTop: 25,
                    marginVertical: 20,
                    marginLeft: 85,
                    justifyContent: "space-between",
                  }}
                >
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => {
                      getEventDataByID(item.id, "yes", item.price_yes);
                      
                    }}
                    style={{
                      backgroundColor: "#0049F1",
                      width: 100,
                      height: 34.5,
                      justifyContent: "center",
                      alignItems: "center",
                      borderRadius: 40,
                      shadowColor: "black",
                      shadowOpacity: 0.6,
                      shadowRadius: 5,
                      shadowOffset: { height: 0.5, width: 0.2 },
                      elevation: Platform.OS === 'ios' ? null : 10,
                    }}
                  >
                    <Text
                      style={{
                        color: "#FFFFFF",
                        fontWeight: "700",
                        fontSize: 12,
                      }}
                    >
                      YES ₹ {item.price_yes}
                    </Text>
                  </TouchableOpacity>


                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => {
                      getEventDataByID(item.id, "no", item.price_no)
                      
                    }}
                    style={{
                      backgroundColor: "#FF2567",
                      width: 100,
                      height: 34.5,
                      marginLeft: 10,
                      justifyContent: "center",
                      alignItems: "center",
                      borderRadius: 40,
                      shadowColor: "black",
                      shadowOpacity: 0.6,
                      shadowRadius: 3,
                      shadowOffset: { height: 0, width: 0 },
                      elevation: Platform.OS === 'ios' ? null : 10,
                    }}
                  >
                    <Text
                      style={{
                        color: "#FFFFFF",
                        fontWeight: "700",
                        fontSize: 12,
                      }}
                    >
                      NO ₹ {item.price_no}
                    </Text>
                  </TouchableOpacity>
                </View>




                <View
                  style={{
                    width: "100%",
                    marginTop: 20,
                    height: 50,
                    backgroundColor: "#F8F8F8",
                    marginTop: 0,
                    borderBottomEndRadius: 10,
                    borderBottomStartRadius: 10,
                  }}
                >
                  <View
                    style={{
                      paddingTop: 10,
                      backgroundColor: "#F8F8F8",
                      width: "90%",
                      height: 50,
                      alignSelf: "center",
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <View>
                      <View
                        style={{
                          justifyContent: "center",
                          marginRight: 100,
                          flexDirection: "row",
                        }}
                      >
                        <Image
                          style={{ width: 18, height: 18 }}
                          source={imageConstant.timer}
                        />
                        <Text
                          style={{
                            color: "#414246",
                            fontWeight: "600",
                            fontSize: 12,
                            marginLeft: 8,
                          }}
                        >
                          {

                            // moment(new Date()).subtract(20,'minutes').toDate()
                            moment(item?.end_datetime).fromNow()
                          }
                        </Text>
                      </View>



                      <Text
                        style={{
                          color: "#414246",
                          fontWeight: "500",
                          fontSize: 9,
                          marginTop: 2,
                        }}
                      >
                        Ends in
                      </Text>
                    </View>

                    <View>
                      <View
                        style={{
                          marginLeft: 15,
                          justifyContent: "center",
                          flexDirection: "row",
                        }}
                      >
                        <Image
                          style={{ width: 18, height: 18 }}
                          source={imageConstant.volume}
                        />
                        <Text
                          style={{
                            color: "#414246",
                            fontWeight: "500",
                            fontSize: 14,
                            marginLeft: 8,
                          }}
                        >
                          ₹ {sum}
                        </Text>
                      </View>
                      <Text
                        style={{
                          color: "#414246",
                          fontWeight: "500",
                          fontSize: 9,
                          marginLeft: 12,
                          marginTop: 2,
                        }}
                      >
                        Volume
                      </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>

              
            );

          })
        :
            <View style={{
              flex:1,
              alignSelf:"center",
              marginTop:Height/8,
            }}>
              <Text style={{
                fontSize:18,
                fontFamily: fontConstant.bold,
                color: colorConstant.blue
              }}>NO EVENTS</Text>
            </View>
        }

      </ScrollView>

{/* ****Exit Modal***** */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>


            <View style={{
              width:"80%",
              marginVertical:25,
            }}>
            <Text style={styles.modalText}>Are you sure you want to exit app ?</Text>
           <View style={{
             flexDirection:"row",
             justifyContent:"space-between",
             width:"40%",
             marginTop:30,
             alignSelf:"flex-end"
             
           }}>
             <TouchableOpacity
             activeOpacity={0.8}
                 onPress={()=>setModalVisible(!modalVisible)}>
               <Text style={{
                 fontSize:16,
                 fontFamily:fontConstant.medium,
                 color:"#0049F1"
               }}>Cancel</Text>
             </TouchableOpacity>
             <TouchableOpacity
               activeOpacity={0.8}
             onPress={()=>BackHandler.exitApp()}>
               <Text style={{
                 fontSize:16,
                 fontFamily:fontConstant.medium,
                 color:"#0049F1"
               }}>Exit</Text>
             </TouchableOpacity>
           </View>
            </View>
          </View>
        </View>
      </Modal>

{/* *******Update modal******* */}

      <Modal
        animationType="slide"
        transparent={true}
        visible={updateFlag}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>



            <Text style={{
              fontSize: 16,
              fontFamily: fontConstant.bold,
              color: colorConstant.gray,
              marginTop: 20,
              width: "90%",
              textAlign: "center",
            }}>Please update your app with new features</Text>

            <TouchableOpacity
              activeOpacity={0.9}
              onPress={takePermissionForDownload}
              style={{
                backgroundColor: colorConstant.blue,
                borderRadius: 30,
                width: "60%",
                alignItems: "center",
                justifyContent: "center",
                height: 50,
                marginVertical: 20
              }}
            >
              <Text style={{
                fontSize: 14,
                fontFamily: fontConstant.medium,
                color: colorConstant.white
              }}>Click here for download</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>


{/* ********Bid Modal*********** */}

      <Modal
        animationType="slide"
        transparent={true}
        visible={detailsModal}
        onRequestClose={()=>{
          setDetailsModal(!detailsModal);
        }}
      >

        <View  
        style={styles.centeredView2}
        >
          <TouchableOpacity
          style={{
            padding:15,
            right: 10,
            top: Height-(Height*0.80)-75,
            position: "absolute"
          }}
            onPress={() => setDetailsModal(!detailsModal)}
            >

            <Image
              source={imageConstant.cancelImg}
              style={{
                width: 25,
                height: 25,
                
              }} />
          </TouchableOpacity>

          <View  
          style={styles.modalView2}
          >

            <View style={{
              flexDirection: "row",
              width: "100%",
              justifyContent: "space-between",
              height: 60

            }}>
              <TouchableOpacity
              activeOpacity={1}
              onPress={()=>setOptionChoose("yes")}
                style={{
                  width: "50%",
                  backgroundColor: optionChoose == "yes" ? colorConstant.bbg : colorConstant.lightGray,
                  justifyContent: "center",
                  alignItems: "center",
                  borderBottomWidth: 1,
                  borderBottomColor: "#D3D3D3",
                  borderRightWidth: 1,
                  borderRightColor: "#D3D3D3",
                  borderTopLeftRadius: 20
                }}>
                <Text style={{
                  fontSize: 18,
                  fontFamily: fontConstant.bold,
                  color: optionChoose == "yes" ? colorConstant.white : colorConstant.blackText ,



                }}>YES <Text style={{
                  fontSize: 14,
                  fontFamily: fontConstant.medium,
                  color:optionChoose == "yes" ? colorConstant.white : colorConstant.blackText 
                }}> ₹{eventDetails?.price_yes}</Text></Text>
              </TouchableOpacity>


              <TouchableOpacity
                 onPress={()=>setOptionChoose("no")}
              activeOpacity={1}
                style={{
                  width: "50%",
                  backgroundColor: optionChoose == "no" ? colorConstant.bbg : colorConstant.lightGray,
                  justifyContent: "center",
                  alignItems: "center",
                  borderBottomWidth: 1,
                  borderBottomColor: "#D3D3D3",
                  borderTopRightRadius: 20

                }}>
                <Text style={{
                  fontSize: 18,
                  fontFamily: fontConstant.bold,
                  color:optionChoose == "no" ? colorConstant.white : colorConstant.blackText 
                }}>NO <Text style={{
                  fontSize: 14,
                  fontFamily: fontConstant.medium,
                  color: optionChoose == "no" ? colorConstant.white : colorConstant.blackText 
                }}> ₹{eventDetails?.price_no} </Text></Text>
              </TouchableOpacity>



            </View>



            <ScrollView
            style={{
              flexGrow:1,
              paddingVertical:20
            }}
            >

              <View style={{
                width: "90%",
                alignSelf: "center",
              }}>
                <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.bold,
                  lineHeight: 25,
                  color: colorConstant.blackText,

                }}>{eventDetails?.title}
                </Text>
              </View>

              <View style={{
                flexDirection: "row",
                marginTop: 20,
                width: "90%",
                alignSelf: "center",
              }}>
                <Image
                  source={imageConstant.timer}

                  style={{
                    width: 20,
                    height: 20,
                    tintColor: colorConstant.blackText
                  }}
                />
                <Text style={{
                  fontSize: 14,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.blackText
                }}> {moment(eventDetails?.end_datetime).fromNow()}</Text>
              </View>



              <View style={{
                width: "90%",
                flexDirection: "row",
                justifyContent: "space-between",
                alignSelf: "center",
                marginVertical: 10,


              }}>

                <View style={{
                  flexDirection: "row",
                  alignItems: "center",

                }}>
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText,
                  }}>Set Price <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText
                  }}> ₹{price}</Text></Text>

                  {
                    sliderShow ?
                      <TouchableOpacity
                        onPress={() => setSliderShow(!sliderShow)}
                        style={{
                          padding: 5,
                          marginLeft: 5
                        }}>

                        <Image
                          resizeMode='contain'
                          source={imageConstant.poly}
                          style={{
                            width: 10,
                            height: 10
                          }} />
                      </TouchableOpacity>
                      :
                      <TouchableOpacity
                        onPress={() => setSliderShow(!sliderShow)}
                        style={{
                          padding: 5,
                          marginLeft: 5
                        }}>

                        <Image
                          resizeMode='contain'
                          source={imageConstant.poly}
                          style={{
                            width: 10,
                            height: 10,
                            transform: [{ rotate: "180deg" }]
                          }} />
                      </TouchableOpacity>
                  }


                </View>


                {
                  suggestedQuantity?.option?.toUpperCase() == optionChoose.toUpperCase()
                    ?
                    <Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.medium,
                      color: colorConstant.blackText,
                      alignSelf: "flex-end",
                    }}>Available Matches<Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.medium,
                      color: colorConstant.blackText
                    }}> {suggestedQuantity?.qty} </Text></Text>
                    :
                    <View>
                    </View>
                }



              </View>





              {/* //Price Slider */}

              {
                sliderShow && (
                  <View style={{
                    width: Width * 0.9,
                    alignSelf: "center",
                    alignItems: "center"
                  }}>
                    <MultiSlider
                      values={price}
                      min={1}
                      max={9}
                      sliderLength={Width * 0.86}
                      onValuesChange={(val) => setPrice(val)}
                      selectedStyle={{ backgroundColor: colorConstant.white }}
                      unselectedStyle={{ backgroundColor: colorConstant.white }}
                      //={imageConstant.thumbimage}
                      customMarker={() => {
                        return (
                          <Image
                            source={imageConstant.thumb}
                            resizeMode='contain'
                            style={{
                              height: 30,
                              width: 30
                            }}
                          />
                        )
                      }}
                      trackStyle={{ height: 4, borderRadius: 10 }}
                      markerStyle={{ height: 20, width: 20, backgroundColor: '#087CFF', }}
                    />
                  </View>
                )
              }


              <View style={{
                width: "90%",
                alignSelf: "center",
                alignItems: "flex-start",
                marginTop: sliderShow ? 0 : 20
              }}>
                <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.blackText

                }}>Quantity <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.blackText
                }}>{quantity}</Text></Text>
              </View>


              <View style={{
                width: Width * 0.9,
                alignSelf: "center",
                alignItems: "center"
              }}>
                <MultiSlider
                  values={quantity}
                  min={1}
                  max={25}
                  sliderLength={Width * 0.85}
                  onValuesChange={(val) => setQuantity(val)}
                  selectedStyle={{ backgroundColor: colorConstant.white }}
                  unselectedStyle={{ backgroundColor: colorConstant.white }}
                  //={imageConstant.thumbimage}
                  customMarker={() => {
                    return (
                      <Image
                        source={imageConstant.thumb}
                        resizeMode='contain'
                        style={{
                          height: 30,
                          width: 30
                        }}
                      />
                    )
                  }}
                  trackStyle={{ height: 4, borderRadius: 10 }}
                  markerStyle={{ height: 20, width: 20, backgroundColor: '#087CFF', }}
                />

              </View>




              <View style={{
                flexDirection: "row",
                // width: "100%",
                justifyContent: "space-between",
                marginTop: 20
              }}>
                <View style={{
                  width: "50%",
                  justifyContent: "center",
                  alignItems: "center",
                  borderRightWidth: 1,
                  borderRightColor: "#004BEB"
                }}>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: fontConstant.bold,
                    color: colorConstant.blackText
                  }}>₹ {quantity * price}</Text>
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText
                  }}>You invest</Text>
                </View>
                <View
                  style={{
                    width: "50%",
                    justifyContent: "center",
                    alignItems: "center"
                  }}>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: fontConstant.bold,
                    color: colorConstant.blackText
                  }}>₹ {eventDetails?.trade_value * quantity}</Text>
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText
                  }}>You Earn</Text>
                </View>
              </View>







              <SwipeButton
                title={`SWIPE FOR ${optionChoose == "yes" ? "YES" : "NO"}`}
                shouldResetAfterSuccess={true}
                height={45}
                width={Width * 0.9}
                railBackgroundColor={colorConstant.blue}
                titleColor='#FFFFFF'
                railFillBackgroundColor={colorConstant.blue}
                railBorderColor={colorConstant.blue}
                railFillBorderColor={colorConstant.blue}
                thumbIconBackgroundColor={colorConstant.green}
                thumbIconImageSource={imageConstant.rightarrow}
                thumbIconBorderColor={colorConstant.green}
                containerStyles={{ marginTop: 50, alignSelf: "center" }}
                onSwipeSuccess={() => checkBalance(eventDetails?.id)}


              />

              {
                price * quantity > walletBalance ?
                  <View style={{
                    width: "85%",
                    flexDirection: "row",
                    marginVertical: 30,
                    alignItems: "center",
                    alignSelf: "center",
                    justifyContent: "space-between"
                  }}>
                    <Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.bold,
                      color: colorConstant.blackText
                    }}> Wallet Balance <Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.bold,
                      color: colorConstant.blackText
                    }}> ₹ {walletBalance?.toFixed(2)}</Text></Text>


                    <TouchableOpacity
                      onPress={() => {
                        setDetailsModal(!detailsModal);
                        setTimeout(() => {
                          navigation.navigate("Wallet");
                        }, 500)
                      }}
                      activeOpacity={1}
                      style={{
                        paddingHorizontal: 25,
                        paddingVertical: 8,
                        borderRadius: 20,
                        backgroundColor: "#99BFF3"

                      }}>
                      <Text style={{
                        fontSize: 16,
                        fontFamily: fontConstant.medium,
                        color: "#004BEB"
                      }}>Recharge Now</Text>
                    </TouchableOpacity>
                  </View>

                  :
                  <></>
              }


            </ScrollView>



          </View>


        </View>


      </Modal>




    </SafeAreaView>
  )
}

export default HomePage

const styles = StyleSheet.create({
    appImageStyle: {
        marginVertical :10,
        marginLeft:15,
        height: 19.12,
        width: 79.43,
        
    },
    dropdownStyle:{
      width:"35%",
      height:30,
      borderColor:'#d3d3d3',
      top:10,
      borderRadius:5,
      paddingVertical:5,
      borderRadius:5,


    },
    bellImageStyle: {
        height: 36,
        width: 36,
    },
    optionImageStyle: {
        height: 36,
        width: 36,
    },

    centeredView: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
      backgroundColor:"rgba(1,1,1,0.5)"

    },
    centeredView2: {
      flex: 1,
      backgroundColor:"rgba(1,1,1,0.5)"

    }
    ,
    modalView: {
      margin:10,
      width:"90%",
      backgroundColor: "white",
      borderRadius: 20,
      alignItems: "center",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    },
    
    modalView2: {
      width:Width,
      height:Height*0.80,
      position:"absolute",
      bottom:0,
      backgroundColor:colorConstant.skyblue,
      borderTopLeftRadius: 20,
      borderTopRightRadius:20,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5
    }
    ,
  
    
})