import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    SafeAreaView,
    Button,
    TouchableOpacity,
    Alert,
    Platform,
    Dimensions,
    ScrollView,
    FlatList,
    Modal,
    StatusBar,
    Keyboard,
    Linking,
    BackHandler
  } from 'react-native';
import React, {Component, useRef, useState, useEffect} from 'react';
import CustomHeader2 from '../../custom/CustomHeader2';
import {fontConstant} from '../../utils/constant';
import { imageConstant } from '../../utils/constant';
import toastShow from '../../utils/Toast';
import { baseURL } from '../../utils/URL';
import { useSelector } from 'react-redux';


const Withdraw = props => {
    const {route, navigation} = props;
    const {amount}=route.params;
    const [upiId, setUpiId] = useState({});
    const {jwtToken} = useSelector((state)=>state.reducers);
    const [upiVerified,setUpiVerified]=useState(false);
    const [upiAdded,setUpiAdded]= useState(false);
    useEffect(() => {
      setUpiAdded(false);
      checkupiAdded();
    }, [])


    const checkupiAdded= async ()=>{
      try {
        console.log(amount);
        let tokenSent = "Token " + jwtToken;
        let response = await fetch(baseURL+'withdrawn/addupi',{
          method : "GET",
          headers: {
              'Authorization': tokenSent,
              'Accept': 'application/json',
              'Content-Type': 'application/json',
  
          },
         
          
        })
        if (response && response.status === 200) {
            const res= await response.json()
            console.log(res);
            if(res.data[0].vpa){
              setUpiId(res.data[0]?.vpa);
              setUpiAdded(true);
            }
            
        }else{
          console.log('status not ok',response.status)
          //const res= await response.json()
          //console.log(res)
        }
        
      }
      catch (error) {
        toastShow(error.message,"red")
      }
      
    }
    const addUPI = async () =>{
      console.log(upiId);
      //console.log(jwtToken)
        try {
          let tokenSent = "Token " + jwtToken;
          let reqData = {
            upi: upiId.trim(),
      
          }
          let response = await fetch(baseURL + 'withdrawn/addupi', {
            method: "POST",
            headers: {
              'Authorization': tokenSent,
              'Accept': 'application/json',
              'Content-Type': 'application/json',
      
            },
            body: JSON.stringify(reqData)
          })
          //console.log("ressss---->", response)
          
          if (response && response.status == 200) {
            let jsonData = await response.json();
            //console.log("jsonDatajsonData==>",jsonData)
            if (jsonData && jsonData.status == 200) {
              toastShow(jsonData.message, "#228B22");
              setUpiAdded(true);
            }
            else {
              toastShow(jsonData.message, "red");
            }
      
      
          }
          else {
            let jsonData = await response.json()
            toastShow(jsonData.message, "red")
          }
      
        }
        catch (error) {
            console.log(error)
        }
    }
    

    const UPIVerification = async () => {
        console.log(upiId);
        //console.log(jwtToken)
        try {
          let tokenSent = "Token " + jwtToken;
          let reqData = {
            upi: upiId.trim(),
      
          }
          let response = await fetch(baseURL + 'withdrawn/verification', {
            method: "POST",
            headers: {
              'Authorization': tokenSent,
              'Accept': 'application/json',
              'Content-Type': 'application/json',
      
            },
            body: JSON.stringify(reqData)
          })
          //console.log("ressss---->", response)
          
          if (response && response.status == 200) {
            let jsonData = await response.json();
            //console.log("jsonDatajsonData==>",jsonData)
            if (jsonData && jsonData.status == 200) {
              toastShow(jsonData.message, "#228B22");
              setUpiVerified(true);
              addUPI();
              //props.navigation.navigate("Wallet");
            }
            else {
              toastShow(jsonData.message, "red");
              //props.navigation.navigate("Wallet");
            }
      
      
          }
          else {
            let jsonData = await response.json()
            toastShow(jsonData.message, "red")
          }
      
        }
        catch (error) {
            console.log("rohit")
            console.log(error)
        }
    }
    const getWithdrawStatus = async (res)=>{
      try {
        let tokenSent = "Token " + jwtToken;
        
        let response = await fetch(baseURL+`withdrawn/status?transferId=${res.data[0].transferId}&referenceId=${res.data[0].referenceId}`,{
          method : "GET",
          headers: {
              'Authorization': tokenSent,
              'Accept': 'application/json',
              'Content-Type': 'application/json',
  
          },
         
          
        })
        if (response && response.status === 200) {
          console.log(response);
            const res= await response.json();
            // console.log("resssssssss--------->")
            // console.log(res);
            
            if(res.status==200){
              toastShow(res.data.transfer.status,"#228B22")
              props.navigation.navigate("WithdrawSuccess",{withdrawamount:amount});
            }else{
              props.navigation.navigate("WithdrawFailure");
            }
            

        }else{
          console.log('status not ok',response.status)
          
        }
        
      }
      catch (error) {
        console.log(error);
        toastShow(error.message,"red")
      }
    }
    const getID = async ()=>{
      try {
        let tokenSent = "Token " + jwtToken;
        // console.log("rohit");
        let response = await fetch(baseURL+'withdrawn/transfer',{
          method : "GET",
          headers: {
              'Authorization': tokenSent,
              'Accept': 'application/json',
              'Content-Type': 'application/json',
  
          },
         
          
        })
        if (response && response.status === 200) {
            const res= await response.json()
            getWithdrawStatus(res);

        }else{
          console.log('status not ok',response.status)
          
        }
        
      }
      catch (error) {
        toastShow(error.message,"red")
      }
    }
    const ProceedPayment = async ()=>{
      console.log(amount);
      try {
        let tokenSent = "Token " + jwtToken;
        
        let reqData = {
          amount: amount,
    
        }
        let response = await fetch(baseURL + 'withdrawn/transfer', {
          method: "POST",
          headers: {
            'Authorization': tokenSent,
            'Accept': 'application/json',
            'Content-Type': 'application/json',
    
          },
          body: JSON.stringify(reqData)
        })
        if (response && response.status == 200) {
          let jsonData = await response.json();
          console.log(jsonData);
          if (jsonData && jsonData.status == 200) {
            //toastShow(jsonData.data, "#228B22");
            getID();
            
          }
          else {
            toastShow(jsonData.data, "red");
            
          }
    
    
        }
        else {
          let jsonData = await response.json()
          toastShow(jsonData.data, "red")
          //navigation.navigate("WithdrawFailure");
        }
    
      }
      catch (error) {
          console.log("xxxxxxxxxxxxxx")
          console.log(error)
      }
        
      }
    function ButtontoDisplay(props){
        const flag=props.upiAdded;
        console.log(flag);
        if(flag===false){
            return <View style={{backgroundColor: '#0049F1', width:'90%',marginTop:15, padding: 20, borderRadius: 10, shadowColor: 'black', shadowOpacity: 0.25, shadowRadius: 5, shadowOffset: { height: 0.5, width: 0.2 }}}><TouchableOpacity
                onPress={UPIVerification}
                style={{ alignItems: 'center', flexDirection: 'row', alignSelf:'center' }}
                >
                <Text style={{ color:'#FFFFFF', fontSize: 20 }}>Verify UPI</Text>
            </TouchableOpacity>
            </View>
        }else{
            return <View style={{backgroundColor: '#0aab35',width: '90%',marginTop:15, padding: 20, borderRadius: 10, shadowColor: 'black', shadowOpacity: 0.25, shadowRadius: 5, shadowOffset: { height: 0.5, width: 0.2 }}}><TouchableOpacity
                onPress={ProceedPayment}
                style={{ alignItems: 'center', flexDirection: 'row', alignSelf:'center' }}
                >
                <Text style={{ color:'#FFFFFF', fontSize: 20 }}>Proceed to Payment</Text>
            </TouchableOpacity>
            </View>
        }
    }
    return(
        <>
            <SafeAreaView style={{flex: 0,backgroundColor:'#0049F1' }}>
            </SafeAreaView>

            <SafeAreaView style={{flex: 1,backgroundColor:'#0049F1' }}>
                <CustomHeader2
                headerText={'Withdraw'}
                img={imageConstant.back}
                navigation={props.navigation}
                />
            
            <View style={{ backgroundColor: '#F5F5F5', width: '100%', height: '100%', alignItems: 'center', paddingTop: 20 }}>
                <View style={{backgroundColor:'#faf0f1',width:'90%',height:100,borderRadius:5}}>
                <View style={{marginTop:10,marginLeft:5}}>
                <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                  <Text style={{fontWeight:'700',fontSize:20,color:'#0d0a0a'}}>Entered Amount:</Text>
                  <Text style={{marginRight:25,fontWeight:'700',fontSize:20,color:'#0d0a0a'}}>₹ {amount}</Text>
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:10}}>
                <Text style={{fontWeight:'600',color:'#0049F1'}}>Gateway Charges </Text>
                <Text style={{marginRight:25,fontWeight:'600',color:'#0049F1'}}>₹ 5</Text>
                </View>
                <View style={{flexDirection:'row',justifyContent:'space-between',marginTop:5}}>
                  <Text style={{fontWeight:'600',color:'#0049F1'}}>Your Withdraw Amount: </Text>
                  <Text style={{marginRight:25,fontWeight:'600',color:'#0049F1'}}>₹ {amount-5}</Text>
                </View>
                
                </View>
                
                </View>
                <View style={{marginTop:20}}>
                <TextInput
                    style={styles.textStyle}
                    placeholder="Enter Your UPI ID"
                    placeholderTextColor={'#777E91'}
                    onChangeText={(e) => setUpiId(e)}
                    value={upiId}
                />
                </View>
                <Text style={{color:'#0d0a0a',marginTop:10}}>{'\u20B9'} 5 will be charged on each withdrawal</Text>
                <View style={{  width: '90%', alignItems:'center'}}>
                <ButtontoDisplay upiAdded={upiAdded}/>
                </View>
                
                </View>
                
            </SafeAreaView>
        </>
    )
  }
  

  export default Withdraw

  const styles = StyleSheet.create({
    optionImageStyle: {
        // marginVertical :4,
        // position:'absolute',
        height: 36,
        width: 36,
        // left:50
    },
    textStyle: {
        fontWeight:'400',
        fontSize:16,
        fontFamily:fontConstant.regular,
        color:'#141416',
        width:'80%',
        height:Platform.OS === 'ios' ? 30 : null,
        left:Platform.OS === 'ios' ? null : -5,
        borderColor:'#0d0a0a'
        // backgroundColor:"pink",
        // marginBottom:10
      //  lineHeight:30
     },
  })