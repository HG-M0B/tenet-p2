import { Image, StyleSheet, Text, View ,Share,Linking,BackHandler} from 'react-native'
import React, { useEffect, useState,useCallback } from 'react'
import { fontConstant, imageConstant } from '../../utils/constant'
import { Height } from '../../dimension/dimension'
import { TouchableOpacity } from 'react-native'
import { useSelector } from 'react-redux'
import Clipboard from '@react-native-clipboard/clipboard';
import toastShow from '../../utils/Toast'
import { useFocusEffect } from '@react-navigation/native'
const WithdrawSuccess = (props) => {
    let {navigation,route}  = props;

    const [button,setButton] =  useState(1);
    const [price,setPrice] = useState(0);
    const [copy,setCopy] = useState("Copy")
  

    const {profileData} =  useSelector(state=>state.reducers);
    useEffect(()=>{
        let subs = navigation.addListener('focus',()=>{
            if(route?.params?.withdrawamount){
                setPrice(route?.params?.withdrawamount);
            }
           
        });
     
    },[]);

    useFocusEffect(
        React.useCallback(() => {
          const backAction = () => {
            props.navigation.navigate("Portfolio");
        
            return true;
          };
      
          const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
          );
      
          return () => backHandler.remove();
        }, [])
      );


    

      
    
      
    
  return (
    <View style={styles.mainView}>

      <View style={{
        width: "90%",
        alignSelf: "center",
        alignItems: "flex-end",
        marginTop: 20
      }}>
        <TouchableOpacity onPress={() => {
          props.navigation.navigate("Wallet");
        }}>
          <Image
            source={imageConstant.plus}
            style={{
              width: 25,
              height: 25,
              tintColor: "#FFFFFF",
              transform: [{ rotate: '130deg' }]
            }} />
        </TouchableOpacity>
      </View>

      <View style={{
        marginTop: Height * 0.20,
        width: "70%",
        alignSelf: "center"
      }}>
        <Text style={{
          fontSize: 18,
          color: "#FFFFFF",
          fontFamily: fontConstant.medium,
          textAlign: "center"
        }}>Sucess</Text>
        <Text style={{
          fontSize: 16,
          color: "#FFFFFF",
          marginTop: 6,
          fontFamily: fontConstant.regular,
          textAlign: "center"
        }}>{`Rs.${price} has been successfully added to your upi account`}</Text>
      </View>
      <Image
        source={imageConstant.right}
        style={{
          width: 50,
          height: 50,
          tintColor: "#FFFFFF",
          alignSelf: "center",
          marginTop: 20
        }}
      />
      
      
    </View>
  )
}

export default WithdrawSuccess

const styles = StyleSheet.create({
    mainView:{
        flex:1,
        backgroundColor:"#024BE8"
    },
    selectedButton:{
        flexDirection:"row",
        alignItems:"center",
        width:"48%",
        justifyContent:"space-evenly",
        backgroundColor:"#FFFFFF",
        borderRadius:5,
        borderWidth:1,
        borderColor:"#FFFFFF",
        paddingVertical:7
    },
    button:{
        flexDirection:"row",
        alignItems:"center",
        width:"48%",
        justifyContent:"space-evenly",
        borderWidth:1,
        borderColor:"#FFFFFF",
        borderRadius:5,
        paddingVertical:7
    },
    selectedText :{
        fontSize:16,
        color:"#024BE8"
    },
    text :{
        fontSize:16,
        color:"#FFFFFF"
    }
})