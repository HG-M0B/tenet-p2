import { Image, StyleSheet, Text, View ,Share,Linking,BackHandler} from 'react-native'
import React, { useEffect, useState,useCallback } from 'react'
import { fontConstant, imageConstant } from '../../utils/constant'
import { Height } from '../../dimension/dimension'
import { TouchableOpacity } from 'react-native'
import { useSelector } from 'react-redux'
import Clipboard from '@react-native-clipboard/clipboard';
import toastShow from '../../utils/Toast'
import { useFocusEffect } from '@react-navigation/native'
const WithdrawSuccess = (props) => {
    let {navigation,route}  = props;

    const [button,setButton] =  useState(1);
    const [price,setPrice] = useState(null);
    const [copy,setCopy] = useState("Copy")
   

    const {profileData} =  useSelector(state=>state.reducers);
    useEffect(()=>{
        let subs = navigation.addListener('focus',()=>{
            if(route?.params?.data){
                setPrice(route?.params?.data);
            }
           
        });
     
    },[]);

    useFocusEffect(
        React.useCallback(() => {
          const backAction = () => {
            props.navigation.navigate("Portfolio");
        
            return true;
          };
      
          const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
          );
      
          return () => backHandler.remove();
        }, [])
      );


    


      
    
      
    
  return (
    <View style={styles.mainView}>

      <View style={{
        width: "90%",
        alignSelf: "center",
        alignItems: "flex-end",
        marginTop: 20
      }}>
        <TouchableOpacity onPress={() => {
          props.navigation.navigate("Wallet");
        }}>
          <Image
            source={imageConstant.plus}
            style={{
              width: 25,
              height: 25,
              tintColor: "#FFFFFF",
              transform: [{ rotate: '130deg' }]
            }} />
        </TouchableOpacity>
      </View>

      <View style={{
        marginTop: Height * 0.20,
        width: "70%",
        alignSelf: "center"
      }}>
        <Text style={{
          fontSize: 18,
          color: "#FFFFFF",
          fontFamily: fontConstant.medium,
          textAlign: "center"
        }}>Failure</Text>
        <Text style={{
          fontSize: 16,
          color: "#FFFFFF",
          marginTop: 6,
          fontFamily: fontConstant.regular,
          textAlign: "center"
        }}>{`Payment Failed`}</Text>
      </View>
      <Image
        source={imageConstant.plus}
        style={{
          width: 60,
          height: 60,
          tintColor: "#FFFFFF",
          alignSelf: "center",
          marginTop: 20,
          transform: [{ rotate: '130deg' }]
        }}
      />

      {/* <View style={{
        width: "90%",
        alignSelf: "center",
        position: "absolute",
        bottom: 20
      }}>
        <Text style={{
          fontSize: 16,
          color: "#FFFFFF",
        }}>Earn ₹ 10 with each referral</Text>




        <View style={{
          marginTop: 15,
          borderRadius: 10,
          borderStyle: "dashed",
          borderColor: "#FFFFFF",
          borderWidth: 2.4,
          flexDirection: "row",
          height: 60,
          paddingHorizontal: "9%",
          alignItems: "center",
          justifyContent: "space-between",
        }}>
          <Text style={{
            fontSize: 16,
            color: "#FFFFFF",
            fontFamily: fontConstant.medium,
          }}>
            {profileData.user_referral_code}
          </Text>


          <TouchableOpacity
            onPress={copyToClipboard}
            activeOpacity={0.4}
            style={{
              backgroundColor: "#FFFFFF",
              opacity: 0.4,
              paddingHorizontal: 15,
              borderRadius: 5,
              paddingVertical: 5,
              position: "absolute",
              right: '9%'

            }}>
            <Text style={{
              fontSize: 16,
              color: "#FFFFFF",
            }}>{copy}</Text>
          </TouchableOpacity>

          <View>

          </View>

        </View>



        <View style={{
          flexDirection: "row",
          marginTop: 15,
          justifyContent: "space-between",
        }}>

          <TouchableOpacity

            onPress={() => {
              setButton(1);
              shareReferCodeOverWhatsapp();
            }}
            style={button == 1 ? styles.selectedButton : styles.button}>
            <Image
              source={imageConstant.whatsapp}
              style={{
                width: 25,
                height: 25,
                tintColor: button == 1 ? "#024BE8" : "#FFFFFF",
              }} />
            <Text style={button == 1 ? styles.selectedText : styles.text}>Whatsapp</Text>
          </TouchableOpacity>


          <TouchableOpacity
            onPress={() => {
              setButton(2);
              shareReferCode();
            }}
            style={button == 2 ? styles.selectedButton : styles.button}>
            <Image
              source={imageConstant.share}
              style={{
                width: 20,
                height: 20,
                tintColor: button != 1 ? "#024BE8" : "#FFFFFF",
              }} />
            <Text style={button != 1 ? styles.selectedText : styles.text}>More Option</Text>
          </TouchableOpacity>
        </View> */}

      {/* </View> */}
    </View>
  )
}

export default WithdrawSuccess

const styles = StyleSheet.create({
    mainView:{
        flex:1,
        backgroundColor:"#FF0000"
    },
    selectedButton:{
        flexDirection:"row",
        alignItems:"center",
        width:"48%",
        justifyContent:"space-evenly",
        backgroundColor:"#FFFFFF",
        borderRadius:5,
        borderWidth:1,
        borderColor:"#FFFFFF",
        paddingVertical:7
    },
    button:{
        flexDirection:"row",
        alignItems:"center",
        width:"48%",
        justifyContent:"space-evenly",
        borderWidth:1,
        borderColor:"#FFFFFF",
        borderRadius:5,
        paddingVertical:7
    },
    selectedText :{
        fontSize:16,
        color:"#024BE8"
    },
    text :{
        fontSize:16,
        color:"#FFFFFF"
    }
})