import { View, Text, TextInput, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import CustomHeader from '../../custom/CustomHeader'
import { imageConstant } from '../../utils/constant'
import CustomInput from '../../custom/CustomInput'
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'
import { Image } from 'react-native'
export default function AccountDetailsFilled(props) {
  const [money,setMoney] = useState("");
  const {navigation,route} = props;
  useEffect(()=>{
    if(route?.params?.money)
    {
      setMoney(props?.route?.params?.money)
    }
  },[])


  return (
    <View style={{flex:1}}>
      <CustomHeader
       img={imageConstant.back}
       navigation={props.navigation}
      headerText={"Account Details"}
      />
      <KeyboardAwareScrollView
      extraScrollHeight={50}
      enableAutomaticScroll={'Yes'}
      >
        <CustomInput
          placeholder={'Enter Amount'}
          value={money}
          editable={false}
        />
        <CustomInput
          placeholder={'Account Holder Name'}
          value={"Tenet"}
          editable={false}
        />
        <CustomInput
          placeholder={'Account Number'}
          value={"1234567890"}
          editable={false}
        />
        <CustomInput
          placeholder={'IFSC code'}
          value={"123454"}
          editable={false}
        />
        <CustomInput
          placeholder={'Bank Name'}
          value={"XYZ BANK"}
          editable={false}
        />
        <CustomInput
          placeholder={'Branch Name'}
          value={"XYZ branch name"}
          editable={false}
        />
      </KeyboardAwareScrollView>
      <TouchableOpacity
      onPress={()=>navigation.goBack()}
          style={{
            alignSelf: "center",
            position: "absolute",
            bottom: 10


          }}
        >
          <Image
            style={{
              width: 345,
              height: 65,
              // marginLeft:10,
              // marginTop:30,
              resizeMode: 'contain'
            }}
            source={imageConstant.withdrawButton}
          />
        </TouchableOpacity>
    </View>
  )
}