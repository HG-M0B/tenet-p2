import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList,
  Linking
} from 'react-native';
import React, {Component, useEffect, useRef, useState} from 'react';


import CustomHeader from '../../custom/CustomHeader';
import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import { baseURL } from '../../utils/URL';

const ContactUs = props => {
  const [contactData, setContactData]=useState(null)
  const {route, navigation} = props;
useEffect(()=>{
  getContactData()
},[])
  const getContactData=async()=>{
    const response=await fetch(baseURL+'static/contact-details/')
    if(response.status==200){
      const res= await response.json()
      setContactData(res)
    }
    else{
      console.log('status not ok'+response.status)
    }
  }
  console.log("contactData",contactData)
  return (
    <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
        <CustomHeader
          headerText={'Contact Us'}
          img={imageConstant.back}
          navigation={props.navigation}
        />
        <View style ={{borderBottomWidth:1, borderBottomColor:'#E6E8EC'}}></View>
        <View style={{backgroundColor: '#FFFFFF',flex: 1}}>
          <View style={{height:'40%',flexDirection:'column',alignItems:'center',padding:35}}>
          <View>
            <Image
            style={styles.imageStyle}
            source={imageConstant.logo}
          />
            </View>
            <View>
              <Text style={{fontFamily:fontConstant.bold,fontSize:20,fontWeight:'700',marginTop:10,color:"#323232"}}>Tenet Pvt. Ltd.</Text>
            </View>
            <View>
              <Text style={{textAlign:"center",fontFamily:fontConstant.regular,fontSize:15,fontWeight:'400',color:'#777E91',marginTop:10}}>{contactData?.company_address}</Text>
            </View>
            {/* <View>
            <Text style={{fontFamily:fontConstant.regular,fontSize:15,fontWeight:'400',color:'#777E91',marginTop:3}}>63, Noida, Uttar Pradesh 201301</Text>
            </View> */}
          </View>
          <View style={{height:'100%',width:'92%',alignSelf:'center'}}>


          <TouchableOpacity onPress={() => {
            Linking.openURL(`tel:${contactData?.company_number}`)
          }} style={{ height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 10 }}>
            <View>
              <Text style={styles.titleStyle}>Contact Number</Text>
              <Text style={styles.subTitleStyle}>{contactData?.company_number}</Text>
            </View>
            <View>
              <Image
                style={{ width: 16, height: 16, marginRight: 20, }}
                source={imageConstant.contactNumber}
              />
            </View>
          </TouchableOpacity>


            <View style={{borderBottomWidth:1, borderBottomColor:'#E6E8EC',}}>
            </View>

          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`mailto:${contactData?.company_email}`)
            }}
            style={{ height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 10, marginTop: 10 }}>
            <View>
              <Text style={styles.titleStyle}>Email ID</Text>
              <Text style={styles.subTitleStyle}>{contactData?.company_email}</Text>
            </View>
            <View>
              <Image
                style={{ width: 20, height: 16, marginRight: 20 }}
                source={imageConstant.emailId}
              />
            </View>
          </TouchableOpacity>


            <View style={{borderBottomWidth:1, borderBottomColor:'#E6E8EC',}}>
            </View>


          <TouchableOpacity
            onPress={() => {
              Linking.openURL(`https://${contactData?.company_website}`)
            }}
            style={{ height: '8%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 10, marginTop: 10 }}>
            <View>
              <Text style={styles.titleStyle}>Website</Text>
              <Text style={styles.subTitleStyle}>{contactData?.company_website}</Text>
            </View>
            <View>
              <Image
                style={{ width: 16, height: 16, marginRight: 20 }}
                source={imageConstant.website}
              />
            </View>
          </TouchableOpacity>



            <View style={{borderBottomWidth:1, borderBottomColor:'#E6E8EC',}}>
            </View>
          </View>
        </View>
    </SafeAreaView>
  )
}

export default ContactUs

const styles = StyleSheet.create({
  imageStyle: {
    height: 94,
    width: 94,
  },
  titleStyle:{
    fontWeight:'500',
    fontSize:16,
    marginBottom:5,
    color:"#323232"
  },
  subTitleStyle:{
    fontWeight:'400',
    fontSize:14,
    color:"#323232"
  },
})