import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList,
  Pressable,
} from "react-native";
import React, { Component, useEffect, useRef, useState } from "react";

import { imageConstant } from "../../utils/constant";
import { fontConstant } from "../../utils/constant";
import CustomHeader from "../../custom/CustomHeader";
import {useSelector} from 'react-redux'
import toastShow from '../../utils/Toast';
import { baseURL } from "../../utils/URL";
const windowWidth = Dimensions.get("window").width;

const layout = windowWidth - 20;

const WatchList = (props) => {
  const { route, navigation } = props;
  const {jwtToken} = useSelector((state)=> state.reducers);  

  let questions = [
    {
      id: 1,
      ques: "Will virat kohli be in the playing 11 for india in the 2nd ODI against England on 14th July?.",
      opt1: "YES 6.5",
      opt2: "NO 3.5",
      img: require("../../assets/images/virat.png"),
    },
    {
      id: 2,
      ques: "Will virat kohli be in the playing 11 for india in the 2nd ODI against England on 14th July?.",
      opt1: "YES 6.5",
      opt2: "NO 3.5",
      img: require("../../assets/images/virat.png"),
    },
    {
      id: 3,
      ques: "Will virat kohli be in the playing 11 for india in the 2nd ODI against England on 14th July?.",
      opt1: "YES 6.5",
      opt2: "NO 3.5",
      img: require("../../assets/images/virat.png"),
    },
  ];

  useEffect(()=>{
    getWatchList();
  },[])

  const getWatchList=async()=>{
    try{
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL+"watchlist/",{
        headers: {
          Authorization:tokenSent,
          'Content-Type': 'application/json'

        }
      });
      if (response && response.status == 200) {
        let jsonData = await response.json();
        console.log("JSON", jsonData);
      }
      else
      {
        let jsonData = await response.json();
        toastShow(jsonData?.errors,"red")
      }
    }
    catch(error){
      console.log("error at watch list",error);
    }
  }
  return (
    // <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
    <View style={{ backgroundColor: "#FFFFFF" }}>
      <CustomHeader
        headerText={"WatchList"}
        img={imageConstant.back}
        navigation={props.navigation}
      />
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingBottom: 110,
        }}
        style={{ backgroundColor: "#F5F5F5" }}
      >
        {questions.map((item, idx) => {
          // if (item.id==1){
          return (
            <>
              <View
                style={{
                  marginTop: 25,
                  borderRadius: 10,
                  marginHorizontal: 15,
                  backgroundColor: "#FFFFFF",
                  elevation: Platform.OS === "ios" ? null : 10,
                  shadowColor: "gray",
                  shadowOpacity: 1,
                  shadowRadius: 3,
                  shadowOffset: { height: 0.2, width: 0.2 },
                }}
              >
                <View
                  style={{
                    backgroundColor: "#FFFFFF",
                    flexDirection: "column",
                    borderRadius: 10,
                    paddingVertical: 18,
                    paddingHorizontal: 15,
                    width: "100%",
                  }}
                >
                  {/* <View style={{backgroundColor:'white',flexDirection:'column',borderRadius:10,paddingVertical:5,width:'100%'}}> */}
                  <View
                    style={{ backgroundColor: "#FFFFFF", flexDirection: "row" }}
                  >
                    <View
                      style={{
                        height: 70,
                        width: 70,
                        borderRadius: 35,
                        shadowColor: Platform.OS === 'ios' ? 'rgba(0, 0, 0, 0.25)' : 'black',
                        elevation:Platform.OS === 'ios' ? null : 20,
                        shadowOpacity: 1,
                        shadowRadius: 10,
                        shadowOffset: { height: 0.2, width: 0.2 },
                      }}
                    >
                      <Image
                        source={item.img}
                        style={{ height: 70, width: 70,
                        resizeMode: "contain",
                        borderRadius:35}}
                      />
                    </View>

                    <View
                      style={{
                        width: 200,
                        marginLeft: 15,
                        backgroundColor: "white",
                      }}
                    >
                      <Text
                        style={{
                          fontWeight: "900",
                          fontSize: 15,
                          lineHeight: 20,
                          textAlign: "left",
                          color: "#1A1A1A",
                        }}
                      >
                        {item.ques}
                      </Text>
                    </View>

                    {/* <View style={{width:22,marginLeft:22,backgroundColor:'white',height:25}}>
            <TouchableOpacity>
            <Image source={imageConstant.save} style={{height:22,width:22,}}/>
            </TouchableOpacity>
            </View> */}
                    <TouchableOpacity
                      style={{
                        height: 22,
                        width: 22,
                        backgroundColor: "white",
                      }}
                    >
                      <Image
                        source={imageConstant.save}
                        style={{ height: 22, width: 22, resizeMode: "contain" }}
                      />
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: 15,
                      width: "65%",
                      height: 40,
                      marginLeft: 85,
                      justifyContent: "space-between",
                    }}
                  >
                    <TouchableOpacity
                      style={{
                        backgroundColor: "#0049F1",
                        width: 100,
                        height: 34.5,
                        justifyContent: "center",
                        alignItems: "center",
                        borderRadius: 40,
                        shadowColor: "black",
                        shadowOpacity: 0.6,
                        shadowRadius: 5,
                        shadowOffset: { height: 0.5, width: 0.2 },
                        elevation:Platform.OS === 'ios' ? null : 10,
                      }}
                    >
                      <Text
                        style={{
                          color: "#FFFFFF",
                          fontWeight: "700",
                          fontSize: 12,
                        }}
                      >
                        {item.opt1}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        backgroundColor: "#FF2567",
                        width: 100,
                        height: 34.5,
                        marginLeft: 10,
                        justifyContent: "center",
                        alignItems: "center",
                        borderRadius: 40,
                        shadowColor: "black",
                        shadowOpacity: 0.6,
                        shadowRadius: 3,
                        shadowOffset: { height: 0, width: 0 },
                    elevation:Platform.OS === 'ios' ? null : 10,
                      }}
                    >
                      <Text
                        style={{
                          color: "#FFFFFF",
                          fontWeight: "700",
                          fontSize: 12,
                        }}
                      >
                        {item.opt2}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View
                  style={{
                    width: "100%",
                    // flexDirection:'row',
                    // alignItems : 'center',
                    height: 50,
                    backgroundColor: "#F8F8F8",
                    marginTop: 0,
                    borderBottomEndRadius: 10,
                    borderBottomStartRadius: 10,
                  }}
                >
                  <View
                    style={{
                      paddingTop: 10,
                      backgroundColor: "#F8F8F8",
                      width: "90%",
                      height: 50,
                      alignSelf: "center",
                      flexDirection: "row",
                      justifyContent: "space-between",
                    }}
                  >
                    <View>
                      <View
                        style={{
                          justifyContent: "center",
                          marginRight: 100,
                          flexDirection: "row",
                        }}
                      >
                        <Image
                          style={{ width: 18, height: 18 }}
                          source={imageConstant.timer}
                        />
                        <Text
                          style={{
                            color: "#414246",
                            fontWeight: "600",
                            fontSize: 12,
                            marginLeft: 8,
                          }}
                        >
                          {" "}
                          24 hours
                        </Text>
                      </View>
                      <Text
                        style={{
                          color: "#414246",
                          fontWeight: "500",
                          fontSize: 9,
                          marginTop: 2,
                        }}
                      >
                        Ends in
                      </Text>
                    </View>

                    <View>
                      <View
                        style={{
                          marginLeft: 15,
                          justifyContent: "center",
                          flexDirection: "row",
                        }}
                      >
                        <Image
                          style={{ width: 18, height: 18 }}
                          source={imageConstant.volume}
                        />
                        <Text
                          style={{
                            color: "#414246",
                            fontWeight: "500",
                            fontSize: 14,
                            marginLeft: 8,
                          }}
                        >
                          ₹ 38,654
                        </Text>
                      </View>
                      <Text
                        style={{
                          color: "#414246",
                          fontWeight: "500",
                          fontSize: 9,
                          marginLeft: 12,
                          marginTop: 2,
                        }}
                      >
                        Volume
                      </Text>
                    </View>
                  </View>
                </View>
              </View>

              {/* </View> */}
            </>
          );
          //   }
          //   else if (item.id % 2==0){

          //     return(
          //       <>
          //  <View style={{marginTop:15,backgroundColor:'#FFFFFF',marginHorizontal:15,borderRadius:10,shadowColor:'gray',shadowOpacity:1,shadowRadius:3,shadowOffset:{height:0.2,width:0.2}}}>
          //  <View style={{backgroundColor:'#FFFFFF',borderRadius:10,flexDirection:'row',justifyContent:'space-around',paddingVertical:10}}>
          //   <View style={{justifyContent:'center',alignItems:'center',shadowColor:'black',shadowOpacity:0.6,shadowRadius:10,shadowOffset:{height:0.5,width:0.2}}}>
          //   <Image source={item.img} style={{height:70,width:70,borderRadius:35}}/>
          //   </View>
          //   <View>
          //   <Text style={{fontWeight:'700',fontSize:16,textAlign:'justify',lineHeight:20}}>{item.ques.replace(/(\S+\s*){1,4}/g, "$&\n")}</Text>
          //   </View>
          //<Image source={require('../../assets/images/Vector(1).png')} />
          //
          //  </View>
          //  <View>
          //  <View style={{flexDirection:'row',justifyContent:'space-around'}}>
          //       <View>
          //       <TouchableOpacity style={{backgroundColor:'#0049F1',width:146,height:34,justifyContent:'center',alignItems:'center',borderRadius:40,marginTop:2,shadowColor:'black',shadowOpacity:0.6,shadowRadius:5,shadowOffset:{height:0.5,width:0.2}}}>
          //           <Text style={{color:'#FFFFFF',textTransform:'capitalize'}}>{item.opt1}</Text>
          //       </TouchableOpacity>
          //       <TouchableOpacity style={{backgroundColor:'#F1B500',width:146,height:34,justifyContent:'center',alignItems:'center',borderRadius:40,marginTop:15,shadowColor:'black',shadowOpacity:0.6,shadowRadius:5,shadowOffset:{height:0.5,width:0.2}}}>
          //           <Text style={{color:'#FFFFFF',textTransform:'capitalize'}}>{item.opt2}</Text>
          //       </TouchableOpacity>
          //       <TouchableOpacity style={{backgroundColor:'#AC00FF',width:146,height:34,justifyContent:'center',alignItems:'center',borderRadius:40,marginTop:15,shadowColor:'black',shadowOpacity:0.6,shadowRadius:5,shadowOffset:{height:0.5,width:0.2}}}>
          //           <Text style={{color:'#FFFFFF',textTransform:'capitalize'}}>{item.opt3}</Text>
          //       </TouchableOpacity>
          //       </View>
          //       <View>
          //       <TouchableOpacity style={{backgroundColor:'#FF2567',width:146,height:34,justifyContent:'center',alignItems:'center',borderRadius:40,marginTop:2,shadowColor:'black',shadowOpacity:0.6,shadowRadius:5,shadowOffset:{height:0.5,width:0.2}}}>
          //           <Text style={{color:'#FFFFFF',textTransform:'capitalize'}}>{item.opt4}</Text>
          //       </TouchableOpacity>
          //       <TouchableOpacity style={{backgroundColor:'#00D495',width:146,height:34,justifyContent:'center',alignItems:'center',borderRadius:40,marginTop:15,shadowColor:'black',shadowOpacity:0.6,shadowRadius:5,shadowOffset:{height:0.5,width:0.2}}}>
          //           <Text style={{color:'#FFFFFF',textTransform:'capitalize'}}>{item.opt5}</Text>
          //       </TouchableOpacity>
          //       </View>
          //   </View>
          //  </View>
          //  <View style={{flexDirection:'row',borderRadius:10,justifyContent:'space-between',backgroundColor:'#F8F8F8',marginTop:8,height:47}}>
          //  <View style={{flexDirection:'row',alignItems:'center',marginLeft:20,justifyContent:'center'}}>
          //  <View style={{justifyContent:'center',alignItems:'center'}}>
          //  <Image source={require('../../assets/images/timer.png')} style={{height:19,width:19,color:'#414246'}}/>
          //   <Text style={{color:'#414246',fontSize:10}}>Ends in</Text>
          //  </View>
          //  <Text style={{marginBottom:10,fontSize:13,color:'#414246'}}>24 hours</Text>
          //  </View>
          //  <View style={{flexDirection:'row',alignItems:'center'}}>
          //  <View style={{justifyContent:'center',alignItems:'center'}}>
          //   <Image source={require('../../assets/images/Group.png')} style={{height:15,width:15,color:'#414246'}}/>
          //   <Text style={{color:'#414246',fontSize:10}}>Volume</Text>
          //  </View>
          //  <Text style={{marginRight:20,marginBottom:10,fontSize:13,color:'#414246'}}><FontAwesome name='rupee'/> 38,654</Text>
          //  </View>
          //  </View>
          //  </View>
          //  </>
          //     )}
          //     else{
          //       return(
          //         <>
          //          <View style={{marginTop:15,borderRadius:10,marginHorizontal:15,backgroundColor:'#FFFFFF',shadowColor:'gray',shadowOpacity:1,shadowRadius:3,shadowOffset:{height:0.2,width:0.2}}}>
          //  <View style={{backgroundColor:'#FFFFFF',borderRadius:10,flexDirection:'row',justifyContent:'space-around',paddingVertical:10}}>
          //   <View style={{justifyContent:'center',alignItems:'center',shadowColor:'black',shadowOpacity:0.6,shadowRadius:10,shadowOffset:{height:0.5,width:0.2}}}>
          //   <Image source={item.img} style={{height:70,width:70,borderRadius:35}}/>
          //   </View>
          //   <View>
          //   <Text style={{fontWeight:'700',fontSize:16,textAlign:'justify',lineHeight:20}}>{item.ques.replace(/(\S+\s*){1,4}/g, "$&\n")}</Text>
          //   </View>
          // <Image source={require('../../assets/images/Vector(1).png')} />
          //
          //  </View>
          //  <View>
          //  <View style={{flexDirection:'row',justifyContent:'space-around'}}>
          //   <View>
          //       <View style={{flexDirection:'row',marginTop:10}}>
          //         <CheckBox boxType='square' style={{height:22,width:22}}/><Text style={{marginLeft:5,textTransform:'capitalize'}}>{item.opt1}</Text>
          //         </View>
          //         <View style={{flexDirection:'row',marginTop:20}}>
          //         <CheckBox boxType='square' style={{height:22,width:22}}/><Text style={{marginLeft:5,textTransform:'capitalize'}}>{item.opt2}</Text>
          //         </View>
          //         <View style={{flexDirection:'row',marginTop:20}}>
          //         <CheckBox boxType='square' style={{height:22,width:22}}/><Text style={{marginLeft:5,textTransform:'capitalize'}}>{item.opt3}</Text>
          //         </View>
          //       </View>
          //       <View>
          //         <View style={{flexDirection:'row',marginTop:10}}>
          //         <CheckBox boxType='square' style={{height:22,width:22}}/><Text style={{marginLeft:5,textTransform:'capitalize'}}>{item.opt4}</Text>
          //         </View>
          //         <View style={{flexDirection:'row',marginTop:20}}>
          //         <CheckBox boxType='square'  style={{height:22,width:22}}/><Text style={{marginLeft:5,textTransform:'capitalize'}}>{item.opt5}</Text>
          //         </View>
          //       </View>
          //   </View>
          //  </View>
          //  <View style={{flexDirection:'row',borderRadius:10,justifyContent:'space-between',backgroundColor:'#F8F8F8',marginTop:8,height:47}}>
          //  <View style={{flexDirection:'row',alignItems:'center',marginLeft:20}}>
          //  <View style={{justifyContent:'center',alignItems:'center'}}>
          //  <Image source={require('../../assets/images/timer.png')} style={{height:19,width:19,color:'#414246'}}/>
          //   <Text style={{color:'gray',fontSize:10}}>Ends in</Text>
          //  </View>
          //  <Text style={{marginBottom:10,fontSize:13,color:'#18191A'}}>24 hours</Text>
          //  </View>
          //  <View style={{flexDirection:'row',alignItems:'center'}}>
          //  <View style={{justifyContent:'center',alignItems:'center'}}>
          //  <Image source={require('../../assets/images/Group.png')} style={{height:15,width:15}}/>
          //   <Text style={{color:'gray',fontSize:10}}>Volume</Text>
          //  </View>
          //  <Text style={{marginRight:20,marginBottom:10,fontSize:13,color:'#18191A'}}><FontAwesome name='rupee'/> 38,654</Text>
          //  </View>
          //  </View>
          //  </View>
          //         </>
          //       )
          //     }
        })}
      </ScrollView>
    </View>
    // </SafeAreaView>
  );
};

export default WatchList;

const styles = StyleSheet.create({});
