import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList,
  Pressable
} from 'react-native';
import React, {Component, useRef, useState} from 'react';

import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import CustomHeader from '../../custom/CustomHeader';
import { widthPercentageToDP } from 'react-native-responsive-screen';


const windowWidth = Dimensions.get('window').width;

const layout=windowWidth-20

const Invoice = props => {
  const {route, navigation} = props;

  const notiArray = [
    {
        id:'1',
        name :'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        time:'22 Feb 2022'
    },
    {
        id:'12',
        name : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        time:'22 Feb 2022',
    },
    {
        id:'3',
        name : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        time:'22 Feb 2022'
    },
    {
      id:'4',
      name :'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      time:'22 Feb 2022'
  },
  {
      id:'5',
      name : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      time:'22 Feb 2022',
  },
  {
      id:'6',
      name : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
      time:'22 Feb 2022'
  },
]

  return (
    <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
    <View style={{flex: 1,backgroundColor:'#FFFFFF',padding:5 }}>
    <CustomHeader
        headerText={'Invoices'}
        img={imageConstant.back}
        // rightImg={imageConstant.calendar}
        navigation={props.navigation}
      />

      <View style={{backgroundColor:'#F5F5F5',width:'100%',height:'100%'}}> 
      <View style={{width:layout,alignSelf:'center',marginTop:20}}>
        <FlatList 
          // horizontal
          contentContainerStyle={{
            paddingBottom:110
          }}
          showsVerticalScrollIndicator = {false}
          data ={notiArray} 
          renderItem = {(d) => {
              return <View style={styles.mainView}>
                <View style={{flexDirection:'column',width:"85%",paddingLeft:5}}>
                <Text style={styles.textStyle}>{d.item.name}</Text>
                <Text style={styles.dateStyle}>Settled on {d.item.time} </Text>
                </View>
                <TouchableOpacity style = {{width:'15%',}}>
                <Image source={imageConstant.download} resizeMode='contain' 
                  style={[styles.Img]}
                />
                </TouchableOpacity>
              </View>
          }}
          />
      </View>
      </View>

    </View>
    </SafeAreaView>
  )
}

export default Invoice

  const styles = StyleSheet.create({
    textStyle: {
      marginTop:5,
      color: "#000000",
      fontSize:16,
      fontWeight:'500',
      fontFamily:fontConstant.regular,
      marginBottom:10,
      lineHeight:23,
    },
    dateStyle:{
      color: "#000000",
      fontSize:12,
      fontWeight:'400',
      fontFamily:fontConstant.regular
    },
    viewStyle : {
      flexDirection:'row'
    },
    Img:{
      marginLeft:15,
      width:20,
      height:20,
    },
    mainView:{
      flexDirection:'row',
      backgroundColor:"#FFFFFF",
      padding:10,
      alignItems:'center',
      borderWidth:0.8,
      borderRadius:8,
      borderColor:'#CDD1D9',
      marginBottom:10}
})