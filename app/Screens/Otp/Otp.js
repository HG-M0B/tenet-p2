import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
} from 'react-native';
import React, {Component, useRef, useState, useEffect} from 'react';
import AsyncStorage from "@react-native-async-storage/async-storage";
import auth from '@react-native-firebase/auth';

import CustomHeader from '../../custom/CustomHeader';

import {imageConstant} from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import OTPInputView from '@twotalltotems/react-native-otp-input';


import Loader from '../../utils/loader'
import { baseURL } from '../../utils/URL';
import { useDispatch } from 'react-redux';
import { actions } from '../../redux/reducers';
import {
  getHash,
  startOtpListener,
  useOtpVerify,
  requestHint
} from 'react-native-otp-verify';
import toastShow from '../../utils/Toast'
import { heightPercentageToDP } from 'react-native-responsive-screen';
import { Height } from '../../dimension/dimension';

const windowWidth = Dimensions.get('window').width;

// const width=Platform.width
const layout = windowWidth - 40;
 
const Otp = props => {

  const {route, navigation} = props;
  const {number} = props.route.params;
  const [status,setStatus] = useState(true);
  const [renumber,setReNumber] = useState("");
  const dispatch =  useDispatch();
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const [loader, setLoader] = useState(false);
  const [counter,setCounter] = useState(30);
  const [enable,setEnable] = useState(false);

 // If null, no SMS has been sent
 const [confirm, setConfirm] = useState(null);
 const [code, setCode] = useState("");
 const [newOtp, setNewOtp] = useState("");
 const [keyboardFlag,setKeyboardFlag] = useState(false)
 let confirmVerification = "";

 const { hash, otp, timeoutError, stopListener, startListener } = useOtpVerify();

function onAuthStateChanged(user) {
setUser(user);
  if (initializing) setInitializing(false);
}

useEffect(() => {
  const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
  let subs = navigation.addListener('focus',()=>{
    setReNumber(number);
    verifyPhoneNumber(number);
    setKeyboardFlag(true)
  })

  return ()=>{
    subs;
    subscriber;
  }; // unsubscribe on unmount
}, []);


  useEffect(() => {
    if (confirm !== null) {
      setEnable(false);
    }
  }, [confirm]);


const resendOtp = () => {
  setConfirm(null);
  toastShow("OTP sent again","#228B22");
  setCounter(30);
  setCode("");
  verifyPhoneNumber(renumber);
}


let tempID ; 
async function verifyPhoneNumber(number) {
  let sentNumber = "+91"+number
const confirmation = await auth().signInWithPhoneNumber(sentNumber);
tempID = confirmation?.verificationId;
 setConfirm(confirmation?.verificationId)

}

React.useEffect(() => {
  if(counter == 0 || counter == 30)
  {
    // setLoader(false);
  }
  const timer = counter > 0 && setInterval(() => setCounter(counter - 1), 1000)
  return () => clearInterval(timer);
}, [counter]);



const onVerfiyPress=()=>{
  if(code == "")
  {
    toastShow("Please enter OTP","red");
  }
  else
  {

    confirmCode();
  }
}


async function confirmCode() {
  try {

    const credential = auth.PhoneAuthProvider.credential(confirm, code);
    let userData = await auth().signInWithCredential(credential);
    Verify(userData.user.uid)
  } catch (error) {
    if (error.code == 'auth/invalid-verification-code') {

      toastShow('Please enter valid OTP','red');
    } else {

      console.log('Account linking error');
    }
  }
}

const Verify = async(data) => {
 try{
  let reqData = {
    phone_number: "+91"+number.toString(),
    firebase_token: data
  }
  let resposne =  await fetch(baseURL + 'auth/login/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'accept':'application/json'
    },
    body:JSON.stringify(reqData)
  }
  )
 
  console.log("login APi call response--->",resposne)
  if(resposne.status == 401){
    let res = await resposne.json();
    //console.log("res?.errors",res?.errors)
    toastShow(res?.errors,"red")
    //console.log('====================401========>',res)
    navigation.goBack();
  }
  else if(resposne && resposne?.status == 404){
    let res = await resposne.json()
    //console.log('====================404========>',res)
    AsyncStorage.setItem('user',JSON.stringify(res))
    navigation.navigate('AdditionalDetails',{number : number, token : data});
  }
  else if( resposne && resposne?.status == 200){
    let res = await resposne.json()
    //console.log('==============200==============>',res)
    AsyncStorage.setItem('user',JSON.stringify(res));
    dispatch(actions.setJwtToken(res?.token))
    dispatch(actions.setIntroStatus("main"))
  }
  else{
  }
 }
 catch(error){
   //console.log("error",error)
 }
}



return (
  <SafeAreaView style={{flex: 1, backgroundColor: '#FFFFFF'}}>
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <CustomHeader
        headerText={'Verify'}
        img={imageConstant.back}
        navigation={props.navigation}
      />
      { loader && <Loader data={loader}/>}
      <View>
        <Text style={styles.textStyle}>
          We have sent an OTP on your number
        </Text>
      </View>
      <View>
        <Text style={styles.numStyle}>{`+91${number}`}</Text>
      </View>
        <OTPInputView
          style={{
            paddingVertical:2,
            width: '80%', 
            height:Height*0.090,
            alignSelf: 'center',
          }}
          code={code}
          pinCount={6}
          onCodeChanged={code=>{setCode(code)}}
          autoFocusOnLoad = {keyboardFlag}
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
      

        />
      <View style={{flexDirection : 'row',marginVertical:50,alignSelf:'center'}}>
      <Text style={styles.num2Style}>Don’t receive any code? </Text>


     
  
        <TouchableOpacity activeOpacity={0.9} onPress={counter == 0 ? resendOtp : null}>

          {
            counter != 0 && counter > 9 ? <Text style={styles.timerStyle}>{`00:${counter}`} </Text>
              : counter ?
                <Text style={styles.timerStyle}>{`00:0${counter}`} </Text>
                :
                <Text style={styles.timerStyle}>Resend OTP </Text>
          }

        </TouchableOpacity> 
     
     
   
      

      </View>
      <TouchableOpacity
      activeOpacity={0.9}
          style={{
            backgroundColor:  confirm == undefined ? "gray" :'#004BEB' ,
            width: '92%',
            height: 50,
            alignSelf: 'center',
            padding: 9,
            // marginTop:30,
            borderRadius: 25,
            borderColor:'rgba(255, 255, 255, 0.4)',
            shadowOpacity:1,
            shadowRadius:3,
            shadowOffset:{height:0.2,width:0.2},
            position:'absolute',
            bottom:20
          }}          
         onPress={ confirm == undefined ? null : onVerfiyPress}> 
          <Text style={styles.buttonStyle}>Verify OTP</Text>
        </TouchableOpacity>
    </View>
  </SafeAreaView>
);
};

export default Otp;

const styles = StyleSheet.create({
  buttonStyle: {
    marginTop: 5,
    alignSelf: 'center',
    color: '#FFFFFF',
    fontFamily: fontConstant.bold,
    fontSize: 16,
  },
  textStyle: {
    fontSize: 16,
    color: '#353945',
    fontFamily: fontConstant.regular,
    textAlign: 'center',
    marginTop: 50,
    alignSelf: 'center',
  },
  numStyle: {
    fontSize: 16,
    color: '#353945',
    fontFamily: fontConstant.bold,
    textAlign: 'center',
    marginVertical: 11,
    // marginTop: 50,
    alignSelf: 'center',
    // left: '1%',
  },
  num2Style: {
    fontSize: 16,
    color: '#353945',
    fontFamily: fontConstant.regular,
    fontWeight:'400',


  },
  timerStyle: {
    fontSize: 16,
    color: '#004BEB',
    fontFamily: fontConstant.bold,
    // textAlign: 'center',
    // marginVertical: 11,
    fontWeight:'700',
    // alignSelf: 'center',
    // display:'flex',
    // alignItems:'center',
    // top:-1
  },
  borderStyleBase: {
    width: 30,
    height: 45,
  },

  borderStyleBase: {
    width: 30,
    height: 45
  },

  borderStyleHighLighted: {
    borderColor: "#03DAC6"
  },

  underlineStyleBase: {
    width: 30,
    // height: 45,
    height : Platform.OS === 'ios' ? 45 : 70,
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor:'#E6E8EC',
    fontSize:28,
    fontWeight:'700',
    color:'#B1B5C3',
    // backgroundColor:'pink'
  },

  underlineStyleHighLighted: {
    borderColor: '#004BEB',
    color:'#004BEB'
  },
});