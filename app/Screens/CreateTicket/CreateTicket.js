import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList,
  Keyboard,
  TouchableWithoutFeedback
} from 'react-native';
import React, {useState} from 'react';


import CustomHeader from '../../custom/CustomHeader';
import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';


const windowWidth = Dimensions.get('window').width;

const width=Platform.width
const layout=windowWidth-20


const CreateTicket = props => {
  const {route, navigation} = props;

  return (
    
    <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
      <View style={{backgroundColor: '#FFFFFF',flex: 1}}>
      <CustomHeader
        headerText={'Create Ticket'}
        img={imageConstant.back}
        navigation={props.navigation}
      />
      <View style ={{borderBottomWidth:1, borderBottomColor:'#E6E8EC'}}></View>
      <TouchableOpacity  activeOpacity={1}  onPress={()=>Keyboard.dismiss()} style={{width:"95%",height:"100%",flexDirection:'column',alignSelf:'center',}}>

        <View style={{marginTop:40}}>


          <View style={{borderBottomWidth:1, borderBottomColor:'#E6E8EC'}}>
          <Text style={styles.textStyle}>Subject</Text>
          <TextInput
            style={styles.placeholderStyle}
            numberOfLines={1}
            multiline={true}
            placeholder="Need to refund the booking amount"
            />
          </View>

          <View style={{borderBottomWidth:1, borderBottomColor:'#E6E8EC',marginTop:20}}>
          <Text style={styles.textStyle}>Description</Text>
          <TextInput
            style={styles.placeholderStyle}
            numberOfLines={1}
            multiline={true}
            // onFocus={()=>alert("focus")}
            // onBlur={()=>alert("blur")}
            // onSubmitEditing={()=>alert("sub")}
            placeholder="Wrong information provide from your end"
            />
          </View>

          
        </View>

        <TouchableOpacity
                style={{
                  // backgroundColor: 'red',
                  // width: '92%',
                  // height: 68,
                  alignSelf: 'center',
                  position: 'absolute',
                  bottom : Platform.OS === 'ios' ? 50 : 80,
                }}
                onPress={() => {
                navigation.navigate("HelpSupport")
                }}
                >
            <Image
            style={styles.Img}
            source={imageConstant.submitButton}
          />
          </TouchableOpacity>
              </TouchableOpacity>
      </View> 
    </SafeAreaView>
  )
}

export default CreateTicket

const styles = StyleSheet.create({
  textStyle: {
    // marginTop:10,
    color: "#141416",
    fontSize:16,
    fontWeight:'500',
    fontFamily:fontConstant.regular,
    marginBottom:5,
    // lineHeight:30
  },
  placeholderStyle: {
    // marginBottom:5,
    color: "#141416",
    fontSize:16,
    fontWeight:'400',
    fontFamily:fontConstant.regular,
    marginBottom:10,
    width:'98%',
    left:Platform.OS === 'ios' ? null : -5,
  },
  ticketStyle: {
    marginTop:5,
    color: "#000000",
    fontSize:13,
    fontWeight:'400',
    fontFamily:fontConstant.regular,
  },
  dateStyle:{
    color: "#353945",
    fontSize:12,
    fontWeight:'400',
    fontFamily:fontConstant.regular
  },
  viewStyle : {
    flexDirection:'row'
  },
  Img:{
    width:345,
    height:65,
    resizeMode:'contain'
  }
})