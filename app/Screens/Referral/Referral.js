import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList,
  Share,
  Linking,
  BackHandler
} from 'react-native';
import React, {Component, useEffect, useRef, useState} from 'react';

import CustomHeader from '../../custom/CustomHeader';
import { imageConstant, ProgessFinish } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Width,Height } from '../../dimension/dimension';
import {useSelector} from 'react-redux'
import { useDispatch } from 'react-redux';
import * as Progress from 'react-native-progress';
import { baseURL } from '../../utils/URL';
import Clipboard from '@react-native-clipboard/clipboard';
import toastShow from '../../utils/Toast'; 
import Loader from '../../utils/loader';
import { useFocusEffect } from '@react-navigation/native';

const windowWidth = Dimensions.get('window').width;
console.log('windowWidth===========>',windowWidth)
// const width=Platform.width
const layout=windowWidth-20

const Referral = (props) => {
  const [name,setName] = useState("")
const [option, setOption]=useState("")
const [referCode,setReferCode] = useState("");
const [referLink,setReferLink] = useState("https://tenetapp.in/");
const [referCopy,setReferCopy]=useState("");
const [refercodeFlag,setReferCodeFlag]=useState(false);
const [referCount,setReferCount]=useState(0);
const [dropDown,setDropDown]=useState(true);
const [loader,setLoader] = useState(false);
const {route, navigation} = props;
let dispatch =  useDispatch();

const {jwtToken}  = useSelector((state)=> state.reducers)

useEffect(()=>{
  let subs = navigation.addListener('focus',()=>{
    getProfileData();
    getCountOfRefer();
  })

},[])
useFocusEffect(
  React.useCallback(() => {
    const backAction = () => {
     props.navigation.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, [])
);


   // *********Api's call *****I


const getCountOfRefer= async ()=>{
  try
  {
  let tokenSent = "Token " + jwtToken;
  let response = await fetch(baseURL +`portfolio/`,{
    method:'GET',
    headers:{
      'Accept': 'application/json',
      'Authorization': tokenSent,
    }
  })
  if(response && response.status  == 200)
  {
    let jsonData = await response.json();
    let tempCount = jsonData?.data?.live?.length + jsonData?.data?.complete?.length;
    if(tempCount > 3)
    {
      setPage(1);
    }
    else
    {
      setPage(2);
    }
    setReferCount(tempCount);
    setLoader(false);

  }
  else
  {
    let jsonData = await response.json();
    toastShow(jsonData?.errors, "red");
    setLoader(false);
  }
  

  }
  catch(error){
    setLoader(false);
  }
}
const getProfileData = async () => {
  try {
    setLoader(true);
    let tokenSent = "Token " + jwtToken;
    const response = await fetch(baseURL + 'profile/', {
      headers: {
        'Authorization': tokenSent,
        'Accept': 'application/json',

      }
    })
    if (response.status == 200) {
      const res = await response.json();
      setReferCode(res?.user_referral_code);
      setName(res?.user_name);
    }
    else {
      console.log('status not ok==>', response.status);
      setLoader(false);
    }
  }
  catch (error) {
    console.log("error", error)
  }
} 

  // *********Aditional function's*******
  const copyLink = () => {
      Clipboard.setString(referLink);
      fetchCopiedText("link");
    };
    const copyCode = () => {
      Clipboard.setString(referCode);
      fetchCopiedText("code");
    };
  
    const fetchCopiedText = async (item) => {
      const text = await Clipboard.getString();
      setOption(item);
      setReferCopy(text);
      setReferCodeFlag(true);
      setTimeout(()=>{
        setReferCodeFlag(false);
      },1000)
  
    };
  
    // const shareReferCode = async ()=>{
    //   let messageTemp = `🚀 ${name} has invited you to join ⏳ Tenet.\n\n
    //   Earn money from your opinion on Real life events like Cricket,Politics & Stocks.Use my referral code to sign-up & get a referral bonus of upto Rs. 15/-*Referral Code*: ${referCode}.
    //   \n\n 📍Download Tenet : https://tenetapp.in/`
    //   const shareOptions = {
    //     message: messageTemp,
    //       url:imageConstant.logo2
    //   }
    //   try {
    //     const shareResponse = await Share.share(shareOptions);
    //     console.log("result",shareResponse)
    //   } catch (error) {
    //     alert(error.message);
    //   }
  
    // }

    const shareReferLink = async ()=>{
      let messageTemp = `🚀 ${name} has invited you to join ⏳ Tenet.\n\n
      Earn money from your opinion on Real life events like Cricket,Politics & Stocks.Use my referral code to sign-up & get a referral bonus of upto Rs. 15/-*Referral Code*: ${referCode}.
      \n\nDownload Tenet : https://tenetapp.in/`
      const shareOptions = {
        message: messageTemp,
          url:imageConstant.logo2
      }
      try {
        const shareResponse = await Share.share(shareOptions);
        console.log("result",shareResponse)
      } catch (error) {
        alert(error.message);
      }
  
    }
    const toBold = text =>{
      const charSet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '?', '.', ',', '"', "'"];
      const targetCharSet = ['𝐚', '𝐛', '𝐜', '𝐝', '𝐞', '𝐟', '𝐠', '𝐡', '𝐢', '𝐣', '𝐤', '𝐥', '𝐦', '𝐧', '𝐨', '𝐩', '𝐪', '𝐫', '𝐬', '𝐭', '𝐮', '𝐯', '𝐰', '𝐱', '𝐲', '𝐳', '𝐀', '𝐁', '𝐂', '𝐃', '𝐄', '𝐅', '𝐆', '𝐇', '𝐈', '𝐉', '𝐊', '𝐋', '𝐌', '𝐍', '𝐎', '𝐏', '𝐐', '𝐑', '𝐒', '𝐓', '𝐔', '𝐕', '𝐖', '𝐗', '𝐘', '𝐙', '𝟎', '𝟏', '𝟐', '𝟑', '𝟒', '𝟓', '𝟔', '𝟕', '𝟖', '𝟗', '❗', '❓', '.', ',', '"', "'"];
      const textArray = text.split('');
      let boldText = '';
      textArray.forEach((letter) => {
        const index = charSet.findIndex((_letter) => _letter === letter);
        if (index !== -1) {
          boldText = boldText + targetCharSet[index];
        } else {
          boldText = boldText + letter;
        }
      });
      return boldText;
    }
    const shareReferCodeOverWhatsapp = async () => {
      let messageTemp = `🚀 ${name} has invited you to join ⏳ Tenet.\n\nEarn money from your opinion on Real life events like Cricket,Politics and Stocks.\nUse my referral code to sign-up and get a referral bonus of upto Rs. 15/-\n\n${toBold('Referral Code')}: ${toBold(referCode)}.
      \n\nDownload Tenet : https://tenetapp.in/`
      
      let url = `whatsapp://send?text=${messageTemp}`
      
      try {
       await Linking.openURL(url)
          .then(() => console.log("WhatsApp Opened successfully"))
          .catch(() => {
            // Linking.openURL("https://tenetapp.in/")
          });
      }
      catch (error) {
        console.log("error", error)
      }
  
    }
  
    // const shareReferLinkOverWhatsapp = async () => {
    //   let messageTemp = `🚀 ${name} has invited you to join ⏳ Tenet.\n\n
    //   Earn money from your opinion on Real life events like Cricket,Politics and Stocks.Use my referral code to sign-up and get a referral bonus of upto Rs. 15/-*Referral Code*: ${referCode}.
    //   \n\nDownload Tenet : https://tenetapp.in/`
      
    //   let url = `whatsapp://send?text=${messageTemp}`
      
    //   try {
    //    await Linking.openURL(url)
    //       .then(() => console.log("WhatsApp Opened successfully"))
    //       .catch(() => {
    //         // Linking.openURL("https://tenetapp.in/")
    //       });
    //   }
    //   catch (error) {
    //     console.log("error", error)
    //   }
  
    // }
return (
  <View style={styles.main}>

        <ScrollView
        showsVerticalScrollIndicator={false}
            contentContainerStyle={{
                paddingBottom: 30
            }}>
            <Text style={styles.text}>GIVE Rs.15, GET Rs.10</Text>
            <Image
                source={imageConstant.referral}
                resizeMode='contain'
                style={styles.img} />

            <View style={[{ ...styles.row }, { marginTop: 30 }]}>
                <Image
                    source={imageConstant.arrow}
                    resizeMode='contain'
                    style={styles.img1} />
                <Text style={styles.text1}>Invite your friends to Tenet App</Text>
            </View>

            <View style={styles.row}>
                <Image
                    source={imageConstant.wallet2}
                    resizeMode='contain'
                    style={styles.img1} />
                <Text style={styles.text1}>Your friends get Rs.15 in their wallet</Text>
            </View>


            <View style={styles.row}>
                <Image
                    source={imageConstant.join}
                    resizeMode='contain'
                    style={styles.img1} />
                <Text style={styles.text1}>You get Rs.10 for every friend that joins</Text>
            </View>

            <Text style={styles.text}>Invite now using:</Text>





            <View style={[{ ...styles.row }, { marginTop: 20 }]}>
                <TouchableOpacity
                onPress={shareReferCodeOverWhatsapp}>
                <Image
                    source={imageConstant.whatsapp}
                    resizeMode='contain'
                    style={styles.img3} />
                </TouchableOpacity>

                <TouchableOpacity
                onPress={shareReferLink}>
                <Image
                    source={imageConstant.share3}
                    resizeMode='contain'
                    style={styles.img2} />
                </TouchableOpacity>
             
                

            </View>





            {/* <View style={{
                marginTop:25
            }}>
                <Text style={styles.text3}>Or copy your personal link</Text>


                <View style={styles.input}>
                    <Text style={styles.text2}>{referLink}</Text>
                    <TouchableOpacity onPress={copyLink} activeOpacity={0.9} style={styles.touchCopy}>
                        <Image
                            source={imageConstant.copy}
                            style={styles.img4}
                        />
                    </TouchableOpacity>
                </View>
            </View>

         {
           refercodeFlag && option == "link" &&
           <Text style={{
             fontSize: 12,
             color: "red",
             width: "90%",
             marginTop: 5,
             alignSelf: "center",
           }}>Referral Link Copied !!</Text>
         } */}

            <View style={{
                marginTop: 15
            }}>
                <Text style={styles.text3}>Unique Referral Code</Text>


                <View style={styles.input}>
                    <Text style={styles.text2}>{referCode}</Text>
                    <TouchableOpacity  onPress={copyCode} style={styles.touchCopy} activeOpacity={0.9}>
                        <Image
                            source={imageConstant.copy}
                            style={styles.img4}
                        />
                    </TouchableOpacity>
                </View>
        
            </View>
      {
        refercodeFlag && option == "code" &&
        <Text style={{
          fontSize: 12,
          color: "red",
          width: "90%",
          marginTop: 5,
          alignSelf: "center",
        }}>Referral Code Copied !!</Text>
      }
        </ScrollView>
  </View>
)
}

export default Referral

const styles = StyleSheet.create({
  main:{
      // flex:1,
      backgroundColor:"#F2F4F7"
  },
  text:{
      fontSize:20,
      fontFamily:fontConstant.bold,
      color:"#001246",
      width:"90%",
      alignSelf:"center",
      marginTop:20
  },
  img:{
      width:"90%",
      marginTop:20,
      height:Height*0.35,
      alignSelf:"center"
  },
  row:{
      flexDirection:"row",
      width:"90%",
      alignSelf:"center",
      alignItems:"center",
      marginVertical:10
  },
  img1:{
      width:45,
      height:45,
  },
  text1:{
      fontSize:18,
      fontFamily:fontConstant.medium,
      color:"#001246",
      marginLeft:15,
      width:"80%",
      // textAlign:"justify",
  },
  img2:{
      width:30,
      height:30,
      marginLeft:20
  },
  img3:{
      width: 45,
      height: 45,
      shadowColor: "#000",
      shadowOffset: {
          width: 0,
          height: 8,
      },
      shadowOpacity: 0.46,
      shadowRadius: 11.14,

      elevation: 17,


  },
  input:{
      flexDirection:"row",
      width:"90%",
      alignSelf:"center",
      alignItems:"center",
   
      marginTop:10,
      justifyContent:"space-between"

  },
  img4:{
      width:25,
      height:30,
  },

  touchCopy:{
      width:"15%",
      justifyContent:"center",
      alignItems:"center",
      backgroundColor:"#001246",
      paddingVertical:13,
      // paddingHorizontal:20
  },
  text2:{
      fontSize:16,
      fontFamily:fontConstant.medium,
      color:"#001246",

      paddingVertical:15,
      borderWidth:2,
      borderColor:"#D3D3D3",
      width:"85%",
      paddingLeft:25
  },
  text3:{
      fontSize:16,
      fontFamily:fontConstant.medium,
      color:"#001246",
      width:"90%",
      alignSelf:"center",
      
  }
})