import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList,
  Modal,
  StatusBar,
  Keyboard,
  BackHandler,
  RefreshControl
} from 'react-native';
import React, {Component, useRef, useState, useEffect} from 'react';

import { useFocusEffect } from '@react-navigation/native';
import CustomHeader from '../../custom/CustomHeader';
import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import MaterialTabs from 'react-native-material-tabs';
import { useIsFocused } from "@react-navigation/native";
import {useDispatch, useSelector } from 'react-redux';
import {baseURL} from '../../utils/URL';
import Loader from '../../utils/loader';
import moment from 'moment';
import { useScrollToTop } from '@react-navigation/native';
import { actions } from '../../redux/reducers';
const windowWidth = Dimensions.get('window').width;
const layout=windowWidth-20

const Portfolio = props => {

  const {route, navigation} = props;
  const [modalPadding, setModalPaddings] = useState(0);
  const [refreshing, setRefreshing] = useState(false);
  const [portfolioData,setPortfolioData] = useState(null);
  const [LiveEvents,setLiveEvents] =  useState(null);
  const [CompleteEvents,setCompleteEvents] =  useState(null);
 const [selectedTab, setSelectedTab] = useState(0);
 const [value1, setValue1] = useState("1");
 const [value2, setValue2] = useState("1");
 const [loader,setLoader] = useState(false);
  const [liveInvest,setLiveInvest] = useState(null)
  const [completeInvest,setCompleteInvest] = useState(null)
 const [cancelTrade, setCancelTrade] = useState(false);
 const [cancelTradeConf, setCancelTradeConf] = useState(false);


 const [date, setDate] = useState(new Date())
 const [openLive, setOpenLive] = useState(false)
 const [openComplete, setOpenComplete] = useState(false)
 const isFocused = useIsFocused();
 const reff = React.useRef(null);
 const dispatch = useDispatch();

 const {jwtToken} = useSelector((state) => state.reducers);


 useEffect(()=> {
 let sub =  navigation.addListener('focus',()=>{
  portfolioCall();
  callMainApi();
 })
  const keyboardDidShowListener = Keyboard.addListener(
    'keyboardDidShow',
    (e) => {
      // setPadding(0)
      if(Platform.OS == 'android') {
        setModalPaddings(0)
      }
      else {
        setModalPaddings(e.endCoordinates.height)
      }
      
      // alert("OPen");
       
    }
  );

  const keyboardDidHideListener = Keyboard.addListener(
    'keyboardDidHide',
    () => {

      setModalPaddings(0)

    }
  );

  return () => {
      sub;
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
  };
 
},[]);


useFocusEffect(
  React.useCallback(() => {
    const backAction = () => {
     props.navigation.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, [])
);

useEffect(()=>{
  setValue1("1");
  setValue2("1");
},[isFocused]);

useEffect(()=>{
  if(portfolioData !== null)
  {

    let tempLivedata = portfolioData?.live;
    let tempCompleteData = portfolioData?.complete;
    let liveSum = 0 ;
    let completeSum = 0;
    tempLivedata?.forEach((item,index)=>{
      liveSum += item?.amount*item?.quantity
    })
    tempCompleteData?.forEach((item,index)=>{
      completeSum += item?.amount*item?.quantity
    })
    setLiveInvest(liveSum);
    setCompleteInvest(completeSum);
    

  }
},[portfolioData])

const onRefresh = () => {
  setRefreshing(true);
  setSelectedTab(0)
  portfolioCall();

}
  useScrollToTop(reff);

// *****************Api call*************

const callMainApi = async () => {
  try {
    let response = await fetch(baseURL + `maintenance/`);
    if (response && response.status == 200) {
      let jsonData = await response.json();
      if (jsonData?.maintenance) {
        dispatch(actions.setIntroStatus("check"));
      }
    }
  }
  catch (error) {
    console.log("error-->", error)
  }
}

  const portfolioCall = async () => {
    try {
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL + `portfolio/`, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Authorization': tokenSent,
        }
      })
      if (response && response.status == 200) {
        let jsonData = await response.json();
        console.log(jsonData);
        let liveDataTemp = jsonData?.data?.live;
        let CompleteDataTemp = jsonData?.data?.complete;
        setLiveEvents(liveDataTemp);
        setCompleteEvents(CompleteDataTemp);
        setPortfolioData(jsonData?.data);

        setRefreshing(false);

      }
      else {
        let jsonData = await response.json();
        setRefreshing(false);
      }


    }
    catch (error) {
      setRefreshing(false);
    }
  }


// **************Rendring***********************

const renderCompleteEvent=({item,index,binary_event})=>{
return(
  <TouchableOpacity
    key={index}
    activeOpacity={0.9}

    style={{ marginTop: 20, borderRadius: 10, marginHorizontal: 15, backgroundColor: '#FFFFFF', width: layout - 10, alignSelf: 'center', elevation: Platform.OS === 'ios' ? null : 10, shadowColor: 'gray', shadowOpacity: 1, shadowRadius: 3, shadowOffset: { height: 0.2, width: 0.2 } }}>
    <View style={{ backgroundColor: '#FFFFFF', flexDirection: 'column', borderRadius: 10, paddingVertical: 18, paddingHorizontal: 15, width: '100%' }}>

      <View style={{ backgroundColor: '#FFFFFF', flexDirection: 'row', }}>

        <View style={{
          height: 70,
          width: 70,
          borderWidth:0.1,
          borderRadius: 35,
          backgroundColor: "#FFFFFF",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 10,
          },
          shadowOpacity: 0.51,
          shadowRadius: 13.16,
          elevation: 20,
        }}>
          <Image
          resizeMode='contain'  
          source={item?.binary_event?.photo?.file ? {uri:item?.binary_event?.photo?.file }: imageConstant.defaultimage}
          style = {{
           height: 70, width: 70, borderRadius:70/2}}
           
           />
        </View>

        <View style={{ width: 200, marginLeft: 15, backgroundColor: 'white' }}>
          <Text style={{ fontWeight: '900', fontSize: 15, lineHeight: 20, textAlign: 'left', color: '#1A1A1A' }}>{item?.binary_event?.title}</Text>
        </View>

        {/* <TouchableOpacity style={{ height: 22, width: 22, }}>
          <Image source={imageConstant.save} style={{ height: 22, width: 22, resizeMode: 'contain' }} />
        </TouchableOpacity> */}

      </View>

      <View style={{ marginTop: 15, width: '100%', alignItems:"center" ,flexDirection:"row", justifyContent:"space-between"}}>

        <View>
          <Text style={{ color: '#414246', fontWeight: '800', fontSize: 13 }}>Investment</Text>
          <Text style={{ color: '#0049F1', fontWeight: '600' }}>{item?.selected_option == "yes" ? "Yes" : "No"}<Text style={{ color: '#414246' }}>{` ₹${item?.amount * item?.quantity}`} </Text></Text>
        </View>
        <View>
          
          <Text style={{ color: '#414246' }}>Matched Quantity: {item.matched_quantity}/{item.quantity}</Text>
          <Text style={{ color: '#9c350c', fontWeight: '700' }}>{item.binary_event.state==='SETTLE_SUCCESS_YES'?<Text style={{color:'#1c8c25'}}>Settled at Yes</Text>:item.binary_event.state==='SETTLE_SUCCESS_NO'?<Text style={{color:'#ad1522'}}>Settled at No</Text>:item.binary_event.state==='CLOSED'?<Text style={{color:'#1c8c25'}}>Waiting Settlement</Text>:'Suspended'
          }
          </Text>
          
        </View>
       



        {/* <Text style={{ color: '#414246', fontFamily:fontConstant.medium, fontSize: 14 }}>Settled at <Text style={{ color: '#0049F1', fontWeight: '600' }}>{item?.selected_option == "yes" ? "Yes" : "No"}</Text></Text> */}
        
      </View>

     

     </View>

  </TouchableOpacity>
)
}


  const renderLiveEvent = ({ item, index }) => {
    let tempArray = item?.binary_event?.event_history?.volume_history;
    let sum  = 0 
    for(let i = 0 ;i<tempArray.length ;i++){
      sum  = tempArray[i].value+sum
    }

    return (
      <TouchableOpacity
        key={index}
        activeOpacity={0.9}
        // onPress={() => {
        //   navigation.navigate("EventPortfolio", {
        //     type: item.status
        //   })
        // }}
        style={{ marginTop: 20,borderRadius: 10, marginHorizontal: 15, backgroundColor: '#FFFFFF', width: layout - 10, alignSelf: 'center', elevation: Platform.OS === 'ios' ? null : 10, shadowColor: 'gray', shadowOpacity: 1, shadowRadius: 3, shadowOffset: { height: 0.2, width: 0.2 } }}>
        <View style={{ backgroundColor: '#FFFFFF', flexDirection: 'column', borderRadius: 10, paddingVertical: 18, paddingHorizontal: 15, width: '100%' }}>

          <View style={{ backgroundColor: '#FFFFFF', flexDirection: 'row', }}>

            <View style={{
              height: 70,
              width: 70,
              borderRadius: 35,
              borderWidth: 0.1,
              backgroundColor: "#FFFFFF",
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 10,
                    },
                    shadowOpacity: 0.51,
                    shadowRadius: 13.16,
                    elevation: 20,
            }}>
              <Image 
              source={item?.binary_event?.photo?.file ? { uri:item?.binary_event?.photo?.file } : imageConstant.defaultimage} 
              style={{ height: 70, width: 70, borderRadius:70/2}} 
              resizeMode='contain'
              />
            </View>

            <View style={{ width: 200, marginLeft: 15, backgroundColor: 'white' }}>
              <Text style={{ fontWeight: '900', fontSize: 15, lineHeight: 20, textAlign: 'left', color: '#1A1A1A' }}>{item?.binary_event?.title}</Text>
            </View>

            {/* <TouchableOpacity style={{ height: 22, width: 22, }}>
              <Image source={imageConstant.save} style={{ height: 22, width: 22, resizeMode: 'contain' }} />
            </TouchableOpacity> */}

          </View>

          <View style={{flexDirection:"row" ,alignItems:"center", marginTop: 15, width: '90%', justifyContent: 'space-between' }}>

            <View>
              <Text style={{ color: '#414246', fontWeight: '600', fontSize: 12 }}>Investment</Text>
              <Text style={{ color: '#0049F1', fontWeight: '600' }}>{item?.selected_option == "yes" ? "Yes" : "No"}<Text style={{ color: '#414246' }}>{` ₹${item?.amount * item?.quantity}`}</Text></Text>
            </View>

            <View style={{ 
            // position: 'absolute', 
            // left: layout - 140, 
            // top: 140,
             flexDirection: 'row' 
             }}>
            <Image source={imageConstant.star} style={styles.starStyle} />
            <Text style={{ color: '#414246', fontSize: 12, fontFamily: 'Roboto', fontWeight: '400' }}>  {item.txt1}  <Text style={{ color: '#50B748' }}>{`${item?.matched_quantity}/${item?.quantity} Matched`}</Text></Text>
          </View>
          </View>

          {/* <View style={{ marginTop: 15, width: '65%', justifyContent: 'space-between' }}>

            <Text style={{ color: '#414246', fontWeight: '600', fontSize: 12 }}>Current Value</Text>
            <Text style={{ color: '#414246', fontWeight: '600' }}>{`₹ ${item?.selected_option == "yes" ? item?.binary_event?.price_yes : item?.binary_event?.price_no}`}</Text>
          </View> */}

         

          {/* {
            item.status == true
              ?
              <>
                <View style={{ position: 'absolute', left: layout - 130, top: 120, flexDirection: 'row' }}>
                  <Image source={imageConstant.star} style={styles.starStyle} />
                  <Text style={{ color: '#414246', fontSize: 12, fontFamily: 'Roboto', fontWeight: '400' }}>  {item.txt1}  <Text style={{ color: '#50B748' }}>Matched</Text></Text>
                </View>

                <TouchableOpacity
                  onPress={() => { setExitTrade1(!exitTrade1) }}
                  style={{ position: 'absolute', left: layout - 120, top: 150, height: 35, backgroundColor: '#414246', width: 100, borderRadius: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ color: '#FFFFFF', fontWeight: '700', fontSize: 12 }}>EXIT TRADE</Text>
                </TouchableOpacity>
              </>
              :
              <>
                <TouchableOpacity style={{ position: 'absolute', left: layout - 130, top: 120, flexDirection: 'row' }}>
                  <Image source={imageConstant.refresh} style={styles.refreshStyle} />
                  <Text style={{ color: '#414246', fontSize: 12, fontFamily: fontConstant.regular, fontWeight: '400', marginLeft: 5 }}>Finding Match</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => { setCancelTrade(!cancelTrade) }}
                  style={{ position: 'absolute', left: layout - 120, top: 150, height: 35, backgroundColor: '#414246', width: 100, borderRadius: 40, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ color: '#FFFFFF', fontWeight: '700', fontSize: 12 }}>CANCEL TRADE</Text>
                </TouchableOpacity>
              </>
          } */}

        </View>

        <View style={{
          width: '100%',
          // flexDirection:'row',
          // alignItems : 'center',
          height: 50,
          backgroundColor: '#F8F8F8',
          marginTop: 0,
          borderBottomEndRadius: 10,
          borderBottomStartRadius: 10
        }}>

          <View style={{ paddingTop: 10, backgroundColor: '#F8F8F8', width: '90%', height: 50, alignSelf: 'center', flexDirection: 'row', justifyContent: 'space-between', }}>
            <View>
              <View style={{ justifyContent: 'center', marginRight: 100, flexDirection: 'row' }}>
                <Image
                  style={{ width: 18, height: 18 }}
                  source={require('../../assets/images/timer.png')}
                />
                <Text style={{ color: '#414246', fontWeight: '600', fontSize: 12, marginLeft: 8 }}>{moment(item?.binary_event?.end_datetime).fromNow()}</Text>
              </View>
              <Text style={{ color: '#414246', fontWeight: '500', fontSize: 9, marginTop: 2 }}>Ends in</Text>
            </View>

            <View>
              <View style={{ marginLeft: 15, justifyContent: 'center', flexDirection: 'row' }}>
                <Image
                  style={{ width: 18, height: 18 }}
                  source={require('../../assets/images/volume.png')}
                />
                <Text style={{ color: '#414246', fontWeight: '500', fontSize: 12, marginLeft: 8 }}>{`₹${sum}`}</Text>
              </View>
              <Text style={{ color: '#414246', fontWeight: '500', fontSize: 9, marginLeft: 12, marginTop: 2 }}>Volume</Text>
            </View>
          </View>
        </View>

      </TouchableOpacity>
    )
  }

  
  const emptyListComponent = () => {
    return (
      <View style={{
        width:"90%",
        alignSelf:"center",
        alignItems:"center",
        marginTop:30
      }}>
        <Text style={{
          fontSize:18,
          fontFamily:fontConstant.medium,
          color:"#024BE8"
        }}>No Events</Text>
      </View>
    )
  }


  return (
    <>
      <SafeAreaView style={{ flex: 0, backgroundColor: '#0049F1' }}>
      </SafeAreaView>
      <SafeAreaView style={{ flex: 1, backgroundColor: '#0049F1' }}>


        {loader && <Loader data={loader} />}

        <View style={{ backgroundColor: '#0049F1', width: '100%', alignSelf: 'center', flexDirection: 'row', marginTop: 10, padding: 4, }}>

          <TouchableOpacity
            style={{
              marginVertical: 4,
              position: 'absolute',
              height: 36,
              width: 36,
              left: 15,
              zIndex: 1
            }}
            onPress={() => {
              navigation.toggleDrawer()
            }}>
            <Image source={imageConstant.whiteOptions}
              style={[styles.optionImageStyle]}
            />
          </TouchableOpacity>
          <Text style={{ fontWeight: '700', fontSize: 16, padding: 10, textAlign: 'center', width: '100%', color: '#FFFFFF' }}>My Portfolio</Text>
        </View>
        <View style={{ backgroundColor: '#0049F1', width: '100%', padding: 10 }}>

          <View style={{ marginTop: 10 }}>
            <Text style={{ color: '#FFFFFF', fontWeight: '700', fontSize: 40, textAlign: 'center', fontFamily: fontConstant.regular }}>{`₹ ${selectedTab == 0 ? liveInvest ?? 0 : completeInvest ?? 0}`}</Text>
            <Text style={{ color: '#FFFFFF', fontWeight: '400', fontSize: 16, textAlign: 'center', fontFamily: fontConstant.regular, marginVertical: 5 }}>Total Investment</Text>
          </View>



        </View>

        <MaterialTabs
          items={['Live Event', 'Complete Event']}
          selectedIndex={selectedTab}
          onChange={setSelectedTab}
          barColor="#FFFFFF"
          indicatorColor="#0049f1"
          activeTextColor="#141416"
          inactiveTextColor='#B1B5C3'
          uppercase={false}
        >
        </MaterialTabs>



        {
          selectedTab == 0
            ?
            <View style={{ flex: 1, width: '100%', backgroundColor: '#FFFFFF' }}>

              {
                LiveEvents?.length > 0 ?
                  <View style={{ flexDirection: 'row', justifyContent: "flex-end", width: layout - 10, alignSelf: 'center', marginTop: 5, alignItems: "center", paddingVertical: 8 }} />
                  :
                  <></>
              }

              <FlatList
                ref={reff}
                refreshControl={
                  <RefreshControl
                    colors={['#0049F1', '#FF2567']}
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                  />
                }
                data={LiveEvents}
                renderItem={renderLiveEvent}
                contentContainerStyle={{
                  paddingBottom: 120,
                }}
                ListEmptyComponent={emptyListComponent} />
            </View>

            :
            <View style={{ flex: 1, backgroundColor: '#FFFFFF', width: '100%', }}>

              {
                CompleteEvents?.length > 0 ?

                  <View style={{ flexDirection: 'row', justifyContent: 'flex-end', width: layout - 10, alignSelf: 'center', marginTop: 5, alignItems: "center", paddingVertical: 8 }}>
                  </View>
                  :
                  <></>
              }
              <FlatList
                ref={reff}
                refreshControl={
                  <RefreshControl
                    colors={['#0049F1', '#FF2567']}
                    refreshing={refreshing}
                    onRefresh={onRefresh}
                  />
                }
                data={CompleteEvents}
                renderItem={renderCompleteEvent}
                contentContainerStyle={{
                  paddingBottom: 120
                }}
                ListEmptyComponent={emptyListComponent} />
            </View>

        }





      </SafeAreaView>
    </>
  )
}

export default Portfolio

const styles = StyleSheet.create({
  exitButtonStyle:{
    width:'95%',
    marginTop:30,
    backgroundColor:'rgba(0, 73, 241, 1)',
    height:44,
    width:343,
    borderRadius:40,
    alignItems:'center',
    justifyContent:'center',
  },
  textModel2Style:{
    color:'#414246',
    fontWeight:'600',
    fontSize:12,
    fontFamily:fontConstant.regular,
    marginTop:2
  },
  refreshStyle:{
    width:15,
    height:15,
  },
  starStyle:{
    width:18,
    height:18,
  },
  dropdownStyle:{
    width:"30%",
    height:30,
    borderWidth:0.5,
    borderColor:'#D9D9D9',
    borderRadius:5,
    padding:5
  },
  calStyle:{
    // position:'absolute',
    // right:15,
    // top:10,
    width:36,
    height:36,
    resizeMode:'contain'
  },
  firstView:{
    // position:'absolute',
    // top:122,
    borderRadius:8,
    backgroundColor:'#FFFFFF',
    height:56,
    width:Dimensions.get('screen').width-30,
    alignSelf:'center',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-around',
  },
  moneyStyle:{
    height: 24,
    width: 24,
  },
  bellImageStyle: {
      marginVertical :4,
      position:'absolute',
      height: 36,
      width: 36,
      right:15,
      resizeMode:'contain'
  },
  optionImageStyle: {
      // marginVertical :4,
      // position:'absolute',
      height: 36,
      width: 36,
      // left:50
  },
  plusImageStyle: {
    height: 24,
    width: 24,
    marginLeft:30,
  },
  tickImageStyle: {
    // marginVertical :4,
    height: 20,
    width: 20,
    resizeMode:'contain'
  },
  buttonStyle:{
    width:163,
    height:56,
    backgroundColor:'#FFFFFF',
    borderRadius:8,
    flexDirection:'row',
    // alignItems:'center',
    justifyContent:'space-between',
    alignItems:'center',
  },
  tabStyle:{
    width:110,
    height:60,
    backgroundColor:'#F4F5F6',
    borderRadius:8,
    flexDirection:'column',
    padding:10
    // alignItems:'center',
    // justifyContent:'space-between',
    // alignItems:'center',
  },
  tabHeading:{
    color:'#141416',
    fontWeight:'400',
    fontSize:12,
    fontFamily:fontConstant.regular
  },
  tabValue:{
    color:'#141416',
    fontWeight:'500',
    fontSize:18,
    fontFamily:fontConstant.regular,
    marginTop:5
  },
  textStyle: {
    // marginTop:5,
    color: "#000000",
    fontSize:16,
    fontWeight:'400',
    fontFamily:fontConstant.regular
  },
  dateStyle:{
    color: "#B1B5C3",
    fontSize:14,
    fontWeight:'600',
    fontFamily:fontConstant.regular,
    marginTop:20,
    marginLeft:15,
    marginBottom:10
  },
centeredView: {
  flex: 1,
  justifyContent: "center",
  alignItems: "center",
  // marginTop: 22,
  // bottom:0,
  // position:'absolute',
  // width:'100%',
  // height:'100%',
  backgroundColor:'rgba(0, 0, 0, 0.4)'
},
cancelModalView:{
  bottom:0,
  position:'absolute',
  // margin: 20,
  backgroundColor: "white",
  borderTopStartRadius: 30,
  borderTopEndRadius: 30,
  padding: 20,
  // alignItems: "center",
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowOpacity: 0.25,
  shadowRadius: 4,
  elevation: 5,
  width:'100%',
  height:293,
  // backgroundColor:'red'
},
cancelModalView2:{
  bottom:0,
  position:'absolute',
  // margin: 20,
  backgroundColor: "white",
  borderTopStartRadius: 20,
  borderTopEndRadius: 20,
  padding: 20,
  // alignItems: "center",
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowOpacity: 0.25,
  shadowRadius: 4,
  elevation: 5,
  width:'100%',
  height:400,
  // backgroundColor:'red'
},
cancelButtonStyle:{
  width:30,
  height:30,
},
modalHeadingStyle:{
  marginTop:30,
  color :'#141416',
  fontWeight:'500',
  fontSize:16,
  fontFamily:fontConstant.regular,
  textAlign:'center'
},
input1 : {
  // textAlign: 'center',
  marginTop: 10,
  fontSize: 16,
  fontWeight:'400',
  color:'#141416',
  // backgroundColor : 'red'
},
tabStyle2:{
  width:75,
  height:40,
  backgroundColor:'#E6E8EC',
  borderRadius:4,
  flexDirection:'column',
  padding:10
  // alignItems:'center',
  // justifyContent:'space-between',
  // alignItems:'center',
},
tabValue1:{
  color:'#141416',
  fontWeight:'400',
  fontSize:16,
  fontFamily:fontConstant.regular,
  // marginTop:5,
  textAlign:'center'
},
mainButton:{
  width:350,
  height:65,
  resizeMode:'contain'
  // marginLeft:10,
  // marginTop:30
},
mainButton2:{
  width:345,
  height:65,
  marginLeft:10,
  marginTop:40
},
exitModalView1:{
  bottom:0,
  position:'absolute',
  // margin: 20,
  backgroundColor: "white",
  borderTopStartRadius: 20,
  borderTopEndRadius: 20,
  padding: 20,
  alignItems: "center",
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowOpacity: 0.25,
  shadowRadius: 4,
  elevation: 5,
  width:'100%',
  height:400,
  // backgroundColor:'red'
},
exitModalView2:{
  bottom:0,
  position:'absolute',
  // margin: 20,
  backgroundColor: "white",
  borderTopStartRadius: 30,
  borderTopEndRadius: 30,
  padding: 20,
  alignItems: "center",
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowOpacity: 0.25,
  shadowRadius: 4,
  elevation: 5,
  width:'100%',
  height:313,
  // backgroundColor:'red'
},
// exitModalView3:{
//   bottom:modalPadding,
//   position:'absolute',
//   // margin: 20,
//   backgroundColor: "white",
//   borderTopStartRadius: 30,
//   borderTopEndRadius: 30,
//   padding: 20,
//   // alignItems: "center",
//   shadowColor: "#000",
//   shadowOffset: {
//     width: 0,
//     height: 2
//   },
//   shadowOpacity: 0.25,
//   shadowRadius: 4,
//   elevation: 5,
//   width:'100%',
//   height:340,
//   // backgroundColor:'red'
// },
})