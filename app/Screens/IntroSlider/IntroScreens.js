import { Text, View,ImageBackground,Image,TouchableOpacity,StyleSheet,SafeAreaView,Dimensions,Platform, Alert, TextInput} from 'react-native'
import React, { Component, useState } from 'react'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { imageConstant, ProgessFinish } from '../../utils/constant';
import {actions} from "../../redux/reducers"
import { useDispatch } from 'react-redux';
import { Height } from '../../dimension/dimension';

import moment from 'moment';
import { Switch } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const layout=windowWidth-20
const screenHeight = windowHeight
import AppIntroSlider from 'react-native-app-intro-slider';


const IntroScreens = () => {
    const [toggle,setToggle] =  useState(1);
    const [date,setDate]=useState(new Date())
    const [open, setOpen] = useState(false)
    const [isEnabled, setIsEnabled] = useState(false);
    const [active,setActive] =  useState(0);
    const dispatch =  useDispatch();
    // constructor(props) {
    //   super(props);
    //   this.state = {
    //     toggle: 1,
    //   };
    // }
    let day=moment(date).date()
    let month=moment(date).month() + 1
    let Year=moment(date).year()

const Slides=[
    {
        key:'one',
        title:'title1',
      
    },
    {
        key:'two',
        title:'title2',
       
    },
    {
        key:'three',
        title:'title3'
    },
    {
        key:'four',
        title:'title4'
    }
]
const doneButton=()=>{
    return(
        <TouchableOpacity
        style={{
            position:"absolute",
            right : windowWidth*0.38,
            bottom:"3%"
        }}
        onPress={() => {
         
          dispatch(actions.setIntroStatus("login"));
        }}
      >
        <Image
          style={styles.ImgStyle}
          resizeMode="contain"
          source={ProgessFinish}
        />
      </TouchableOpacity>
    )
}

const renderItem=({item})=>{
    console.log(item)
return(
    <View style={{flex:1}}>
        {item.title=='title1'?
      <ImageBackground
        source={imageConstant.Slide1}
        style={[{...styles.ImageStyle},{flex:1,marginTop:"2%",height:Height * 0.72,
        width: windowHeight * 0.43 ,}]}
        resizeMode='contain'
      >
        <View style={{ backgroundColor: '#FFFFFF', flexDirection: 'row', alignItems: 'center', width: '70%', justifyContent: 'space-between', height: 50, bottom: 5, position: 'absolute', alignSelf: 'center' }}>
          <Image
            style={{ width: 40, height: 20 }}
            resizeMode="contain"
            source={imageConstant.lion}
          />

          <Image
            style={{ width: 40, height: 20 }}
            resizeMode="contain"
            source={imageConstant.upi}
          />

          <TouchableOpacity
            //   style={styles.TouchStyle}
            onPress={() => {
              dispatch(actions.setIntroStatus("login"));
            }}
          >
            <Text style={{ color: 'rgba(50, 50, 50, 0.79)', fontWeight: '500', fontSize: 16, letterSpacing: 0.005 }}>skip</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
            :
            item.title=='title2'
            ?
        <ImageBackground
          source={imageConstant.Slide2}
          style={styles.ImageStyle2}
        >
          {/* <TouchableOpacity
              style={styles.TouchStyle}
              onPress={() => {
                
                // setToggle(3);
                // this.setState({ toggle: 3 });
              }}
            >
              <Image
                style={styles.ImgStyle}
                resizeMode="contain"
                source={imageConstant.Progress2}
              />
            </TouchableOpacity> */}

          <View style={{ backgroundColor: '#FFFFFF', flexDirection: 'row', alignItems: 'center', width: '100%', justifyContent: 'center', height: 50, bottom: 5, position: 'absolute', alignSelf: 'center' }}>
            <Text style={{
              color:"#323232"
            }}>Made with love in India  <Image source={imageConstant.flag} /> <Image source={imageConstant.heart} style={{ height: 22, width: 22 }} /></Text></View>

        </ImageBackground>
           : 
           item.title=='title3'?
          <ImageBackground
            source={imageConstant.Slide3}
            style={styles.ImageStyle3}
          >

          </ImageBackground>
           :
           item.title=='title4'?
        //    <ImageBackground
        //    source={imageConstant.Slide4}
        //    style={styles.ImageStyle}
        //  >
        //  </ImageBackground>
            <Image
              source={imageConstant.Slide4}
              style={styles.ImageStyle}
            />
           :
           null
           }
    </View>
)

}

let activeSLider = 0 ;
console.log("Activve",active)
  return (
    <View
    style={{
       backgroundColor:'#FFFFFF',
        flex:1
    }}>
      <AppIntroSlider
      data={Slides}
      renderItem={renderItem}
      keyExtractor={(item)=>item.key}
      renderDoneButton={doneButton}
      dotClickEnabled={false}
      dotStyle={{
        backgroundColor:"#FFFFFF"
      }}
      onSlideChange={(item)=>setActive(item)}
      renderNextButton={()=>{
        return(
            <View style={{
                position:"absolute",
                right : windowWidth*0.38,
                bottom:"3%"
            }}>
           {
             active == 0 && (
              <Image
              style={styles.ImgStyle}
              resizeMode="contain"
              source={imageConstant.Progress1}
            />
             )
           }
           {
             active == 1 && (
              <Image
              style={styles.ImgStyle}
              resizeMode="contain"
              source={imageConstant.Progress2}
            />
             )
           }
           {
             active == 2 && (
              <Image
              style={styles.ImgStyle}
              resizeMode="contain"
              source={imageConstant.Progress3}
            />
             )
           }
            </View>
        )
      }}
      
      />
    </View>
  )
}

export default IntroScreens

const styles = StyleSheet.create({
  
    ImageStyle:{ 
      height:Height * 0.70,
      width:"89%",
      resizeMode:'contain',
      alignSelf:'center',
      marginTop:-20
    },
    ImageStyle2:{
      flex:1,
      height:Height * 0.62,
      width: windowHeight * 0.43 ,
      resizeMode:'contain',
      alignSelf:'center',
      marginTop:'2%'
    },
    ImageStyle3:{ 
      flex:1,
      height:Height * 0.62,
      width: windowHeight * 0.43 ,
      // backgroundColor:"green",
      resizeMode:'contain',
      alignSelf:'center',
      // alignItems:'center'
      // backgroundColor:"green"
      marginTop:'8%'
    },

    TouchStyle:{
        height:wp('15%'),
        width:wp('15%'),
        alignSelf:'center',
        // backgroundColor:'green',
        position:'absolute',
        bottom: Platform.OS === 'ios' ? 45: 50
    },
    ImgStyle:{
        height:wp('15%'),
        width:wp('15%'),
       
        // alignSelf: "center",
        // justifyContent: "center",
        // marginTop: hp(70),
        // backgroundColor:'pink'
      },
      toglle5View:{
        height:77,
        backgroundColor:'#024BEB',
        flexDirection:'row',
        alignItems:'center',
      },
      titleText:{
        fontSize:20,
        color:'#FFFFFF',
        fontWeight:'500',
        position:'absolute',
        left:'35%',
        width:windowWidth-30
      },
      labelText:{
        left:20,
        marginTop:10,
        fontSize:13
      },
      textinput:{
        backgroundColor:'#D6E3F3',
        marginHorizontal:20,
        borderRadius:4,
        marginTop:10,
        paddingLeft:25,
        shadowColor:'red',
        shadowOpacity:10,
        shadowRadius:10,
        elevation:-5,
        position:'relative',
        shadowOffset:{width:-5,height:5},
      }
})

