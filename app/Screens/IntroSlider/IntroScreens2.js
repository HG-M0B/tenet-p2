import { Text, View,ImageBackground,Image,TouchableOpacity,StyleSheet,SafeAreaView,Dimensions,Platform, Alert, TextInput} from 'react-native'
import React, { Component, useState } from 'react'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { imageConstant, ProgessFinish } from '../../utils/constant';
import AsyncStorage from "@react-native-async-storage/async-storage";
import {actions} from "../../redux/reducers"
import { useDispatch } from 'react-redux';
import { Height } from '../../dimension/dimension';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';
import { Switch } from 'react-native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const layout=windowWidth-20
const screenHeight = windowHeight

export default IntroScreens2 = ()=>{
  
  const [toggle,setToggle] =  useState(1);
  const [date,setDate]=useState(new Date())
  const [open, setOpen] = useState(false)
  const [isEnabled, setIsEnabled] = useState(false);
  const dispatch =  useDispatch();
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     toggle: 1,
  //   };
  // }
  let day=moment(date).date()
  let month=moment(date).month() + 1
  let Year=moment(date).year()
  console.log(isEnabled)
  console.log()

    return (
      <SafeAreaView style={{flex:1,backgroundColor:"#FFFFFF"}}>
      <View style={{flex:1}}>

          {toggle == 1 ? (
            <ImageBackground
              source={imageConstant.Slide1}
              style={styles.ImageStyle}
              resizeMode='contain'
            >
              <TouchableOpacity
                style={styles.TouchStyle}
                onPress={() => {
                  setToggle(2);
                  // this.setState({ toggle: 2 });
                }}
              >

                <Image
                  style={styles.ImgStyle}
                  resizeMode="contain"
                  source={imageConstant.Progress1}
                />
              </TouchableOpacity>
              <View style={{ backgroundColor: '#FFFFFF', flexDirection: 'row', alignItems: 'center', width: '70%', justifyContent: 'space-between', height: 50, bottom: -5, position: 'absolute', alignSelf: 'center' }}>
                <Image
                  style={{ width: 40, height: 20 }}
                  resizeMode="contain"
                  source={imageConstant.lion}
                />

                <Image
                  style={{ width: 40, height: 20 }}
                  resizeMode="contain"
                  source={imageConstant.upi}
                />

                <TouchableOpacity
                  //   style={styles.TouchStyle}
                  onPress={() => {
                    dispatch(actions.setIntroStatus("login"));
                  }}
                >
                  <Text style={{ color: 'rgba(50, 50, 50, 0.79)', fontWeight: '500', fontSize: 16, letterSpacing: 0.005 }}>skip</Text>
                </TouchableOpacity>

              </View>
            </ImageBackground>
          ) :
            toggle == 2 ?

              <ImageBackground
                source={imageConstant.Slide2}
                style={styles.ImageStyle2}
              >
                <TouchableOpacity
                  style={styles.TouchStyle}
                  onPress={() => {
                    setToggle(3);
                    // this.setState({ toggle: 3 });
                  }}
                >
                  <Image
                    style={styles.ImgStyle}
                    resizeMode="contain"
                    source={imageConstant.Progress2}
                  />
                </TouchableOpacity>
                
                <View style={{ backgroundColor: '#FFFFFF', flexDirection: 'row', alignItems: 'center', width: '100%', justifyContent: 'center', height: 50, bottom: -5, position: 'absolute', alignSelf: 'center' }}>
                  <Text>made with love in India  <Image source={imageConstant.flag}/> </Text></View>
              </ImageBackground> :
              toggle == 3 ?
                <ImageBackground
                  source={imageConstant.Slide3}
                  style={styles.ImageStyle3}
                >
                  <TouchableOpacity
                    style={styles.TouchStyle}
                    onPress={() => {
                      setToggle(4);
                      // this.setState({ toggle: 4 });
                    }}
                  >
                    <Image
                      style={styles.ImgStyle}
                      resizeMode="contain"
                      source={imageConstant.Progress3}
                    />
                  </TouchableOpacity>  
                </ImageBackground>
                :
                toggle == 4 ?
                  <ImageBackground
                    source={imageConstant.Slider_4}
                    style={styles.ImageStyle}
                  >
                    <TouchableOpacity
                      style={styles.TouchStyle}
                      onPress={() => {
                       
                        dispatch(actions.setIntroStatus("login"));
                      }}
                    >
                      <Image
                        style={styles.ImgStyle}
                        resizeMode="contain"
                        source={ProgessFinish}
                      />
                    </TouchableOpacity>
                  </ImageBackground>
                
                  :null}
      </View>
      </SafeAreaView>
    );

}

const styles = StyleSheet.create({
    ImageStyle:{ 
      flex:1,
      height:Height * 0.72,
      width: windowHeight * 0.43 ,
      // backgroundColor:"green",
      resizeMode:'contain',
      alignSelf:'center',
      // alignItems:'center'
      // backgroundColor:"green"
      marginTop:'2%'
    },
    ImageStyle2:{
      flex:1,
      height:Height * 0.60,
      width: windowHeight * 0.43 ,
      // backgroundColor:"green",
      resizeMode:'contain',
      alignSelf:'center',
      // alignItems:'center'
      // backgroundColor:"green"
      marginTop:'8%'
    },
    ImageStyle3:{ 
      flex:1,
      height:Height * 0.62,
      width: windowHeight * 0.43 ,
      // backgroundColor:"green",
      resizeMode:'contain',
      alignSelf:'center',
      // alignItems:'center'
      // backgroundColor:"green"
      marginTop:'8%'
    },

    TouchStyle:{
        height:wp('15%'),
        width:wp('15%'),
        alignSelf:'center',
        // backgroundColor:'green',
        position:'absolute',
        bottom: Platform.OS === 'ios' ? 45: 50
    },
    ImgStyle:{
        height:wp('15%'),
        width:wp('15%'),
       
        // alignSelf: "center",
        // justifyContent: "center",
        // marginTop: hp(70),
        // backgroundColor:'pink'
      },
      toglle5View:{
        height:77,
        backgroundColor:'#024BEB',
        flexDirection:'row',
        alignItems:'center',
      },
      titleText:{
        fontSize:20,
        color:'#FFFFFF',
        fontWeight:'500',
        position:'absolute',
        left:'35%',
        width:windowWidth-30
      },
      labelText:{
        left:20,
        marginTop:10,
        fontSize:13
      },
      textinput:{
        backgroundColor:'#D6E3F3',
        marginHorizontal:20,
        borderRadius:4,
        marginTop:10,
        paddingLeft:25,
        shadowColor:'red',
        shadowOpacity:10,
        shadowRadius:10,
        elevation:-5,
        position:'relative',
        shadowOffset:{width:-5,height:5},
      }
})