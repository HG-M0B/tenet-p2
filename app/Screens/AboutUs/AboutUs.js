import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList
} from 'react-native';
import React, {Component, useEffect, useRef, useState} from 'react';

import CustomHeader from '../../custom/CustomHeader';
import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import { baseURL } from '../../utils/URL';
import { ColorSpace } from 'react-native-reanimated';

const windowWidth = Dimensions.get('window').width;
// const width=Platform.width
const layout=windowWidth-20

const AboutUs = props => {
  const [aboutUsdata, setAboutUsData]=useState([])
  useEffect(()=>{
    getAboutData()
  },[])
  const {route, navigation} = props;
  
  const getAboutData=async()=>{
    const response=await fetch(baseURL+'static/about-us/')
    if(response.status==200)
    {
      const res= await response.json()
      console.log(res)
      setAboutUsData(res)
    }
    else{
      console.log('status not ok',response.status)
    }
  }
  console.log(aboutUsdata)
  return (
    <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
      <CustomHeader
        headerText={'About Us'}
        img={imageConstant.back}
        navigation={props.navigation}
      />
      <ScrollView style={{backgroundColor:'#FFFFFF',flexDirection:'column'}}>
      <View style= {{width:'100%',borderBottomWidth:6, borderBottomColor:'#E6E8EC',}}>
        {/* <View style={{width:'100%',marginBottom:10}}>
        <Image source={imageConstant.betting2}
          style={{width:'100%',height:189}}
          />
        </View> */}
        {/* <View style={{width:'95%',alignSelf:'center'}}>
          <Text style={{color:'#141416',fontSize:28,fontWeight:'700',fontFamily:fontConstant.regular,marginBottom:10,}}>About Tenet</Text>
        </View> */}
      </View>
      <View style= {{width:'100%',backgroundColor:'white',height:'100%'}}>
      {/* <View style={{width:'92%', alignSelf:'center',marginTop:25}}>
        <Text style={styles.textStyle}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum</Text>
      </View> */}
      <View style={{width:'100%',marginTop:20,alignItems:'center'}}>
      {/* <Image
        style={{width:'100%',height:210}}
        source={imageConstant.bettingBanner}
      /> */}
      </View>
      <View style={{width:'92%', alignSelf:'center',marginTop:25}}>
        <Text style={styles.textStyle}>{aboutUsdata.about_us}</Text>
      </View>
      {/* <View style={{width:'92%', alignSelf:'center',marginTop:25,borderLeftWidth:4,borderLeftColor:'#C2F44E',paddingLeft:15}}>
        <Text style={styles.text1Style}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales </Text>
      </View> */}
      </View>
      </ScrollView>
    </SafeAreaView>
  )
}

export default AboutUs
   
const styles = StyleSheet.create({
  textStyle:{
    color:'#414246',
    fontWeight:'400',
    fontSize:16,
    fontFamily:fontConstant.regular,
    lineHeight:22,
  },
  text1Style:{
    color:'#414246',
    fontWeight:'500',
    fontSize:20,
    fontFamily:fontConstant.italic,
    lineHeight:30
  },
})