import { View, Text, TextInput, TouchableOpacity, Image } from 'react-native'
import React, { useEffect, useState } from 'react'
import CustomHeader from '../../custom/CustomHeader'
import { colorConstant, fontConstant, imageConstant } from '../../utils/constant'
import CustomInput from '../../custom/CustomInput'
import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'
import { Height } from '../../dimension/dimension'
import { baseURL } from '../../utils/URL'
import { useSelector } from 'react-redux'
import toastShow from '../../utils/Toast'
import validation from '../../utils/validation'
export default function AccountDetails(props) {
  const [addButton,setButtton] =  useState(false);
  const [money,setMoney] =  useState("");
  const [accountNumber,setAccountNumber] =  useState("");
  const [ifsc,setIfsc] =  useState("");
  const [bankName,setBankName] =  useState("");
  const [branchName,setBranchName] =  useState("");
  const [AcctHolderName,setAcctHolderName] =  useState("");
  const [email,setEmail] =  useState("");
  const [number,setNumber] =  useState("");
  const [address,setAddress] =  useState("");
  const [city,setCity] =  useState("");
  const [state,setState] =  useState("");
  const [pin,setPin] =  useState("");
  const [preFilled,setPrefilled] = useState(null);

const {navigation,route}  = props;



useEffect(()=>{
  let subs =  navigation.addListener('focus',()=>{
    getBankDetials();
    
  })
  return ()=>subs;
  },[])



  useEffect(()=>{
    if(preFilled != null){
      setAcctHolderName(preFilled?.name)
      setEmail(preFilled?.email)
      setNumber(preFilled?.phone)
      setAccountNumber(preFilled?.bankAccount)
      setIfsc(preFilled?.ifsc)
      setAddress(preFilled?.address1)
      setCity(preFilled?.city)
      setState(preFilled?.state)
      setPin(preFilled?.pincode)
    }
    },[preFilled])

  const {jwtToken} = useSelector((state)=>state.reducers);


  const accountNumberHandler = (text) => {
    setAccountNumber(text)
  }
  const ifscHandler = (text) => {
    setIfsc(text)

  }
  const bankNameHandler = (text) => {
    setBankName(text)
  }
  const branchNameHandler = (text) => {
    setBranchName(text)
  }

  const AcctholderName = (text) => {
    setAcctHolderName(text)
  }
  const emailHandler = (text) => {
    setEmail(text)
  }
  const numberHandler = (text) => {
    setNumber(text)
  }

  const addressHandler = (text) => {
    setAddress(text)
  }

  const cityHandler = (text) => {
    setCity(text)
  }
  const stateHandler = (text) => {
    setState(text)
  }
  const pinHandler = (text) => {
    setPin(text)
  }


  const validate=()=>{
    {
      AcctHolderName == ""
      ? toastShow("Name is required","red")
      : email == ""
      ? toastShow("Email is required","red")
      : validation.isEmail(email)
      ? toastShow("Email invalid format","red")
      : validation.isEmpty(number)
      ? toastShow("Number is required","red")
      : validation.isphoneNumber(number)
      ? toastShow("Please enter correct number","red")
      : validation.isEmpty(accountNumber)
      ? toastShow("Account number is required","red")
      :validation.isEmpty(ifsc)
      ? toastShow("IFSC is required","red")
      : validation.isEmpty(address)
      ? toastShow("Address is required","red")
      : validation.isEmpty(city)
      ? toastShow("City is required","red")
      : validation.isEmpty(state)
      ? toastShow("State is required","red")
      : validation.isEmpty(pin)
      ? toastShow("pin is required","red")
      : pin.length !== 6
      ? toastShow("Pincode must be 6 length","red")
      : checkPrefilledDataIsPresent()
    }
  }

  //Api call's



  const checkPrefilledDataIsPresent=()=>{
    if(preFilled == null)
    {
      bankAccountAdd();
    }
    else
    {
      props.navigation.navigate("KycVerification")
    }
  }
  const bankAccountAdd=async()=>{
    try {
      let reqData = {
        name: AcctHolderName.trim(),
        email: email.trim(),
        phone: number.trim(),
        bankAccount: +accountNumber.trim(),
        ifsc: ifsc.trim(),
        address1:address.trim(),
        city: city,
        state: state,
        pincode: +pin
      }
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL + 'withdrawn/add',{
        method : "POST",
          headers: {
            'Authorization': tokenSent,
            'Accept': 'application/json',
            'Content-Type': 'application/json',

          },
        body : JSON.stringify(reqData)
        
      });
      if(response && response.status === 200)
      {
        let jsonData = await response.json();
        if (jsonData.status == 200) {
          toastShow(jsonData?.message, "green");
          props.navigation.navigate('KycVerification');
        }
        else {
          toastShow(jsonData?.message, "red")
        }
      }
      else {
        let jsonData = await response.json();
        toastShow(jsonData?.errors, "red")
      }
    }
    catch (error) {
      toastShow(error, "red")
    }

  }


  const getBankDetials = async () => {
    try {

      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL+'withdrawn/add',{
        method : "GET",
          headers: {
            'Authorization': tokenSent,
            'Accept': 'application/json',
            'Content-Type': 'application/json',

          },
       
        
      })
      if (response && response.status === 200) {
        let jsonData = await response.json();
        if(jsonData.status == 200)
        {
          setPrefilled(jsonData?.data)
        }
        else
        {
          toastShow(jsonData.errors,"red")
        }
       

      }
      else {
        let jsonData = await response.json();
        toastShow(jsonData.errors,"red")
        
      }
    }
    catch (error) {
      toastShow(error, "red")
    }
  }
  return (
    <View style={{ flex: 1 ,backgroundColor:"#FFFFFF" }}>
      <CustomHeader
        img={imageConstant.back}
        navigation={props.navigation}
        headerText={"Account Details"}
      />
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingBottom:20
        }}>

     

        <CustomInput
          placeholder={'Account Holder Name'}
          value={AcctHolderName}
          onChangeText={AcctholderName}
          editable={preFilled == null ? true : false}
          opacity={ preFilled == null ? 1 : 0.5}
          

        />
        <CustomInput
          placeholder={'Email'}
          value={email}
          onChangeText={emailHandler}
          editable={preFilled == null ? true : false}
          opacity={ preFilled == null ? 1 : 0.5}

        />
        <CustomInput
          placeholder={'Phone Number'}
          keyboardType={'numeric'}
          value={number}
          onChangeText={numberHandler}
          editable={preFilled == null ? true : false}
          opacity={ preFilled == null ? 1 : 0.5}

        />

        <CustomInput
        value={accountNumber}
          placeholder={'Account Number'}
          onChangeText={accountNumberHandler}
          keyboardType={'Numeric'}
          editable={preFilled == null ? true : false}
          opacity={ preFilled == null ? 1 : 0.5}
    
        />
        <CustomInput
        value={ifsc}
          placeholder={'IFSC code'}
          onChangeText={ifscHandler}
          editable={preFilled == null ? true : false}
          opacity={ preFilled == null ? 1 : 0.5}
        />
        <CustomInput
        value={address}
          placeholder={'Address'}
          onChangeText={addressHandler}
          editable={preFilled == null ? true : false}
          opacity={ preFilled == null ? 1 : 0.5}
        />
        <CustomInput
        value={city}
          placeholder={'City'}
          onChangeText={cityHandler}
          editable={preFilled == null ? true : false}
          opacity={ preFilled == null ? 1 : 0.5}
        />
        <CustomInput
        value={state}
          placeholder={'State'}
          onChangeText={stateHandler}
          editable={preFilled == null ? true : false}
          opacity={ preFilled == null ? 1 : 0.5}
        />
        <CustomInput
        value={pin}
          placeholder={'Pincode'}
          marginBottom={30}
          keyboardType='numeric'
          onChangeText={pinHandler}
          editable={preFilled == null ? true : false}
          opacity={ preFilled == null ? 1 : 0.5}
        />


        <TouchableOpacity
          onPress={validate}
          style={{
            backgroundColor: "#0049F1",
            width: "90%",
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 30,
            // bottom:10,
            // position:"absolute",
            alignSelf: "center",
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 6,
            },
            shadowOpacity: 0.39,
            shadowRadius: 8.30,
            elevation: 6,
          }}
        >
          <Text style={{
            marginVertical: 15,
            fontSize: 16,
            fontFamily: fontConstant.medium,
            color: "#FFFFFF"
          }}>Add</Text>
        </TouchableOpacity>

      </KeyboardAwareScrollView>


     


      


    </View>
  )
}