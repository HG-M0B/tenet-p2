import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    SafeAreaView,
    Button,
    TouchableOpacity,
    Alert,
    Platform,
    Dimensions,
    ScrollView,
    FlatList,
    BackHandler
  } from 'react-native';
  import React, {Component, useRef,useEffect, useState} from 'react';

  
  import CustomHeader from '../../custom/CustomHeader';
  import { imageConstant } from '../../utils/constant';
  import {fontConstant} from '../../utils/constant';
  import {useSelector} from 'react-redux';
import { useDispatch } from 'react-redux';
import {actions} from '../../redux/reducers'
import toastShow from '../../utils/Toast';
import { baseURL } from "../../utils/URL";
import VideoPlayer from 'react-native-video-player';
import moment from 'moment';
import { useFocusEffect } from '@react-navigation/native';
  const windowWidth = Dimensions.get('window').width;
  
  // const width=Platform.width
  const layout=windowWidth-20



  



const HowToTrade = props => {


  const {route, navigation} = props;
  const [tradeData,setTradeData] =  useState(null)
  const {jwtToken} =  useSelector((state) => state.reducers);

  useEffect(()=>{
    let subscribe = navigation.addListener('focus',()=>{
      getTradeData();
    })
    return()=>subscribe
  },[])
  
  useFocusEffect(
    React.useCallback(() => {
      const backAction = () => {
       props.navigation.goBack();
        return true;
      };
  
      const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
      );
  
      return () => backHandler.remove();
    }, [])
  );



  // *******Api call********

  const getTradeData = async () => {
    try {
      let tokenSent = "Token " + jwtToken;
      const response = await fetch(baseURL + 'static/trade-tutorials/',
        {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': tokenSent,
          }
        })
      if (response && response.status == 200) {
        const res = await response.json();
        console.log("res trade--->",res);
        setTradeData(res)
      }

      else {
        console.log('response not ok===>', response.status)
      }

    }
    catch (error) {
      console.log("error", error)
    }
  }
  return (
    <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
      <CustomHeader
        headerText={'How to Trade'}
        img={imageConstant.back}
        navigation={props.navigation}
      />
      <ScrollView
      showsVerticalScrollIndicator = {false}
      style={{backgroundColor:'#FFFFFF',flexDirection:'column'}}>
      <View style= {{width:'100%',borderBottomWidth:6, borderBottomColor:'#E6E8EC',}}>
        <View style={{width:'100%',marginBottom:10}}>
            <VideoPlayer
              video={{ uri: tradeData?.video?.file}}
              videoWidth={1600}
              videoHeight={900}
              thumbnail={{ uri: 'https://i.picsum.photos/id/866/1600/900.jpg' }}
            />
 
            
        </View>
        <View style={{width:'95%',alignSelf:'center'}}>
          <Text style={{color:'#141416',fontSize:28,fontWeight:'700',fontFamily:fontConstant.regular,}}>{tradeData?.title}</Text>
          <Text style={{color:'#353945',fontSize:12,fontWeight:'400',fontFamily:fontConstant.regular,marginBottom:10}}>{moment(tradeData?.video?.created_at).format('YYYY/MM/DD HH:MM A')}</Text>
        </View>
      </View>

      <View style= {{width:'100%',backgroundColor:'white',height:'100%',paddingBottom:10}}>

      <View style={{width:'92%', alignSelf:'center',marginTop:25}}>
        <Text style={styles.textStyle}>{tradeData?.description}</Text>
      </View>



      <TouchableOpacity
            style={{
              backgroundColor: '#004BEB',
              width: '92%',
              height: 50,
              alignSelf: 'center',
              padding: 9,
              marginTop:30,
              borderRadius: 25,
              borderColor:'rgba(255, 255, 255, 0.4)',
              shadowOpacity:1,
              shadowRadius:3,
              shadowOffset:{height:0.2,width:0.2},
              // paddingBottom:10
            }}          
            onPress={() => {
              navigation.navigate("HomePage")
            }}>
            <Text 
            style={{
              marginTop: 5,
              alignSelf: 'center',
              color: '#FFFFFF',
              fontFamily: fontConstant.bold,
              fontSize: 16,
            }}>
            Tap to play event</Text>
      </TouchableOpacity>

      </View>

      </ScrollView>
      </SafeAreaView>
  )
}

export default HowToTrade

const styles = StyleSheet.create({
    textStyle:{
        color:'#414246',
        fontWeight:'400',
        fontSize:16,
        fontFamily:fontConstant.regular,
        lineHeight:22
      },
      text1Style:{
        color:'#414246',
        fontWeight:'500',
        fontSize:20,
        fontFamily:fontConstant.italic,
        lineHeight:30
      },
})