import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    SafeAreaView,
    Button,
    TouchableOpacity,
    Alert,
    Platform,
    Dimensions,
    ScrollView,
    FlatList,
    Pressable
} from 'react-native';
import React, {Component, useRef, useState} from 'react';

import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import CustomHeader from '../../custom/CustomHeader';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
  
  
  const windowWidth = Dimensions.get('window').width;
  
  const layout=windowWidth-20

const Congrats = props => {
  const {route, navigation} = props;
    setTimeout(() => {
      props.navigation.navigate("Wallet");
    }, 3000);

  return (
    <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
    <View style={{backgroundColor: '#FFFFFF',flex: 1,}}>
        <TouchableOpacity 
        style={{width:'100%',height:400}}
        onPress={() => {
        navigation.navigate("Wallet")
        }}        
        >
        <Image
        style={{height: 400,width: 343,alignSelf: 'center'}}
        source={imageConstant.kycCongrats}
        />

        <View
        style={{width: 343,alignSelf: 'center'}}
        >
            <Text style={{color:'#141416',fontWeight:'700',fontSize:28,textAlign:'center',}}>
            Congratulation!
            </Text>
            <Text style={styles.textStyle}>
            Your account has been verified{'\n'}successfully!
            </Text>
            <Text style={styles.textStyle}>
            Now you can withdraw amount from{'\n'}Tenet Wallet to your bank account
            </Text>
        </View>

        </TouchableOpacity>
    </View>
    </SafeAreaView>
  )
}

export default Congrats

const styles = StyleSheet.create({
    textStyle:{
        color:'#141416',
        fontWeight:'400',
        fontSize:16,
        // lineHeight:'140%',
        textAlign:'center',
        marginTop:20
    }
})