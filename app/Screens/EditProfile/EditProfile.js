import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    SafeAreaView,
    Button,
    TouchableOpacity,
    Alert,
    Platform,
    Dimensions,
    ScrollView,
    FlatList,
    BackHandler
  } from 'react-native';
  import React, {Component, useEffect, useRef, useState} from 'react';
  import LinearGradient from 'react-native-linear-gradient';
  import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
  import { KeyboardAwareScrollView } from '@codler/react-native-keyboard-aware-scroll-view'

  
  import CustomHeader from '../../custom/CustomHeader';
  import { colorConstant, imageConstant } from '../../utils/constant';
  import {fontConstant} from '../../utils/constant';
  import { baseURL } from '../../utils/URL';
  import {useSelector} from 'react-redux';
import { useDispatch } from 'react-redux';
import {actions} from '../../redux/reducers'
import { useFocusEffect } from '@react-navigation/native';
import toastShow from '../../utils/Toast';
import { Height } from '../../dimension/dimension';

  const windowWidth = Dimensions.get('window').width;
  
  // const width=Platform.width
  const layout=windowWidth-20

const EditProfile = props => {
  const [name,setName]=useState('')
  const [email,setEmail]=useState('')
  const [profile,setProfile] =  useState(null)
  const {route, navigation} = props;
  const {jwtToken} =  useSelector((state) => state.reducers);
  useEffect(()=>{
    getProfileImage();
    if(route?.params)
    {
      setName(route.params.data.user_name);
      setEmail(route.params.data.email)
    }
  },[])

 
  const getProfileImage = async () => {
        try {
            let tokenSent = "Token " + jwtToken;
            let response = await fetch(baseURL+"profile/profile-photo/", {
                method: "GET",
                headers: {
                    "Authorization": tokenSent,
                    'Content-Type': 'application/json'
                }
            })

            if (response && response.status == 200) {
                 let jsonData = await response.json();
                 setProfile(jsonData);
            }
        }
        catch (error) {
            console.log(error)

        }
    }


    useFocusEffect(
      React.useCallback(() => {
        const backAction = () => {
          props.navigation.goBack();
          // Alert.alert("Hold on!", "Are you sure you want to exit?", [
          //   { text: "Cancel" },
          //   { text: "Yes", onPress: () => BackHandler.exitApp() }
          // ]);
          return true;
        };
    
        const backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          backAction
        );
    
        return () => backHandler.remove();
      }, [])
    );
  const updateProfileImage = async(reqData)=>{
    try{
        let tokenSent = "Token " + jwtToken;
        let response = await fetch(baseURL+"profile/profile-photo/",{
            method: "POST",
            headers: {
                "Authorization": tokenSent,
                'Content-Type': 'multipart/form-data'
            },
            body : reqData
        })
        if(response &&  response.status == 201)
        {
            
            getProfileImage();
            setTimeout(()=>{
              toastShow("Profile Uploaded","green")
            },100);
        }

    }
    catch(error)
    {
        console.log(error)
    }
}


const validateInputs = ()=>{
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/;
  let regName = /^[A-Za-z0-9 _]*$/;
if(name.length ==  0)
{
  toastShow('Name must be enter','red');
}
else if( name.length != 0  && !regName.test(name))
{
  toastShow('Only alphabets and number are allowed','red');
}

else
{
  if(email?.length > 0)
  {
    if(reg?.test(email))
    {
      handleSaveWithEmail();
    }
    else
    {
      toastShow("Please enter a valid email","red")
    }
    
  }
  else
  {
    handleSaveWithNoEmail();
  }
}
}
  const handleSaveWithEmail = async () => {
    let data = {
      user_name: name,
      email: email,

    }
    let tokenSent = "Token " + jwtToken;
    let response = await fetch(baseURL + 'profile/', {

      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': tokenSent,
      },
      body: JSON.stringify(data)
    })
  
    if (response.status == 200) {
      const res = await response.json()
      if(res &&  res.status == 200)
      {
        navigation.navigate("Drawer");
      }
      else
      {
        console.log("res-->",res)
        toastShow(res?.errors,"red")
      }

    }
    else {
   
      const res = await response.json();
      console.log("res",res?.email?.[0])
      toastShow(res?.email?.[0],"red")

    }

  }
  const handleSaveWithNoEmail = async () => {
    console.log("res-->",response)
    let data = {
      user_name: name
    }
    let tokenSent = "Token " + jwtToken;
    let response = await fetch(baseURL + 'profile/', {

      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': tokenSent,
      },
      body: JSON.stringify(data)
    })
    console.log("res-->",response)
    if (response.status == 200) {
      const res = await response.json()
      if(res &&  res.status == 200)
      {
        navigation.navigate("Drawer");
      }
      else
      {
        toastShow(res?.errors,"red")
      }

    }
    else {
      const res = await response.json()
      toastShow(res?.email?.[0],"red")

    }

  }
  
  const openGallery = () => {
    let options = {
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
    };
    launchImageLibrary(options, (response) => {
        if (response.didCancel) {
            // toastShow('User cancelled ', colorConstant.darkRed)
            console.log('User cancelled image picker');
        } else if (response.error) {
            // toastShow('Something went wrong', colorConstant.darkRed)
            console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);

        } else {
            console.log('response from gallery------------------->', JSON.stringify(response));
            let formDataReq = new FormData();
            let data = {
                uri:response?.assets?.[0]?.uri,
                type:response?.assets?.[0]?.type,
                name:response?.assets?.[0]?.fileName
            }
            formDataReq.append("file",data);
            updateProfileImage(formDataReq);
        }
    })};

  return (
    <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF', }}>
        <CustomHeader
        headerText={'Edit Profile'}
        img={imageConstant.back}
        navigation={props.navigation}
      />
      <View style ={{borderBottomWidth:1, borderBottomColor:'#E6E8EC'}}></View>

      <KeyboardAwareScrollView
      showsVerticalScrollIndicator={false}
      extraScrollHeight={300}
      contentContainerStyle={{
        height:Height*0.90,
      }}
      >
        <View style={{ padding: 40, alignItems: 'center' }}>
          <View>
            <View style={{ borderRadius: 60, height: 120, width: 120, overflow: 'hidden' }}>
              <Image
                style={styles.imageStyle}
                source={profile !== null ? { uri: profile?.file } : imageConstant.Profile_image}
              // source={profile?.profile_photo ?? {uri : profile?.file} : imageConstant.Profile_image}
              />
            </View>
            <TouchableOpacity
              style={{
                position: 'absolute',
                width: 42,
                height: 42,
                bottom: 0,
                right: 0
              }}
              onPress={openGallery}
            >
              <Image
                style={styles.imageStyle}
                source={imageConstant.camera}
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={{ backgroundColor: 'white', marginTop: 30, flexDirection: 'column', width: '90%', alignSelf: 'center' }}>

          <View style={{ borderBottomWidth: 1, borderBottomColor: '#E6E8EC', paddingBottom: 10, paddingTop: 10 }}>
            <Text style={styles.titleStyle}>Name</Text>
            <TextInput
              style={styles.textStyle}
              placeholder="Enter Name"
              placeholderTextColor={'#777E91'}
              onChangeText={(e) => setName(e)}
              value={name}
            />
          </View>

          <View style={{ borderBottomWidth: 1, borderBottomColor: '#E6E8EC', marginTop: 10, paddingBottom: 10, paddingTop: 10, }}>
            <Text style={styles.titleStyle}>Email</Text>
            <TextInput
              style={styles.textStyle}
              placeholder="Enter Email"
              placeholderTextColor={'#777E91'}
              onChangeText={(e) => setEmail(e)}
              value={email}
            />
          </View>

        </View>

        <TouchableOpacity
          style={{
            width: '90%',
            position:"absolute",
            bottom:10,
            alignSelf: 'center',
            justifyContent: "center"

          }}
          onPress={
            validateInputs}>
          <View style={{ width: '98%', alignSelf: 'center' }}>
            <Image
              style={{
                width: "100%",
                height: 60
              }}
              source={imageConstant.saveChabgesButton}
            />
          </View>
        </TouchableOpacity>
      </KeyboardAwareScrollView>

      
      
        

    </SafeAreaView>

  )
}

export default EditProfile

const styles = StyleSheet.create({
    imageStyle: {
        height: '100%',
        width: '100%',
        borderWidth:0.5,
        resizeMode:'contain'
    },
    titleStyle: {
       fontWeight:'500',
       fontSize:16,
       fontFamily:fontConstant.regular,
       color:'#141416',
      //  backgroundColor:"red"

    //    lineHeight:10
    },
    textStyle: {
        fontWeight:'400',
        fontSize:16,
        fontFamily:fontConstant.regular,
        color:'#141416',
        height:Platform.OS === 'ios' ? 30 : null,
        left:Platform.OS === 'ios' ? null : -5,
        // backgroundColor:"pink",
        // marginBottom:10
      //  lineHeight:30
     },
    //  buttonStyle: {
    //     marginTop: 5,
    //     // alignSelf: 'center',
    //     color: '#FFFFFF',
    //     fontFamily: fontConstant.bold,
    //     fontSize: 16,
    //     fontWeight:'700'
    //   },
})