import {
    Text,
    StyleSheet,
    View,
    Image,
    TextInput,
    SafeAreaView,
    Button,
    TouchableOpacity,
    Alert,
    Platform,
    Dimensions,
    ScrollView,
    FlatList,
    BackHandler
  } from 'react-native';
import React, {Component, useEffect, useRef, useState} from 'react';

import CustomHeader from '../../custom/CustomHeader';
import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import { source } from 'deprecated-react-native-prop-types/DeprecatedImagePropType';
import { baseURL } from '../../utils/URL';
import { useFocusEffect } from '@react-navigation/native';
const windowWidth = Dimensions.get('window').width;

// const width=Platform.width
const layout=windowWidth-20
const Notifications = props => {

  const [notificationData, setNotificationData]=useState([])
  const {route, navigation} = props;
  useEffect(()=>{
  getNotificationData()
  },[])


  useFocusEffect(
    React.useCallback(() => {
      const backAction = () => {
       props.navigation.goBack();
        return true;
      };
  
      const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
      );
  
      return () => backHandler.remove();
    }, [])
  );
  const getNotificationData = async() => {
    const response = await fetch(baseURL+'notifications/')
    if(response.status==200){
      const res=response.json();
      setNotificationData(res)
      console.log(notificationData)
    }
    else{
      console.log('status not ok ==>',response.status)
    }
  }

  const notiArray = [
    {
        name :'Dont go to Rome for Euro tie, England fans warned',
        time:'30/06/21 - 6:30 AM',
        status:'read',
        img : require('../../assets/images/settingsNoti.png')
    },
    {
        name : 'Dont go to Rome for Euro tie, England fans warned',
        time:'30/06/21 - 6:30 AM',
        status:'unread',
        img : require('../../assets/images/walletNoti.png')
    },
    {
        name : 'New update is available. Upgrade now for the best experience',
        time:'30/06/21 - 6:30 AM',
        status:'read',
        img : require('../../assets/images/userNoti.png')
    },
    {
      name : 'New update is available. Upgrade now for the best experience',
      time:'30/06/21 - 6:30 AM',
      status:'unread',
      img : require('../../assets/images/noteNoti.png')
    },
    {
      name : 'New update is available. Upgrade now for the best experience',
      time:'30/06/21 - 6:30 AM',
      status:'read',
      img : require('../../assets/images/settingsNoti.png')
    },
  
]
    return(
        <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
        <View style={{backgroundColor: '#FFFFFF',flex: 1,}}>
        <CustomHeader
          headerText={'Notifications'}
          img={imageConstant.back}
          navigation={props.navigation}
        />
        <View style ={{borderBottomWidth:1, borderBottomColor:'#E6E8EC'}}></View>
        <View style={{ height:'100%',width:'100%', backgroundColor:'white'}}>
        <FlatList 
          // horizontal
          contentContainerStyle={{
            paddingBottom:110
          }}
          showsHorizontalScrollIndicator = {false}
          data ={notiArray} 
          renderItem = {(d) => {
              return <View style={{flexDirection:'row',backgroundColor:d.item.status== 'unread'?'rgba(0, 73, 241, 0.1)':"#fff",padding:10,alignItems:'center'}}>
                <View style = {{width:'15%',}}>
                {/* <Image source={imageConstant.logo} resizeMode='contain' 
                  style={[styles.Img]}
                /> */}
                <Image source={d.item.img} resizeMode='contain' 
                  style={[styles.Img]}
                />
                </View>
                <View style={{flexDirection:'column',width:"85%",paddingLeft:5}}>
                <Text style={styles.dateStyle}>{d.item.time} </Text>
                <Text style={styles.textStyle}>{d.item.name}</Text>
                </View>
              </View>
          }}
          />
        </View>
        </View>
        </SafeAreaView>
    )
}

export default Notifications

const styles = StyleSheet.create({
  textStyle: {
    marginTop:5,
    color: "#141416",
    fontSize:15,
    fontWeight:'400',
    fontFamily:fontConstant.regular
  },
  dateStyle:{
    color: "#353945",
    fontSize:12,
    fontWeight:'400',
    fontFamily:fontConstant.regular
  },
  viewStyle : {
    flexDirection:'row'
  },
  Img:{
    width:48,
    height:48,
  },
})