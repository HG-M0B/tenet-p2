import { Text, View,ImageBackground,Image,TouchableOpacity,StyleSheet,SafeAreaView,Dimensions,Platform, Alert, TextInput, ScrollView} from 'react-native'
import React, { Component, useEffect, useState } from 'react'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { imageConstant, ProgessFinish } from '../../utils/constant';
import { Height } from '../../dimension/dimension';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';
import { Switch } from 'react-native';
import {useSelector} from 'react-redux';
import { baseURL } from '../../utils/URL';
import toastShow from '../../utils/Toast';
import { actions } from '../../redux/reducers';
import { useDispatch } from 'react-redux';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const layout=windowWidth-20
const screenHeight = windowHeight

const UserConsent = (props) => {
    const [date,setDate]=useState(new Date(new Date().getTime()-365*18*24*60*60*1000-24*60*60*1000*5))
    const [open, setOpen] = useState(false)
    const [token,setToken] = useState('')
    const [isEnabled, setIsEnabled] = useState(false);
    const [email,setEmail] =  useState('');
    const [referCode,setReferCode] = useState('');
    const [number,setNumber] = useState('');
    const {jwtToken} = useSelector((state) => state.reducers);
    const [profileData,setProfileData]=useState("")
    const [pincode,setPinCode]=useState('201301')
    const dispatch =  useDispatch();
    useEffect(()=>{
      if(props?.route)
      {
        setProfileData(props?.route?.params?.username);
        setToken(props?.route?.params?.uidToken);
        setEmail(props?.route?.params?.email);
        setNumber("+91"+props?.route?.params?.num);
        setReferCode(props?.route?.params?.referCode);
      }
    },[])
    let day=moment(date).date()
    let month=moment(date).month() + 1
    let Year=moment(date).year()
    const dates=`${Year}-${month}-${day}`

 const onPlay=()=>{
    (profileData == "")
    ? toastShow("Enter name","red")
    : (pincode.length !== 6 )
    ? toastShow("Please Enter correct Pincode","red")
    : !isEnabled
    ? toastShow("Please accept the declaration","red")
    : userRegister()  
 }


 const userRegister = async (props) => {
  try{
    let fullDataBody = {
      user_name:profileData,
      email: email?.toLowerCase()?.trim(),
      phone_number: number,
      firebase_token: token,
      referral_code_used_at_registration:referCode,
      pincode:pincode,
      date_of_birth:dates,
      has_given_consent:isEnabled
    }
    let noEmailBody = {
      user_name:profileData,
      phone_number: number,
      firebase_token: token,
      referral_code_used_at_registration:referCode,
      pincode:pincode,
      date_of_birth:dates,
      has_given_consent:isEnabled
  
    }
    let noReferBody = {
      user_name:profileData,
      email: email?.toLowerCase()?.trim(),
      phone_number: number,
      firebase_token: token,
      pincode:pincode,
      date_of_birth:dates,
      has_given_consent:isEnabled
  
    }
    let noReferEmailBody = {
      user_name:profileData,
      phone_number: number,
      firebase_token: token,
      pincode:pincode,
      date_of_birth:dates,
      has_given_consent:isEnabled
  
    }
    let finalBody ;
    if(email !== "" && referCode !== "")
    {
      finalBody = fullDataBody;
    }
    if(email == "")
    {
      finalBody = noEmailBody;
    }
    if(referCode == "")
    {
      finalBody = noReferBody;
    }
    if(email == "" && referCode == "")
    {
      finalBody = noReferEmailBody
    }


    let response = await fetch(baseURL + 'auth/register/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'accept': 'application/json'
      },
      body: JSON.stringify(finalBody)
    }
    )
    if (response.status == 200) {
      let res=await response.json();
      dispatch(actions.setJwtToken(res?.token))
      dispatch(actions.setIntroStatus("main"))
      
    }
    else {
      let res = await response.json();
      { res.errors.email?.[0] == undefined  ? <></> :  toastShow(res.errors.email?.[0],"red")}
      { res.errors.non_field_errors?.[0] == undefined  ? <></> :  toastShow(res.errors.non_field_errors?.[0],"red")}
     
    }

  }
  catch(error){
    toastShow(error, "red")
  }
}


      const setUserConsentData=async(token)=>{
        let tokenSent = "Token "+token;
        let data={
            "pincode": pincode,
            "date_of_birth":dates,
            "has_given_consent":isEnabled
          
        }
        let response=await fetch(baseURL+'profile/consent/',{
            method:'POST',
            headers:{
                'Content-Type': 'application/json',
        'Accept':'application/json',
      'Authorization':tokenSent,
            },
            body:JSON.stringify(data)
        })
        if(response.status==200 ){
            let res=await response.json();
          dispatch(actions.setJwtToken(token))
          dispatch(actions.setIntroStatus("main"))
        }
        else{
        }
      }
    
  return (
    <View style={{flex:1}}>
        <View style={styles.toglle5View}>
          <Image source={imageConstant.logo2} style={{ left: '15%' }} />
          <Text style={styles.titleText}>User Consent</Text>
        </View>

        <ScrollView contentContainerStyle={{
          paddingBottom:20
        }}>
          <Text style={styles.topText}>Tenet is a Platform where you have to do research and should have knowledge,
            before trading. We in no form encourage or indulge in online betting.{'\n'}{'\n'}
            Before using Tenet you acknowledge that you are above the age of 18 Years & legally
            competent to access this platform</Text>
          <Text style={{ color: '#323232', fontSize: 12, marginTop: 5, marginHorizontal: 20, marginTop: 10 }}>Please fill details as in Government Documents</Text>
          <Text style={styles.labelText}>Your name</Text>
          <TextInput
            //   editable={false}
            placeholder='Enter your name'
            placeholderTextColor={"black"}
            style={styles.textinput}
            value={profileData}
          />
          <Text style={styles.labelText}>Date of Birth</Text>
          <TouchableOpacity activeOpacity={1} onPress={() => setOpen(true)} style={styles.calText}>
            <Image source={imageConstant.Calen} />

            <Text style={{ paddingLeft: 10, color: '#000000' }}>{day}-{month}-{Year}</Text>
          </TouchableOpacity>
          <DatePicker
            modal
            open={open}
            date={date}
            maximumDate={date}
            mode='date'
            onConfirm={(date) => {
              setOpen(false)
              setDate(date)
            }}
            onCancel={() => {
              setOpen(false)
            }}

          />
          {/* <Text style={styles.labelText}>Pincode</Text>
          <TextInput
            placeholder='6 Digit Pincode'
            style={styles.textinput}
            maxLength={6}
            keyboardType='numeric'
            onChangeText={(e) => setPinCode(e)}
          /> */}
          <View style={{ flexDirection: 'row', marginTop: 25 }}>
            <Switch
              trackColor={{ false: "#D6E3F3", true: "#81b0ff" }}
              thumbColor={isEnabled ? "blue" : "#f4f3f4"}
              onValueChange={() => setIsEnabled(!isEnabled)}
              value={isEnabled}

              style={{ alignSelf: 'flex-start', left: 20, }}
            />
            <Text style={{ textAlign: 'justify', width: 270, left: 35, fontSize: 12,color:"black" }}>I certify that I am over 18 years old, & legally competent, have read and understood information provided here.</Text>
          </View>


          <Text style={{ marginTop: 5, width:"90%",alignSelf:"center", fontSize: 12, color: '#FC0330', fontWeight: '500', }}>Please accept the declaration to proceed</Text>
          <TouchableOpacity
            style={styles.TouchStyle}
            onPress={onPlay}
          >
            <Image
              style={styles.ImgStyle}
              resizeMode="contain"
              source={ProgessFinish}
            />
          </TouchableOpacity>


          <View style={styles.lastView}>
            <Text style={{
              color:"#323232"
            }}>Made with love in India   <Image source={imageConstant.flag} /> </Text>
          </View>
        </ScrollView>
    </View>
  );
}

export default UserConsent;

const styles = StyleSheet.create({
    ImageStyle:{ 
      flex:1,
      height:Height * 0.78,
      width: windowHeight * 0.53 ,
      // backgroundColor:"green",
      
      alignSelf:'center',
      // alignItems:'center'
      // backgroundColor:"green"
      marginTop:'2%'
    },

    TouchStyle:{
        height:wp('15%'),
        width:wp('15%'),
        alignSelf:'center',
        marginTop:20,
        borderRadius:30,
        backgroundColor:"white",
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 3,
      },
      shadowOpacity: 0.29,
      shadowRadius: 4.65,

      elevation: 7,
      
    },
    ImgStyle:{
        height:wp('15%'),
        width:wp('15%'),
      },
      toglle5View:{
        height:77,
        backgroundColor:'#024BEB',
        flexDirection:'row',
        alignItems:'center',
      },
      titleText:{
        fontSize:20,
        color:'#FFFFFF',
        fontWeight:'500',
        position:'absolute',
        left:'35%',
        width:windowWidth-30
      },
      labelText:{
        left:20,
        marginTop:10,
        fontSize:13,
        color:"#111111"
      },
      textinput:{
        backgroundColor:'#D6E3F3',
        marginHorizontal:20,
        borderRadius:4,
        marginTop:10,
        paddingLeft:25,
        shadowColor:'red',
        shadowOpacity:10,
        shadowRadius:10,
        elevation:-5,
        position:'relative',
        shadowOffset:{width:-5,height:5},
        color:"#323232"
      },
      topText:{
        fontSize:14,
        marginHorizontal:20,
        color:"#323232",
        textAlign:'justify',
        marginTop:20
      },
      calText:{
        backgroundColor:'#D6E3F3',
        marginHorizontal:20,
        borderRadius:4,
        flexDirection:'row',
        height:50,
        alignItems:'center',
        marginTop:10,
        paddingLeft:20
      },
      lastView:{
        width:"90%",
        alignSelf:"center",
        alignItems:"center",
        marginTop:20
      }
})