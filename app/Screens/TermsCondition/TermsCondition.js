import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView
} from 'react-native';
import React, {Component, useEffect, useRef, useState} from 'react';


import CustomHeader from '../../custom/CustomHeader';
import { imageConstant } from '../../utils/constant';
import {fontConstant} from '../../utils/constant';
import { baseURL } from '../../utils/URL';

const windowWidth = Dimensions.get('window').width;

// const width=Platform.width
const layout=windowWidth-20

const TermsConds = props => {
  const [termConData, setTermConData]=useState([])
const {route, navigation} = props;

useEffect(()=>{
  getTermConditionData()
},[])
  const getTermConditionData = async () => {
    const response = await fetch(baseURL + 'static/terms-n-conditions/')
    if (response.status == 200) {
      const res = await response.json()
      setTermConData(res)
    }
    else {
      console.log('status not ok', response.status)
    }
  }

  return(
    <SafeAreaView style={{flex: 1,backgroundColor:'#FFFFFF' }}>
    <CustomHeader
      headerText={'Terms & Conditions'}
      img={imageConstant.back}
      navigation={props.navigation}
    />
    <ScrollView 
    showsVerticalScrollIndicator={false}
    style={{backgroundColor:'#FFFFFF',flexDirection:'column'}}>
    <View style= {{width:'100%',borderBottomWidth:6, borderBottomColor:'#E6E8EC',}}>
    </View>

        <View style={{
          width: '90%',
          alignSelf: 'center',
          marginTop: 25,
          borderLeftWidth: 4,
          borderLeftColor: '#C2F44E',
          paddingLeft: 15,
          marginBottom:10,
        }}>
          <Text style={styles.text1Style}>{termConData.terms_and_conditions}</Text>
        </View>
    
    </ScrollView>
    </SafeAreaView>
  )
}

export default TermsConds

const styles = StyleSheet.create({
  textStyle:{
    color:'#414246',
    fontWeight:'400',
    fontSize:16,
    fontFamily:fontConstant.regular,
    lineHeight:22,
   
  },
  text1Style:{
    color:'#414246',
    width:"99%",
    // backgroundColor:"pink",
    // fontWeight:'500',
    fontSize:20,
    fontFamily:fontConstant.italic,
    lineHeight:25,
    // textAlign:'justify'
    
  },

})