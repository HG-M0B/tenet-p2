import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  ScrollView,
  FlatList,
  Pressable,
  BackHandler,
  Modal
} from "react-native";
import React, { Component, useEffect, useRef, useState } from "react";

import CustomHeader from "../../custom/CustomHeader";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { baseURL } from "../../utils/URL";
import {useSelector,useDispatch} from 'react-redux'
import moment from 'moment';
import { sub } from "react-native-reanimated";
import { Height,Width } from "../../dimension/dimension";
import SwipeButton from 'rn-swipe-button';
import toastShow from '../../utils/Toast';
import {actions} from '../../redux/reducers'
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import Loader from '../../utils/loader';
import { useScrollToTop } from '@react-navigation/native';
import { colorConstant, imageConstant ,fontConstant} from '../../utils/constant';
const windowWidth = Dimensions.get("window").width;

const layout = windowWidth - 20;

const Search = (props) => {
  const { route, navigation } = props;
  const [name, setName] = useState("");
  const [serachData, setsearchData]=useState(null)
  const [search,setSearch]=useState('')
  const [trendingEventData, setTrendingEventData]=useState(null)
  const [catogData,setCatogData]=useState([]);
  const [quantity,setQuantity]=useState([5]);
  const [price,setPrice]=useState([7]);
  const [eventDetails,setEventDetails] = useState(null);
  const [detailsModal, setDetailsModal] = useState(false);
  const [optionChoose,setOptionChoose] = useState("");
  const [orderData,setOrderData] =  useState(null);
  const [mapsuggestedData,setMapSuggestData] = useState([]);
  const [suggestedQuantity,setSuggestedQuantity] = useState(0);
  const [loader,setLoader] = useState(false)
  const [sliderShow,setSliderShow] =  useState(false);
  const {jwtToken,walletBalance} =  useSelector((state) => state.reducers);
  const reff = React.useRef(null);
  const dispatch = useDispatch();
  useEffect(()=>{
    let subs = props.navigation.addListener('focus', () => {
      getCatogData();
      setsearchData(null);
      setSearch("");
      getEventData();
      callMainApi();
   
    });
    const backAction = () =>{
      navigation.goBack();
      return true;
    }
    let backhandler =  BackHandler.addEventListener("hardwareBackPress",backAction)
    return () => {
      sub,
      backhandler.remove();
    };
  },[])

  useScrollToTop(reff)
    useEffect(() => {
    if (orderData !== null) {
      suggestData()
    }
  }, [orderData])



    
  const callMainApi = async () => {
    try {
      let response = await fetch(baseURL + `maintenance/`);
      if (response && response.status == 200) {
        let jsonData = await response.json();
        if (jsonData?.maintenance) {
          dispatch(actions.setIntroStatus("check"));
        }
      }
    }
    catch (error) {
      console.log("error-->", error)
    }
  }
 

  const getCategoriesSearch = async (text) => {
    try {
      let tokenSent = "Token " + jwtToken;
      const response = await fetch(baseURL + `category/get-categories/?keyword=${text}`, {
        headers: {
          'Authorization': tokenSent,
          'Accept': 'application/json',
        }
      })
      if (response.status == 200) {
        const res = await response.json()
        setsearchData(res?.results)

      }
      if (response.status == 401) {
        const jsonData = await response.json()
        dispatch(actions.setIntroStatus("login"));
        toastShow(jsonData.detail, "red");
    
    }
    }
    catch (error) {
      toastShow(error.message, "red")
    }

  }

  const getCatogData = async () => {
    try {
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL + `category/get-categories/`, {
        headers: {
          'Authorization': tokenSent,
          'Content-Type': 'application/json',

        }
      })
      if (response.status == 200) {
        let res = await response.json()
        setCatogData(res?.results);
      }
      
    }
    catch (error) {
      toastShow(error.message, "red")
    }

  }




  const getEventData = async () => {
    try {
      setLoader(true)
      let tokenSent = "Token " + jwtToken;
      const response = await fetch(baseURL + 'events/get-events/',
        {
          headers: {
            'Authorization': tokenSent,
            'Accept': 'application/json',
            'Content-Type': 'application/json',

          }
        })
      if (response && response.status == 200) {
        const res = await response.json();
        setLoader(false);
        setTrendingEventData(res?.results)
      }
      else {
        const res = await response.json();
        setLoader(false);
        toastShow(res.detail, "red")
      }
    }
    catch (error) {
      setLoader(false);
      toastShow(error.message,"red")
    }
  }


  const getEventDataByID = async (id,otp,money) => {
    getTradeOrderCall(id)
    setOptionChoose(otp);
    setPrice([money]);
    setSliderShow(false);
    try {
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL + `events/get-events/${id}`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': tokenSent
        }
      })
      if (response.status == 200) {
        let res = await response.json();
        setEventDetails(res);
        setDetailsModal(!detailsModal);

        
      }
    }
    catch (error) {
      toastShow(error.message,"red")
      setLoader(false)
    }

  }

  const tradeCall =  async (id)=>{
    try{
      let reqData = {
        amount:price?.[0],
        quantity:quantity?.[0],
        selected_option:optionChoose,
        binary_event: id
      }
      let tokenSent = "Token " + jwtToken;
      let response = await fetch(baseURL +`orders/`, {
        method:"POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': tokenSent
        },
        body:JSON.stringify(reqData)
      })
      if( response && response.status == 200)
      {
        let jsonData = await response.json();
        
        if(jsonData?.status == 200)
        {
          setDetailsModal(!detailsModal);
          toastShow(jsonData?.data,"red");
        }
        
        }

    if(response && response.status == 201) 
      {
        setDetailsModal(!detailsModal);
        navigation.navigate("TradeSuccess",{
          data : price * quantity
        });
      } 

    }
    catch(error){
      toastShow(error.message,"red")
    }

  }

  
  const checkBalance =(id)=>{
    if(price*quantity > walletBalance)
    {
  
      toastShow("You don't have enough funds to proceed","red");
    }
    else
    {
      tradeCall(id)
    }
  }
  
  const handleSearch=(text)=>{
    setSearch(text);
    getCategoriesSearch(text);
  }

  const emptyListComponent=()=>{
    return(
      <View style={{
        width:"90%",
        alignSelf:"center",
        alignItems:"center",
        marginVertical:Height*0.10
      }}>
      <Text style={{
        fontSize:20,
        fontFamily:fontConstant.medium,
        color:"rgba(1,1,1,0.35)"
      }}>No Category Found</Text>
      </View>
    )
  }

    //****************Matches function****/
    const getTradeOrderCall = async(id)=>{
      try{
        let tokenSent = "Token " + jwtToken;
        let response = await fetch(baseURL +`orders/?event_id=${id}`, {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': tokenSent
          }
        });
        
        if(response.status === 200)
        {
          let jsonData = await response.json();
          setOrderData(jsonData);
        }
  
      }
      catch(error){
        toastShow(error.message,"red")
      }
    }
  
  
    const suggestData = () => {
  
      let tempObj = { "yes": [], "no": [] }
      let tempData = orderData;
      let obj = {};
      // **********Group by obj**********
      for (let i = 0; i < tempData?.length; i++) {
  
        if (tempData?.[i]?.selected_option == "yes") {
          tempObj["yes"] = [...tempObj["yes"], tempData?.[i]]
        }
        if (tempData?.[i]?.selected_option == "no") {
          tempObj["no"] = [...tempObj["no"], tempData?.[i]]
        }
  
      }
  
      let yesArr = tempObj?.["yes"];
      let noArr = tempObj?.["no"]
  
      yesArr?.forEach((item, index) => {
        yesArr[index] = { amount: 10 - item?.amount, qty: item?.quantity - item?.matched_quantity, option: "No" }
      })
      noArr?.forEach((item, index) => {
        noArr[index] = { amount: 10 - item?.amount, qty: item?.quantity - item?.matched_quantity, option: "Yes" }
      })
      let yesFinalArray = [];
      let noFinalArray = [];
  
      let sumYesArray = yesArr?.reduce(function (acc, curr) {
        let findIndex = acc.findIndex(item => item.amount === curr.amount);
  
        if (findIndex === -1) {
          acc.push(curr)
        } else {
  
          acc[findIndex].qty += curr.qty
        }
        return acc;
      }, [])
  
      let sumNoArray = noArr?.reduce(function (acc, curr) {
        let findIndex = acc.findIndex(item => item.amount === curr.amount);
  
        if (findIndex === -1) {
          acc.push(curr)
        } else {
  
          acc[findIndex].qty += curr.qty
        }
        return acc;
      }, [])
  
  
      let finalArray = [...sumYesArray, ...sumNoArray];
      setMapSuggestData(finalArray);
    }
  
    useEffect(()=>{
      gettingSingleSuggestion();
      },[price,mapsuggestedData]);
      
      const gettingSingleSuggestion=()=>{
      let tempData = mapsuggestedData;
      let qty = tempData?.filter((item,index) => item?.amount === price[0]);
      setSuggestedQuantity(qty?.[0]);
      }

      const renderCategories=({item,index})=>{
            return (
                <TouchableOpacity
                activeOpacity={0.8}
                  onPress={() => {
                    navigation.navigate("Category", {
                      name:item.name,
                      id :item.id
                    });
                  }}
                  style={{ 
                    alignItems:"center",
                    borderWidth:1,
                    borderColor:colorConstant.lightGray,
                    borderRadius:10,
                    minWidth:"30%",
                    padding:7,
                    paddingVertical:13,
                    margin:7

                   }}
                >
                  <View
                    style={{
                      borderWidth: 0,
                      borderRadius: 35,
                      overflow: "hidden",
                    }}
                  >
                    <Image
                      source={item.photo.file ? {uri:item?.photo?.file} : imageConstant.EventCat}
                      resizeMode="cover"
                      style={[styles.Img]}
                    />
                  </View>
                    <Text style={{
                      fontSize:13,
                      fontFamily:fontConstant.regular,
                      color:"#141416",
                      marginTop:6,
                      width:"55%",
                      textAlign:"center"
                    }}>
                      {item.name}
                      </Text>
                </TouchableOpacity>
            )
      }


  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
      <View style={{ flex: 1, backgroundColor: "#FFFFFF" }}>
        {loader && <Loader data={loader}/> }
        <View
          style={{
            backgroundColor: "#FFFFFF",
            width: "99%",
            height: 50,
            alignSelf: "center",
            flexDirection: "row",
            marginTop: Platform.OS === "ios" ? null : 10,
          }}
        >

          <TouchableOpacity
            style={{
              marginVertical: 4,
              position: "absolute",
              height: 36,
              width: 36,
              left: 15,
              zIndex: 1

            }}
            onPress={() => {
              navigation.toggleDrawer();
            }}
          >
            <Image
              source={imageConstant.options}
              style={[styles.optionImageStyle]}
            />
          </TouchableOpacity>


          <Text
            style={{
              fontWeight: "700",
              fontSize: 16,
              padding: 11,
              textAlign: "center",
              width: "100%",
              color: "#141416",
            }}
          >
            Search
          </Text>
        </View>

        <ScrollView
        ref={reff}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 10,
          }}
          style={{ width: layout - 5, alignSelf: "center" }}
        >
          <View
            style={{
              marginTop: 20,
              flexDirection: "row",
              justifyContent: "space-between",
              borderBottomWidth: 1,
              borderBottomColor: "#E6E8EC",
            }}
          >
            <TextInput
              style={styles.input1}
              value={search}
              placeholder="Search"
              placeholderTextColor={"#777E91"}
              onChangeText={(text)=>handleSearch(text)}
            />

            <TouchableOpacity
              // onPress={() => {
              //   navigation.navigate("Notifications")
              //   }}
              style={{
                height: 24,
                width: 24,
                marginTop: 5,
                marginBottom: Platform.OS === "ios" ? null : -10,
              }}
            >
              <Image
                source={imageConstant.searchMain}
                style={[styles.searchImageStyle]}
              />
            </TouchableOpacity>
          </View>

          <View style={{ marginTop: 20, flexDirection: "column" }}>
            <View
              style={{ width: "100%", height: 42, justifyContent: "center" }}
            >
          
            {  serachData?.length == 0 || catogData?.length > 0 && ( <Text style={styles.catStyle}>Our Categories</Text>)}
            </View>

            <View>
              <FlatList
                // horizontal
                numColumns={3}
                ListEmptyComponent={emptyListComponent}
                showsHorizontalScrollIndicator={false}
                data={ serachData ?? catogData}
                keyExtractor={item=>item.id}
                renderItem={renderCategories}
                contentContainerStyle={{
                  // alignItems:"center",
                  paddingRight:'1.5%',
                  width:"100%"

                }}
              />
            </View>
          </View>

          {
            trendingEventData?.length > 0 &&
            (<View style={{
              width: "100%",
              alignSelf: "center",
              justifyContent: "center",
              marginTop: 15,
            }}
            >
              <Text style={styles.catStyle}>Events</Text>
            </View>)
          }


          {
            trendingEventData?.length > 0 ? 
            trendingEventData?.map((item, index) => {
              let tempArray = item?.event_history?.volume_history;
              let sum = 0
              for (let i = 0; i < tempArray?.length; i++) {
                sum = tempArray?.[i]?.value + sum
              }
  
              return (
                <View >

                  <TouchableOpacity

                    activeOpacity={1}
                    onPress={() => {
                      navigation.navigate("EventDetails", { id: item?.id });
                    }}
                    style={{
                      marginTop: 20,
                      borderRadius: 10,
                      marginHorizontal: 15,
                      backgroundColor: "#FFFFFF",
                      width: "94%",
                      alignSelf: "center",
                      shadowColor: "#000",
                      shadowOffset: {
                        width: 0,
                        height: 2,
                      },
                      shadowOpacity: 0.25,
                      shadowRadius: 3.84,
                      elevation: 5,
                    }}
                  >

                    <>


                      <View
                        style={{
                          flexDirection: "row",
                          width: "90%",
                          alignSelf: "center",
                          marginTop: 20,
                        }}
                      >
                        <View
                          style={{
                            height: 70,
                            width: 70,
                            borderRadius: 35,
                            shadowColor: "#000",
                            shadowOffset: {
                              width: 0,
                              height: 4,
                            },
                            shadowOpacity: 0.80,
                            shadowRadius: 10,
                            elevation: 25,
                            overflow: "hidden"
                          }}
                        >
                          <Image
                            resizeMode="contain"
                            source={{ uri: item?.photo?.file }}
                            style={{ height: 70, width: 70, borderRadius: wp("15%") }}
                          />
                        </View>


                        <Text
                          style={{
                            width: "70%",
                            fontWeight: "900",
                            marginLeft: 15,
                            fontSize: 15,
                            lineHeight: 20,
                            textAlign: "left",
                            color: "#1A1A1A",
                          }}
                        >
                          {item?.title}
                        </Text>




                        {/* ******Bookmark********* */}
                        {/* <TouchableOpacity
                        // style={{position:'absolute',}}
                        style={{ height: 22, width: 22 }}
                      >
                        <Image
                          source={imageConstant.save}
                          style={{
                            height: 22,
                            width: 22,
                            resizeMode: "contain",
                            position:'absolute',
                            right:-25
                          }}
                        />
                      </TouchableOpacity> */}


                        {/* </View> */}
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          marginTop: 15,
                          width: "65%",
                          height: 40,
                          marginLeft: 85,
                          justifyContent: "space-between",
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            getEventDataByID(item?.id, "yes", item?.price_yes)
                            // navigation.navigate("EventDetails1", { id: trendingEventData?.id, opt: "yes", price: trendingEventData?.price_yes });
                          }}
                          style={{
                            backgroundColor: "#0049F1",
                            width: 100,
                            height: 34.5,
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: 40,
                            shadowColor: "black",
                            shadowOpacity: 0.6,
                            shadowRadius: 5,
                            shadowOffset: { height: 0.5, width: 0.2 },
                            elevation: Platform.OS === 'ios' ? null : 10,
                          }}
                        >
                          <Text
                            style={{
                              color: "#FFFFFF",
                              fontWeight: "700",
                              fontSize: 12,
                            }}
                          >
                            YES ₹{item?.price_yes}
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => {
                            getEventDataByID(item?.id, "no", item?.price_no)
                            // navigation.navigate("EventDetails1", { id: trendingEventData?.id, opt: "no", price: trendingEventData?.price_no });
                          }}
                          style={{
                            backgroundColor: "#FF2567",
                            width: 100,
                            height: 34.5,
                            marginLeft: 10,
                            justifyContent: "center",
                            alignItems: "center",
                            borderRadius: 40,
                            shadowColor: "black",
                            shadowOpacity: 0.6,
                            shadowRadius: 3,
                            shadowOffset: { height: 0.5, width: 0.2 },
                            elevation: Platform.OS === 'ios' ? null : 10,
                          }}
                        >
                          <Text
                            style={{
                              color: "#FFFFFF",
                              fontWeight: "700",
                              fontSize: 12,
                            }}
                          >
                            NO ₹{item?.price_no}
                          </Text>
                        </TouchableOpacity>
                      </View>


                      <View
                        style={{
                          width: "100%",
                          // flexDirection:'row',
                          // alignItems : 'center',
                          height: 50,
                          backgroundColor: "#F8F8F8",
                          marginTop: 20,
                          borderBottomEndRadius: 10,
                          borderBottomStartRadius: 10,
                        }}
                      >
                        <View
                          style={{
                            paddingTop: 10,
                            backgroundColor: "#F8F8F8",
                            width: "90%",
                            height: 50,
                            alignSelf: "center",
                            flexDirection: "row",
                            justifyContent: "space-between",
                          }}
                        >
                          <View>
                            <View
                              style={{
                                justifyContent: "center",
                                marginRight: 100,
                                flexDirection: "row",
                              }}
                            >
                              <Image
                                style={{ width: 18, height: 18 }}
                                source={imageConstant.timer}
                              />
                              <Text
                                style={{
                                  color: "#414246",
                                  fontWeight: "600",
                                  fontSize: 12,
                                  marginLeft: 8,
                                }}
                              >
                                {
                                  moment(item?.end_datetime).fromNow()

                                }
                              </Text>
                            </View>
                            <Text
                              style={{
                                color: "#414246",
                                fontWeight: "500",
                                fontSize: 9,
                                marginTop: 2,
                              }}
                            >
                              Ends in
                            </Text>
                          </View>

                          <View>
                            <View
                              style={{
                                marginLeft: 15,
                                justifyContent: "center",
                                flexDirection: "row",
                              }}
                            >
                              <Image
                                style={{ width: 18, height: 18 }}
                                source={imageConstant.volume}
                              />
                              <Text
                                style={{
                                  color: "#414246",
                                  fontWeight: "500",
                                  fontSize: 14,
                                  marginLeft: 8,
                                }}
                              >
                                ₹ {sum}
                              </Text>
                            </View>
                            <Text
                              style={{
                                color: "#414246",
                                fontWeight: "500",
                                fontSize: 9,
                                marginLeft: 12,
                                marginTop: 2,
                              }}
                            >
                              Volume
                            </Text>
                          </View>
                        </View>
                      </View>

                    </>

                  </TouchableOpacity>

                </View>
              )


            })

            :
            <View style={{
              alignSelf:"center",
              marginTop:40
            }}>
              <Text style={{
                fontSize:16,
                fontFamily:fontConstant.bold,
                color:colorConstant.blue
              }}>NO EVENTS</Text>
            </View>

          }

          
        </ScrollView>
      </View>

     

      <Modal
        animationType="slide"
        transparent={true}
        visible={detailsModal}
        onRequestClose={()=>{
          setDetailsModal(!detailsModal);
        }}
      >

        <View
          style={styles.centeredView2}>
          <TouchableOpacity
            style={{
              padding: 15,
              right: 10,
              top: Height - (Height * 0.80) - 75,
              position: "absolute"
            }}
            onPress={() => setDetailsModal(!detailsModal)}>

            <Image
              source={imageConstant.cancelImg}
              style={{
                width: 25,
                height: 25,

              }} />
          </TouchableOpacity>

          <View style={styles.modalView2}>

            <View style={{
              flexDirection: "row",
              justifyContent: "space-between",
              height: 60

            }}>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => setOptionChoose("yes")}
                style={{
                  width: "50%",
                  backgroundColor: optionChoose == "yes" ? colorConstant.bbg : colorConstant.lightGray,
                  justifyContent: "center",
                  alignItems: "center",
                  borderBottomWidth: 1,
                  borderBottomColor: "#D3D3D3",
                  borderRightWidth: 1,
                  borderRightColor: "#D3D3D3",
                  borderTopLeftRadius: 20
                }}>
                <Text style={{
                  fontSize: 18,
                  fontFamily: fontConstant.bold,
                  color: optionChoose == "yes" ? colorConstant.white : colorConstant.blackText,



                }}>YES <Text style={{
                  fontSize: 14,
                  fontFamily: fontConstant.medium,
                  color: optionChoose == "yes" ? colorConstant.white : colorConstant.blackText
                }}> ₹{eventDetails?.price_yes}</Text></Text>
              </TouchableOpacity>


              <TouchableOpacity
                onPress={() => setOptionChoose("no")}
                activeOpacity={1}
                style={{
                  width: "50%",
                  backgroundColor: optionChoose == "no" ? colorConstant.bbg : colorConstant.lightGray,
                  justifyContent: "center",
                  alignItems: "center",
                  borderBottomWidth: 1,
                  borderBottomColor: "#D3D3D3",
                  borderTopRightRadius: 20

                }}>
                <Text style={{
                  fontSize: 18,
                  fontFamily: fontConstant.bold,
                  color: optionChoose == "no" ? colorConstant.white : colorConstant.blackText
                }}>NO <Text style={{
                  fontSize: 14,
                  fontFamily: fontConstant.medium,
                  color: optionChoose == "no" ? colorConstant.white : colorConstant.blackText
                }}> ₹{eventDetails?.price_no} </Text></Text>
              </TouchableOpacity>



            </View>



            <ScrollView
              show showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                paddingVertical: 20
              }}>
              {/* <Text style={{
                fontSize: 16,
                fontFamily: fontConstant.medium,
                color: colorConstant.blackText,
                width: "90%",
                alignSelf: "center"
              }}>#{eventDetails?.subcategory?.name}</Text> */}

              <View style={{
                width: "90%",
                alignSelf: "center",
                marginTop: 20
              }}>
                <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.bold,
                  lineHeight: 25,
                  color: colorConstant.blackText,

                }}>{eventDetails?.title}
                </Text>
              </View>

              <View style={{
                flexDirection: "row",
                alignItems: "center",
                alignSelf: "center",
                marginTop: 20,
                width: "90%"
              }}>
                <Image
                  source={imageConstant.timer}

                  style={{
                    width: 20,
                    height: 20,
                    tintColor: colorConstant.blackText
                  }}
                />
                <Text style={{
                  fontSize: 14,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.blackText
                }}> {moment(eventDetails?.end_datetime).fromNow()}</Text>
              </View>



              <View style={{
                flexDirection: "row",
                width: Width * 0.9,
                justifyContent: "space-between",
                alignSelf: "center",


              }}>

                <View style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 20

                }}>
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText,
                  }}>Set Price <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText
                  }}> ₹{price}</Text></Text>

                  {
                    sliderShow ?
                      <TouchableOpacity
                        onPress={() => setSliderShow(!sliderShow)}
                        style={{
                          padding: 5,
                          marginLeft: 5
                        }}>

                        <Image
                          resizeMode='contain'
                          source={imageConstant.poly}
                          style={{
                            width: 10,
                            height: 10
                          }} />
                      </TouchableOpacity>
                      :
                      <TouchableOpacity
                        onPress={() => setSliderShow(!sliderShow)}
                        style={{
                          padding: 5,
                          marginLeft: 5
                        }}>

                        <Image
                          resizeMode='contain'
                          source={imageConstant.poly}
                          style={{
                            width: 10,
                            height: 10,
                            transform: [{ rotate: "180deg" }]
                          }} />
                      </TouchableOpacity>
                  }


                </View>


                {
                  suggestedQuantity?.option?.toUpperCase() == optionChoose.toUpperCase()
                    ?
                    <Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.medium,
                      color: colorConstant.blackText,
                      textAlign: "right",
                      marginTop: 20
                    }}>Available Matches<Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.medium,
                      color: colorConstant.blackText
                    }}> {suggestedQuantity?.qty} </Text></Text>
                    :
                    <View>
                    </View>
                }



              </View>





              {/* //Price Slider */}

              {
                sliderShow && (
                  <View style={{
                    width: Width * 0.9,
                    alignSelf: "center",
                    alignItems: "center"
                  }}>
                    <MultiSlider
                      values={price}
                      min={1}
                      max={9}
                      sliderLength={Width * 0.86}
                      onValuesChange={(val) => setPrice(val)}
                      selectedStyle={{ backgroundColor: colorConstant.white }}
                      unselectedStyle={{ backgroundColor: colorConstant.white }}
                      //={imageConstant.thumbimage}
                      customMarker={() => {
                        return (
                          <Image
                            source={imageConstant.thumb}
                            resizeMode='contain'
                            style={{
                              height: 30,
                              width: 30
                            }}
                          />
                        )
                      }}
                      trackStyle={{ height: 4, borderRadius: 10 }}
                      markerStyle={{ height: 20, width: 20, backgroundColor: '#087CFF', }}
                    />
                  </View>
                )
              }


              <View style={{
                width: "90%",
                alignSelf: "center",
                alignItems: "flex-start",
                marginTop: sliderShow ? 0 : 20
              }}>
                <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.blackText

                }}>Quantity <Text style={{
                  fontSize: 16,
                  fontFamily: fontConstant.medium,
                  color: colorConstant.blackText
                }}>{quantity}</Text></Text>
              </View>


              <View style={{
                width: Width * 0.9,
                alignSelf: "center",
                alignItems: "center"
              }}>
                <MultiSlider
                  values={quantity}
                  min={1}
                  max={25}
                  sliderLength={Width * 0.85}
                  onValuesChange={(val) => setQuantity(val)}
                  selectedStyle={{ backgroundColor: colorConstant.white }}
                  unselectedStyle={{ backgroundColor: colorConstant.white }}
                  //={imageConstant.thumbimage}
                  customMarker={() => {
                    return (
                      <Image
                        source={imageConstant.thumb}
                        resizeMode='contain'
                        style={{
                          height: 30,
                          width: 30
                        }}
                      />
                    )
                  }}
                  trackStyle={{ height: 4, borderRadius: 10 }}
                  markerStyle={{ height: 20, width: 20, backgroundColor: '#087CFF', }}
                />

              </View>




              <View style={{
                flexDirection: "row",
                // width: "100%",
                justifyContent: "space-between",
                marginTop: 20
              }}>
                <View style={{
                  width: "50%",
                  justifyContent: "center",
                  alignItems: "center",
                  borderRightWidth: 1,
                  borderRightColor: "#004BEB"
                }}>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: fontConstant.bold,
                    color: colorConstant.blackText
                  }}>₹ {quantity * price}</Text>
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText
                  }}>You invest</Text>
                </View>
                <View
                  style={{
                    width: "50%",
                    justifyContent: "center",
                    alignItems: "center"
                  }}>
                  <Text style={{
                    fontSize: 18,
                    fontFamily: fontConstant.bold,
                    color: colorConstant.blackText
                  }}>₹ {eventDetails?.trade_value * quantity}</Text>
                  <Text style={{
                    fontSize: 16,
                    fontFamily: fontConstant.medium,
                    color: colorConstant.blackText
                  }}>You Earn</Text>
                </View>
              </View>







              <SwipeButton
                title={`SWIPE FOR ${optionChoose == "yes" ? "YES" : "NO"}`}
                shouldResetAfterSuccess={true}
                height={45}
                width={Width * 0.9}
                railBackgroundColor={colorConstant.blue}
                titleColor='#FFFFFF'
                railFillBackgroundColor={colorConstant.blue}
                railBorderColor={colorConstant.blue}
                railFillBorderColor={colorConstant.blue}
                thumbIconBackgroundColor={colorConstant.green}
                thumbIconImageSource={imageConstant.rightarrow}
                thumbIconBorderColor={colorConstant.green}
                containerStyles={{ marginTop: 50, alignSelf: "center" }}
                onSwipeSuccess={() => checkBalance(eventDetails?.id)}


              />

              {
                price * quantity > walletBalance ?
                  <View style={{
                    width: "85%",
                    flexDirection: "row",
                    marginTop: 30,
                    alignItems: "center",
                    alignSelf: "center",
                    justifyContent: "space-between"
                  }}>
                    <Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.bold,
                      color: colorConstant.blackText
                    }}> Wallet Balance <Text style={{
                      fontSize: 14,
                      fontFamily: fontConstant.bold,
                      color: colorConstant.blackText
                    }}> ₹ {walletBalance?.toFixed(2)}</Text></Text>


                    <TouchableOpacity
                      onPress={() => {
                        setDetailsModal(!detailsModal);
                        setTimeout(() => {
                          navigation.navigate("Wallet");
                        }, 500)
                      }}
                      activeOpacity={1}
                      style={{
                        paddingHorizontal: 25,
                        paddingVertical: 8,
                        borderRadius: 20,
                        backgroundColor: "#99BFF3"

                      }}>
                      <Text style={{
                        fontSize: 16,
                        fontFamily: fontConstant.medium,
                        color: "#004BEB"
                      }}>Recharge Now</Text>
                    </TouchableOpacity>
                  </View>

                  :
                  <></>
              }


            </ScrollView>



          </View>


        </View>


      </Modal>
    </SafeAreaView>
  );
};

export default Search;

const styles = StyleSheet.create({
  bellImageStyle: {
    height: 36,
    width: 36,
    resizeMode: "contain",
  },
  optionImageStyle: {
    height: 36,
    width: 36,
  },
  input1: {
    fontSize: 16,
    color: "#777E91",
    fontWeight: "400",
    width: "90%",
    marginBottom: Platform.OS === "ios" ? null : -10,

  },
  searchImageStyle: {
    height: 24,
    width: 24,
    marginTop: 5,
    resizeMode: "contain",
  },
  catStyle: {
    fontSize: 14,
    color: "#1A1A1A",
    fontWeight: "600",
    fontFamily: fontConstant.medium,
  },
  Img: {
    width: wp("15%"),
    height: wp("15%"),
    borderRadius:wp("15%")
  },
  textStyle: {
    color: "#141416",
    fontSize: 14,
    fontWeight: "500",
    fontFamily: fontConstant.regular,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor:"rgba(1,1,1,0.5)"

  },
  centeredView2: {
    flex: 1,
    backgroundColor:"rgba(1,1,1,0.5)"

  }
  ,
  modalView: {
    margin:10,
    width:"90%",
    backgroundColor: "white",
    borderRadius: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  
  modalView2: {
    width:"100%",
    height:Height*0.80,
    position:"absolute",
    bottom:0,
    backgroundColor:colorConstant.skyblue,
    borderTopLeftRadius: 20,
    borderTopRightRadius:20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  }
});
