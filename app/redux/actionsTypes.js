export const INTRO_STATUS = "INTRO_STATUS";
export const JWT_TOKEN = "JWT_TOKEN";
export const APP_STATE = "APP_STATE";
export const WALLET_BALANCE = "WALLET_BALANCE";
export const PROFILE = "PROFILE";