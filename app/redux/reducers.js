import * as types from "./actionsTypes";
import defaultState from "./states";

/**
 * Reducer
 */
export default function parentFlowReducer(
    state = defaultState.localStates,
    action
) 
{
    switch (action.type) {
        case types.INTRO_STATUS:
            return {
                ...state,
                introStatus: action.payload.introStatus
            }
            case types.JWT_TOKEN:
            return {
                ...state,
                jwtToken: action.payload.jwtToken
            }
            case types.APP_STATE:
            return {
                ...state,
                appState: action.payload.appState
            }
            case types.WALLET_BALANCE:
            return {
                ...state,
                walletBalance: action.payload.walletBalance
            }
            case types.PROFILE:
            return {
                ...state,
                profileData: action.payload.profileData
            }
        default:
            return state;
    }

}

/**
 * Actions
 */
export const actions = {
    setIntroStatus: introStatus => {
        return {
            type: types.INTRO_STATUS,
            payload: { introStatus: introStatus }
        };
    },
    setJwtToken: jwtToken => {
        return {
            type: types.JWT_TOKEN,
            payload: { jwtToken: jwtToken }
        };
    },
    setAppState: appState => {
        return {
            type: types.APP_STATE,
            payload: { appState: appState }
        };
    },
    setWalletBalance: payload => {
        let sum = payload?.deposits + payload?.promotional + payload?.winnings;
        return {
            type: types.WALLET_BALANCE,
            payload: { walletBalance:sum}
        };
    },
    setReferCode: profileData => {
        return {
            type: types.PROFILE,
            payload: { profileData:profileData}
        };
    }
} 