import React, { useState, useEffect } from "react";
import { View, Text, Dimensions, SafeAreaView } from "react-native";
import NetInfo from "@react-native-community/netinfo";
import { colorConstant, fontConstant } from "./constant";

var { height, width } = Dimensions.get("window");
const NetConnectionScreen = (props) => {
  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener((state) => {
      console.log(state)
      setNetConncetionStatus(state?.isConnected);
    });
    return ()=>unsubscribe
  }, []);
  const [data, setData] = useState();
  const [netConncetionStatus, setNetConncetionStatus] = useState(true);

  return (
    <SafeAreaView>
      <View>
        {!netConncetionStatus && (
          <View
            style={{
              width: width,
              backgroundColor:"red",
              alignItems: "center",
              justifyContent: "center",
              height: 40,
            }}
          >
            <Text style={{ color:"#FFFFFF" }}>
              Please check Internet Connection
            </Text>
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};
export default NetConnectionScreen;
