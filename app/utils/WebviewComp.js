import React, { Component } from 'react';
import { useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';


const WebviewComp = (props) => {
    return (
        <View style={{
            flex: 1,
        }}>
            <WebView source={{ uri: props?.route?.params?.url }} />
        </View>
    )
}

export default WebviewComp;