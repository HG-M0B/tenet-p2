const fontConstant = {
    bold: "Roboto-Bold",
    medium: "Roboto-Medium",
    regular: "Roboto-Regular",
    italic: "Roboto-italic"
}
import thumb from  '../assets/images/thumb.png'; 
import poly from '../assets/images/poly.png';
import back from '../assets/images/back.png';
import logo from '../assets/images/logo.png';
import appName from '../assets/images/appName.png';
import bell from '../assets/images/bell.png';
import options from '../assets/images/options.png';
import home1 from '../assets/images/home1.png';
import home from '../assets/images/home.png';
import profile from '../assets/images/profile.png';
import profile1 from '../assets/images/profile1.png';
import search from '../assets/images/search.png';
import search1 from '../assets/images/search1.png';
import wallet from '../assets/images/wallet.png';
import wallet1 from '../assets/images/wallet1.png';
import portfolio from '../assets/images/portfolio.png';
import portfolio1 from '../assets/images/portfolio1.png';
import profileimage from '../assets/images/profileimage.png';
import aboutUs from '../assets/images/aboutUs.png';
import tAndc from '../assets/images/tAndc.png';
import privacypolicy from '../assets/images/privacypolicy.png';
import rateUs from '../assets/images/rateUs.png';
import updateapp from '../assets/images/updateapp.png';
import helpSupport from '../assets/images/helpSupport.png';
import contactUs from '../assets/images/contactUs.png';
import contactNumber from '../assets/images/contactNumber.png';
import emailId from '../assets/images/emailId.png';
import website from '../assets/images/website.png';
import resolved from '../assets/images/resolved.png';
import pending from '../assets/images/pending.png';

import Edit from '../assets/images/Edit.png';
import invoice from '../assets/images/invoice.png';
import Logout from '../assets/images/Logout.png';
import referEarn from '../assets/images/referEarn.png';
import trade from '../assets/images/trade.png';
import twoArrow from '../assets/images/twoArrow.png';
import watchlist from '../assets/images/watchlist.png';
import whiteBell from '../assets/images/whiteBell.png';
import whiteOptions from '../assets/images/whiteOptions.png';
import camera from '../assets/images/camera.png';
import saveChabgesButton from '../assets/images/saveChabgesButton.png';
import betting2 from '../assets/images/betting2.png';
import bettingBanner from '../assets/images/bettingBanner.png';
import cancelButton from '../assets/images/cancelButton.png';
import logoutLogo from '../assets/images/logoutLogo.png';
import confirmButton from '../assets/images/confirmButton.png';
import submitButton from '../assets/images/submitButton.png';
import recomendedIcon from '../assets/images/recomendedIcon.png';
import save from '../assets/images/save.png';
import timer from '../assets/images/timer.png';
import volume from '../assets/images/volume.png';
import videoPlaceholder from '../assets/images/videoPlaceholder.png';
import referBackground from '../assets/images/referBackground.png';
import savingMoney from '../assets/images/savingMoney.png';
import share from '../assets/images/share.png';
import share2 from '../assets/images/share2.png';
import whatsapp from '../assets/images/whatsapp.png';
import backgroundColl from '../assets/images/backgroundColl.png';

import Slide1 from '../assets/images/Slide1.png';
import Slide2 from '../assets/images/Slide2.png';
import Slide3 from '../assets/images/Slide3.png';
import Slide4 from '../assets/images/Slide4.png';


import lion from '../assets/images/lion.png';
import upi from '../assets/images/upi.png';


import Progress1 from '../assets/images/Progress1.png';
import Progress2 from '../assets/images/Progress2.png';
import Progress3 from '../assets/images/Progress3.png';

import searchMain from '../assets/images/searchMain.png';
import Rectangle1 from '../assets/images/Rectangle1.png';
import Rectangle2 from '../assets/images/Rectangle2.png';
import Rectangle3 from '../assets/images/Rectangle3.png';

import sb1 from '../assets/images/sb1.png';
import sb2 from '../assets/images/sb2.png';
import sb3 from '../assets/images/sb3.png';

import kycCongrats from '../assets/images/kycCongrats.png';
import kycButton from '../assets/images/kycButton.png';

import tick from '../assets/images/tick.png';
import minus from '../assets/images/minus.png';
import plus from '../assets/images/plus.png';
import iButton from '../assets/images/iButton.png';

import addAmount from '../assets/images/addAmount.png';
import withdrawButton from '../assets/images/withdrawButton.png';
import cancelImg from '../assets/images/cancelImg.png';
import calendar from '../assets/images/calendar.png';
import download from '../assets/images/download.png';
import refresh from '../assets/images/refresh.png';
import star from '../assets/images/star.png';
import money from '../assets/images/money.png';
import Success from '../assets/images/Success.png';
import tradeMore from '../assets/images/tradeMore.png';

import closedImage from '../assets/images/closedImage.png';
import openImage from '../assets/images/openImage.png';
import virat from '../assets/images/virat.png';
import up from '../assets/images/up.png';

import noteNoti from '../assets/images/noteNoti.png';
import settingsNoti from '../assets/images/settingsNoti.png';
import userNoti from '../assets/images/userNoti.png';
import walletNoti from '../assets/images/walletNoti.png';
import graph from '../assets/images/graph.png';
import greenBell from '../assets/images/greenBell.png';
import greenBulb from '../assets/images/greenBulb.png';
import verified from '../assets/images/verified.png';
import conf from '../assets/images/conf.png';
import greenCal from '../assets/images/greenCal.png';
import letStart from '../assets/images/letStart.png';
import Profile_image from '../assets/images/Profile_image.png'
import EventCat from '../assets/images/EventCat.png'
import bookmark from '../assets/images/bookmark.png'

import flag from '../assets/images/flag.png'
import logo2 from '../assets/images/logo2.png'
import Calen from '../assets/images/Calen.png'
import Ref1 from '../assets/images/Ref1.png'
import Ref2 from '../assets/images/Ref2.png'
import Ref3 from '../assets/images/Ref3.png'
import Down from '../assets/images/Down.png'
import lock from '../assets/images/lock.png'
import rightarrow from '../assets/images/rightarrow.png'
import hands from '../assets/images/hands.png'
import stars from '../assets/images/stars.png'
import rupees from '../assets/images/rupees.png'
import edits from '../assets/images/edits.png'
import thumbimage from '../assets/images/thumbimage.png'
import bank from '../assets/images/bank.png';
import heart from '../assets/images/heart.png'
import Research from '../assets/images/Research.png'
import right from '../assets/images/right.png';
import defaultimage from '../assets/images/defaultimage.png';
import referral from '../assets/images/referral.png';
import arrow from '../assets/images/arrow.png';
import join from '../assets/images/join.png';
import whatsbg from '../assets/images/whatsbg.png'
import wallet2 from '../assets/images/wallet2.png'
import share3 from '../assets/images/share3.png'
import copy from '../assets/images/copy.png'
import telegram from '../assets/images/telegram.png'
import main from '../assets/images/main.png';
export const  ProgessFinish = require('../assets/images/ProgessFinish.png')


const imageConstant = {
    main,
    telegram,
    thumb,
    copy,
    share3,
    wallet2,
    whatsbg,
    join,
    arrow,
    referral,
    defaultimage,
    right,
    heart,
    Research,
    bank,
    rightarrow,
    hands,
    stars,
    rupees,
    edits,
    thumbimage,
    lock,
    Ref1,Ref2,Ref3,Down,
    flag,Calen,logo2,
    back,
    poly,
    logo,
    appName,
    bell,
    options,
    wallet,
    portfolio,
    profile,
    search,
    home1,
    home,
    profile1,
    portfolio1,
    search1,
    wallet1,
    profileimage,
    aboutUs,
    tAndc,
    privacypolicy,
    rateUs,
    updateapp,
    helpSupport,
    contactUs,
    contactNumber,
    emailId,
    website,
    resolved,
    pending,
    Edit, 
    invoice,
    Logout,
    referEarn, 
    trade, 
    twoArrow, 
    watchlist, 
    whiteBell, 
    whiteOptions,
    camera,
    saveChabgesButton,
    betting2,
    bettingBanner,
    cancelButton,
    logoutLogo,
    confirmButton,
    submitButton,
    recomendedIcon,
    save,
    timer,
    volume,

    Slide1,
    Slide2,
    Slide3,
    Slide4,
    Progress1,
    Progress2,
    Progress3,

    upi,
    lion,

    videoPlaceholder,

    referBackground,
    savingMoney,
    share,
    whatsapp,
    backgroundColl,

    searchMain,
    Rectangle1,
    Rectangle2,
    Rectangle3,
    sb1,
    sb2,
    sb3,

    kycCongrats,
    kycButton,
    tick,
    plus,
    minus,
    iButton,

    addAmount,
    withdrawButton,
    cancelImg,
    calendar,
    download,
    refresh,
    star,
    money,
    Success,
    tradeMore,

    closedImage,
    openImage,
    virat,
    up,
    noteNoti,
    userNoti,
    walletNoti,
    settingsNoti,
    
    share2,
    graph,
    greenBell,
    greenBulb,
    verified,
    conf,
    greenCal,
    letStart,
    Profile_image,
    EventCat,
    bookmark
}

export const colorConstant = {
    white:"#FFFFFF",
    blue:"#0049F1",
    pink:"#FF2567",
    gray:"#899499",
    skyblue:"#F5F5F5",
    black:"1A1A1A",
    blackText:"#000000",
    green:"#08A02A",
    bbg:"#004BEB",
    lightGray:"#DEDEDE"
}

export {
    fontConstant,
    imageConstant
}