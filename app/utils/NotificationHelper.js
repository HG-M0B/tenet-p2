import { Alert } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';
import PushNotification, {Importance} from 'react-native-push-notification';
import * as NavigationService from "react-navigation-helpers";
import { colorConstant } from './constant';

export const requestUserPermission = async ()=> {
  const authStatus = await messaging().requestPermission();
  const enabled =
    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
    authStatus === messaging.AuthorizationStatus.PROVISIONAL;

  if (enabled) {
      getToken();
  }
}


export const getToken = async () => {    
    let checkToken = await AsyncStorage.getItem('fcmToken');
    console.log("Old token==>", checkToken);
    if (!checkToken) {
      try {
        const fcmToken = await messaging().getToken();
        if (!!fcmToken) {
          console.log("fcmtoken generated ====>", fcmToken);
          await AsyncStorage.setItem('fcmToken',fcmToken);
        }
      }
      catch (error) {
        console.log("error in token generation", error);
       
      }
    }
  }

 

export  const NotificationListener = () => {
    messaging().onNotificationOpenedApp(remoteMessage => {
        console.log(
          'Notification caused app to open from background state:',
          remoteMessage.notification,
        );
        NavigationService.navigate("Notifications");
      });
    

      messaging()
        .getInitialNotification()
        .then(remoteMessage => {
          if (remoteMessage) {
            console.log(
              'Notification caused app to open from quit state:',
              remoteMessage.notification,
            );
            NavigationService.navigate("Notifications");
          }
        });

      

        PushNotification.configure({
          onNotification: (notification) => {
            console.log('onNotification-------------touched object', notification);
              // NavigationService.navigate("Notifications");
           
          },
          onAction: function (notification) {
            console.log("ACTION:", notification.action);
            console.log("NOTIFICATION:", notification);
          },
          popInitialNotification: true,
          requestPermissions: true,
        });
}


