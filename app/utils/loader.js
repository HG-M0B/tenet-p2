import React, { useState, useEffect } from 'react'
import Spinner from 'react-native-loading-spinner-overlay';
import { colorConstant } from './constant'
const Loader = (props) => {
    return (

        <Spinner
            visible={props.data}
            color= '#004BEB'
        />


    )
}
export default Loader