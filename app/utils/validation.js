
export default validation = {

    isEmail: (email) => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(String(email).toLowerCase())) {
            return false;
        }
        else {
            return true
        }
    },
    isEmpty: (string) => {
        return string.trim() === ""
    },
    
    isEmptyNumber: (string) => {
        return string === 0
    },
    isName: (string) => {
        const re = /^[a-zA-Z ]+$/
        return re.test(string)
    },
    isphoneNumber: (string) => {
        let tempString = string?.trim();
        // string.length<15?isNaN(string) ?
        return tempString.length < 10 || tempString.length > 10 || isNaN(string) ? true : false
    },


    isZipCode: (string) => {
        // string.length<15?isNaN(string) ?
        return string.length < 6 || string.length > 6 || isNaN(string) ? false : true
    },


    isAlphabet: (string) => {
        const re = /^[a-zA-Z() ]+$/
        return re.test(string)
    },

    isCheck :(boolean)=>{
        return boolean ? false : true
    },
     generateColor : () => {
        const randomColor = Math.floor(Math.random() * 16777215)
          .toString(16)
          .padStart(6, '0');
        return `#${randomColor}`;
      },
     isPan:(string) => {
            let regex = /^([A-Z]){5}([0-9]){4}([A-Z]){1}?$/
             regex.test(string);
             if(regex.test(string)){
              return false
             }else{
                 return true
             }
   }
}