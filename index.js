/**
 * @format
 */
 import 'react-native-gesture-handler'
import {AppRegistry} from 'react-native';
import App from './App';
import React from 'react';
import {name as appName} from './app.json';
import { Provider } from "react-redux";
import store ,{ persistor }from './app/utils/store';
import { PersistGate } from 'redux-persist/integration/react';
import messaging from '@react-native-firebase/messaging';

const Root = () => {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <App />
        </PersistGate>
      </Provider>
    )
  }

  messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('Message handle in backgroun', JSON.stringify(remoteMessage));
  });

AppRegistry.registerComponent(appName, () => Root);
