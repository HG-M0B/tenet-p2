import 'react-native-gesture-handler';

import {
  Text,
  StyleSheet,
  View,
  Image,
  TextInput,
  SafeAreaView,
  Button,
  TouchableOpacity,
  Alert,
  Platform,
  Dimensions,
  AppState,
  LogBox
} 
from 'react-native';
import React, { Component,useEffect } from 'react'

import SplashScreen from 'react-native-splash-screen'

import { useDispatch } from 'react-redux';
import RootNavigator from './app/navigator/RootNavigator';


// import HomePage from './app/Screens/HomePage/HomePage'
// import Search from './app/Screens/Search/Search'
// import Wallet from './app/Screens/Wallet/Wallet'
// import Profile from './app/Screens/Profile/Profile'
// import EditProfile from './app/Screens/EditProfile/EditProfile'
// import Portfolio from './app/Screens/Portfolio/Portfolio'
// import Notifications from './app/Screens/Notifications/Notifications'
// import DrawerContainer from './app/Screens/DrawerContainer/DrawerContainer';
// import AboutUs from './app/Screens/AboutUs/AboutUs';
// import ContactUs from './app/Screens/ContactUs/ContactUs';
// import TermsConds from './app/Screens/TermsCondition/TermsCondition';
// import TermsConds1 from './app/Screens/TermsCondition/TermsCondition1';
// import PrivacyPolicy from './app/Screens/PrivacyPolicy/PrivacyPolicy';
// import HelpSupport from './app/Screens/HelpSupport/HelpSupport';
// import Invoice from './app/Screens/Invoice/Invoice';
// import CreateTicket from './app/Screens/CreateTicket/CreateTicket'
// import WatchList from './app/Screens/WatchList/WatchList'
// import HowToTrade from './app/Screens/HowToTrade/HowToTrade'
// import ReferEarn from './app/Screens/ReferEarn/ReferEarn'
// import Category from './app/Screens/Category/Category'
// import SubCat from './app/Screens/SubCat/SubCat'
// import KycVerification from './app/Screens/KycVerification/KycVerification'
// import Congrats from './app/Screens/Congrats/Congrats'
// import EventPortfolio from './app/Screens/Portfolio/EventPortfolio';
// import EventDetails from './app/Screens/EventDetails/EventDetails';



import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {NotificationListener,getToken,requestUserPermission} from './app/utils/NotificationHelper';
import NotificationController from './app/utils/NotificationController';
import NetConnectionScreen from './app/utils/NetConnectionScreen'
import { actions } from './app/redux/reducers';
import { isReadyRef, navigationRef } from "react-navigation-helpers";
import { baseURL } from './app/utils/URL';

const Stack = createNativeStackNavigator();

const App = () => {

let dispatch = useDispatch();

  useEffect(()=>{
    requestUserPermission();
    NotificationListener();
    getToken();
    callMainApi();
    const subscription = AppState.addEventListener("change", nextAppState => {
      if(nextAppState == "active")
      {
        dispatch(actions.setAppState("foreground"));
      }
      if(nextAppState == "background")
      {
        dispatch(actions.setAppState("background"));
      }
    });
    return () => {
      subscription.remove();
    };
  },[])


   useEffect(() => {
    return () => (isReadyRef.current = false);
  }, []);
  const callMainApi = async () => {
    try {
      let response = await fetch(baseURL + `maintenance/`);
      if (response && response.status == 200) {
        let jsonData = await response.json();
        console.log("prod main-->",jsonData?.maintenance)
        if (jsonData?.maintenance) {
          dispatch(actions.setIntroStatus("check"));
        }
      }
     

    }
    catch (error) {
      console.log("error-->", error)
    }
  }
  LogBox.ignoreAllLogs();
  console.log("App render")
  return (
    // <SafeAreaView style={{flex:1}}>
    <NavigationContainer
    ref={navigationRef}
    >
      <NetConnectionScreen />
      <NotificationController />
      <RootNavigator />
    </NavigationContainer>
    // </SafeAreaView>

  )
}


export default App
